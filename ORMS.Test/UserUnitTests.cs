﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ORMS.Infrastructure;
using ORMS.Core;
using System.Collections.Generic;
using System.Linq;

namespace ORMS.Test
{
    [TestClass]
    public class UserUnitTests
    {
        [TestMethod]
        public void GetAllUsers()
        {
            using (UnitOfWork unitOfWork = new UnitOfWork(new ORMSContext()))
            {
                IEnumerable<User> users = unitOfWork.Users.GetAll();

                Assert.IsNotNull(users, "GetAllUsers() returned null.");
                Assert.IsTrue(users.Count() > 0, "There is no users in database.");
            }
        }

        [TestMethod]
        public void GetUserById()
        {
            using (UnitOfWork unitOfWork = new UnitOfWork(new ORMSContext()))
            {
                var user = unitOfWork.Users.Get(1);

                Assert.IsNotNull(user, "GetUserById() returned null.");
                Assert.AreEqual(user.UserId, 1);
            }
        }

        [TestMethod]
        public void RemoveUser()
        {
            using (UnitOfWork unitOfWork = new UnitOfWork(new ORMSContext()))
            {
                var user = unitOfWork.Users.Get(2);
                unitOfWork.Users.Remove(user);

                Assert.IsNotNull(user, "RemoveUser() returned null.");
                Assert.AreEqual(user.UserId, 2);
            }
        }
    }
}
