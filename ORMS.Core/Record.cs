namespace ORMS.Core
{
    using ORMS.Core.Base;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class Record : BaseEntity
    {
        public Record()
        {
            RecordItems = new HashSet<RecordItem>();
            WorkerLogs = new HashSet<WorkerLog>();
        }

        public int RecordId { get; set; }

        [Column(TypeName = "money")]
        public decimal TotalEarnings { get; set; }

        [StringLength(300)]
        public string Comment { get; set; }

        public int OfficeId { get; set; }

        public virtual Office Office { get; set; }

        public virtual ICollection<RecordItem> RecordItems { get; set; }

        public virtual User UserCreated { get; set; }

        public virtual User UserUpdated { get; set; }

        public virtual ICollection<WorkerLog> WorkerLogs { get; set; }
    }
}
