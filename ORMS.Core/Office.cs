namespace ORMS.Core
{
    using ORMS.Core.Base;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class Office : BaseEntity
    {
        public Office()
        {
            Records = new HashSet<Record>();
            Users = new HashSet<User>();
        }

        public int OfficeId { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        [StringLength(255)]
        public string Address { get; set; }

        [StringLength(5)]
        public string PostalCode { get; set; }

        [StringLength(255)]
        public string Place { get; set; }

        [StringLength(255)]
        public string City { get; set; }

        [Required]
        [StringLength(11)]
        public string OIB { get; set; }

        public virtual User UserCreated { get; set; }

        public virtual User UserUpdated { get; set; }

        public virtual ICollection<Record> Records { get; set; }

        public virtual ICollection<User> Users { get; set; }
    }
}
