namespace ORMS.Core
{
    using System;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class WorkerLog
    {
        public int WorkerLogId { get; set; }

        public int UserId { get; set; }

        public int ShiftId { get; set; }

        public int RecordId { get; set; }

        [Column(TypeName = "money")]
        public decimal ShiftEarnings { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime DateUpdated { get; set; }

        public virtual Record Record { get; set; }

        public virtual Shift Shift { get; set; }

        public virtual User User { get; set; }
    }
}
