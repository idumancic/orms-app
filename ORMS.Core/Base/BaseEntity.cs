﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMS.Core.Base
{
    public abstract class BaseEntity
    {
        public int Active { get; set; }

        public int UserCreatedId { get; set; }

        public int UserUpdatedId { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime DateUpdated { get; set; }
    }
}
