namespace ORMS.Core
{
    using ORMS.Core.Base;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class Shift : BaseEntity
    {
        public const string FIRST = "1S";
        public const string SECOND = "2S";

        public Shift()
        {
            WorkerLogs = new HashSet<WorkerLog>();
        }

        public int ShiftId { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [Required]
        [StringLength(3)]
        public string Code { get; set; }

        public virtual User UserCreated { get; set; }

        public virtual User UserUpdated { get; set; }

        public virtual ICollection<WorkerLog> WorkerLogs { get; set; }
    }
}
