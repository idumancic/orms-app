namespace ORMS.Core
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class Role
    {
        public const string ADMIN = "ORMS_Admin";
        public const string OWNER = "ORMS_Owner";
        public const string MANAGER = "ORMS_Manager";
        public const string WORKER = "ORMS_Worker";

        public Role()
        {
            Users = new HashSet<User>();
        }

        [Key]
        [StringLength(30)]
        public string RoleCode { get; set; }

        [Required]
        [StringLength(30)]
        public string Name { get; set; }

        public int Active { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime DateUpdated { get; set; }

        public virtual ICollection<User> Users { get; set; }
    }
}
