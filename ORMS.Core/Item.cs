namespace ORMS.Core
{
    using ORMS.Core.Base;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class Item : BaseEntity
    {
        public Item()
        {
            RecordItems = new HashSet<RecordItem>();
        }

        public int ItemId { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        [Column(TypeName = "money")]
        public decimal Price { get; set; }

        public int CategoryId { get; set; }

        public virtual Category Category { get; set; }

        public virtual User UserCreated{ get; set; }

        public virtual User UserUpdated { get; set; }

        public virtual ICollection<RecordItem> RecordItems { get; set; }
    }
}
