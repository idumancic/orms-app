namespace ORMS.Core
{
    using ORMS.Core.Base;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class Category : BaseEntity
    {
        public Category()
        {
            Items = new HashSet<Item>();
        }

        public int CategoryId { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        public virtual User UserCreated { get; set; }

        public virtual User UserUpdated { get; set; }

        public virtual ICollection<Item> Items { get; set; }
    }
}
