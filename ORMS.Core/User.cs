namespace ORMS.Core
{
    using ORMS.Core.Base;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class User : BaseEntity
    {
        public User()
        {
            Categories = new HashSet<Category>();
            Categories1 = new HashSet<Category>();
            Items = new HashSet<Item>();
            Items1 = new HashSet<Item>();
            Offices = new HashSet<Office>();
            Offices1 = new HashSet<Office>();
            Records = new HashSet<Record>();
            Records1 = new HashSet<Record>();
            Shifts = new HashSet<Shift>();
            Shifts1 = new HashSet<Shift>();
            Users1 = new HashSet<User>();
            Users11 = new HashSet<User>();
            WorkerLogs = new HashSet<WorkerLog>();
        }

        public int UserId { get; set; }

        [Required]
        [StringLength(255)]
        public string Username { get; set; }

        [Required]
        [StringLength(255)]
        public string Password { get; set; }

        [StringLength(255)]
        public string FirstName { get; set; }

        [StringLength(255)]
        public string LastName { get; set; }

        [Column(TypeName = "date")]
        public DateTime? DateOfBirth { get; set; }

        [StringLength(13)]
        public string JMBG { get; set; }

        [StringLength(11)]
        public string OIB { get; set; }

        [StringLength(255)]
        public string Address { get; set; }

        [StringLength(5)]
        public string PostalCode { get; set; }

        [StringLength(255)]
        public string City { get; set; }

        [StringLength(50)]
        public string PhoneNumber { get; set; }

        [StringLength(100)]
        public string Email { get; set; }

        public int? OfficeId { get; set; }

        public int? SanitaryIdVerified { get; set; }

        [Column(TypeName = "date")]
        public DateTime? SanitaryIdCreatedDate { get; set; }

        [Column(TypeName = "date")]
        public DateTime? SanitaryIdExpirationDate { get; set; }

        [Required]
        [StringLength(30)]
        public string RoleCode { get; set; }

        public virtual ICollection<Category> Categories { get; set; }

        public virtual ICollection<Category> Categories1 { get; set; }

        public virtual ICollection<Item> Items { get; set; }

        public virtual ICollection<Item> Items1 { get; set; }

        public virtual ICollection<Office> Offices { get; set; }

        public virtual ICollection<Office> Offices1 { get; set; }

        public virtual Office Office { get; set; }

        public virtual ICollection<Record> Records { get; set; }

        public virtual ICollection<Record> Records1 { get; set; }

        public virtual Role Role { get; set; }

        public virtual ICollection<Shift> Shifts { get; set; }

        public virtual ICollection<Shift> Shifts1 { get; set; }

        public virtual ICollection<User> Users1 { get; set; }

        public virtual User UserCreated { get; set; }

        public virtual ICollection<User> Users11 { get; set; }

        public virtual User UserUpdated { get; set; }

        public virtual ICollection<WorkerLog> WorkerLogs { get; set; }
    }
}
