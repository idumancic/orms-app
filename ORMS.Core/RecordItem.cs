namespace ORMS.Core
{
    public partial class RecordItem
    {
        public const string ITEM_NAME = "ARTIKL";
        public const string YESTERDAYS_STATE_OF_THE_BAR = "�ANK";
        public const string RECEIVED = "PRIM";
        public const string IN_TOTAL = "UKUPNO";
        public const string TODAYS_STATE_OF_THE_BAR = "�ANK";
        public const string SOLD_OUT = "PROD";
        public const string ITEM_PRICE = "CIJENA";
        public const string SUM_OF_SOLD_ARTICLES_PRICE = "ZBROJ";

        public int RecordItemId { get; set; }

        public int RecordId { get; set; }

        public int ItemId { get; set; }

        public int YesterdaysStateOfTheBar { get; set; }

        public int? Received { get; set; }

        public int? InTotal { get; set; }

        public int? TodaysStateOfTheBar { get; set; }

        public int? SoldOut { get; set; }

        public int? SumOfSoldArticlesPrice { get; set; }

        public virtual Item Item { get; set; }

        public virtual Record Record { get; set; }
    }
}
