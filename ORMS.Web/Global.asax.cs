﻿using Newtonsoft.Json.Serialization;
using ORMS.Infrastructure;
using ORMS.Infrastructure.Interfaces;
using ORMS.Service.Authentication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace ORMS.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            JsonConfig.Configure();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            Response.Redirect("#/error");
        }

        private class JsonConfig
        {
            internal static void Configure()
            {
                var formatters = GlobalConfiguration.Configuration.Formatters;
                var jsonFormatter = formatters.JsonFormatter;
                var settings = jsonFormatter.SerializerSettings;

                jsonFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/html"));
                settings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            }
        }
    }
}
