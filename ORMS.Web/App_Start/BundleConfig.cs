﻿using System.Web;
using System.Web.Optimization;

namespace ORMS.Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/angularjs").Include(
                        "~/Scripts/angular/angular.js",
                        "~/Scripts/angular/angular-route.js",
                        "~/Scripts/angular/angular-loader.js",
                        "~/Scripts/angular/angular-cookies.js",
                        "~/Scripts/angular/angular-paging.js",
                        "~/Scripts/angular/angular-base64.js",
                        "~/Scripts/angular/angular-chart.js",
                        "~/Scripts/i18n/angular-locale_hr.js"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/angularjs.min").Include(
                        "~/Scripts/angular/angular.min.js",
                        "~/Scripts/angular/angular-route.min.js",
                        "~/Scripts/angular/angular-loader.min.js",
                        "~/Scripts/angular/angular-cookies.min.js",
                        "~/Scripts/angular/angular-paging.js",
                        "~/Scripts/angular/angular-base64.js",
                        "~/Scripts/angular/angular-chart.min.js",
                        "~/Scripts/i18n/angular-locale_hr.js"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/sweetalert2").Include(
                        "~/Scripts/sweetalert2/sweetalert2.all.min.js"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/chartjs").Include(
                        "~/Scripts/Chart.min.js"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/app").Include(
                        "~/App/app.js",
                        "~/App/app-constants.js",
                        "~/App/app-controller.js")
                        .IncludeDirectory("~/App/components", "*.js", true)
                        .IncludeDirectory("~/App/shared", "*.js", true)
                        .IncludeDirectory("~/App/orms", "*.js", true));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap/bootstrap.bundle.js",
                      "~/Scripts/bootstrap/datepicker/bootstrap-datepicker.min.js",
                      "~/Scripts/i18n/bootstrap-datepicker.hr.min.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/Bootstrap/bootstrap.css",
                      "~/Content/Bootstrap/Datepicker/bootstrap-datepicker.standalone.min.css",
                      "~/Content/Sweetalert2/sweetalert2.css",
                      "~/Content/site.css"));
        }
    }
}
