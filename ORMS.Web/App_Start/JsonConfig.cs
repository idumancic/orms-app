﻿using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;

namespace ORMS.Web.App_Start
{
    public static class JsonConfig
    {

        // Formatira Web API response u JSON i pretvara C# propertye iz TitleCase-a u CamelCase
        public static void Configure(HttpConfiguration configuration)
        {
            var formatters = GlobalConfiguration.Configuration.Formatters;
            var jsonFormatter = formatters.JsonFormatter;
            var settings = jsonFormatter.SerializerSettings;

            jsonFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/html"));
            settings.ContractResolver = new CamelCasePropertyNamesContractResolver();
        }
    }
}