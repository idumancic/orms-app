﻿using ORMS.Service;
using ORMS.Service.Authentication;
using ORMS.Service.Authentication.Interface;
using ORMS.Service.Authentication.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Web;

namespace ORMS.Web.Modules
{
    public class BasicAuthHttpModule : IHttpModule
    {
        private const string REALM = "ORMS_Api";

        public void Init(HttpApplication context)
        {
            context.AuthenticateRequest += OnApplicationAuthenticateRequest;
            //context.EndRequest += OnApplicationEndRequest;
        }

        private static void OnApplicationAuthenticateRequest(object sender, EventArgs e)
        {
            var request = HttpContext.Current.Request;
            var authHeader = request.Headers["Authorization"];

            if(authHeader != null)
            {
                var authHeaderValue = AuthenticationHeaderValue.Parse(authHeader);
                if(authHeaderValue.Scheme.Equals("basic", StringComparison.OrdinalIgnoreCase) && authHeaderValue.Parameter != null)
                {
                    AuthenticateUser(authHeaderValue.Parameter);
                }
            }
        }

        private static void AuthenticateUser(string credentials)
        {
            string decodedCredentials = Encoding.UTF8.GetString(Convert.FromBase64String(credentials));
            string[] _credentials = decodedCredentials.Split(':');

            var username = _credentials[0];
            var password = _credentials[1];

            IAuthenticationService authService = ServiceFactory.CreateAuthenticationService();
            var authResponse = authService.Authenticate(new AuthenticationRequest(username, password));
            if (authResponse.Success)
            {
                var identity = new GenericIdentity(username);
                SetPrincipal(new GenericPrincipal(identity, new string[] { authResponse.User.Role }));
            }
        }

        private static void SetPrincipal(IPrincipal principal)
        {
            Thread.CurrentPrincipal = principal;
            if(HttpContext.Current != null)
            {
                HttpContext.Current.User = principal;
            }
        }

        //private static void OnApplicationEndRequest(object sender, EventArgs e)
        //{
        //    var response = HttpContext.Current.Response;
        //    if(response.StatusCode == 401)
        //    {
        //        response.Headers.Add("WWW-Authenticate", string.Format("Basic realm=\"{0}\"", REALM));
        //    }
        //}

        public void Dispose() {
        }
    }
}