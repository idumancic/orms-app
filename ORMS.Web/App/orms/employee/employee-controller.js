﻿(function (orms) {

    'use strict';

    const employeeController = function ($location, $route, $log, $alert, confirmModalService, userService, workers, STATUS) {

        let vm = this;
        orms.ec = this;

        vm.workers = workers;

        vm.goAdd = goAdd;
        vm.goEdit = goEdit;
        vm.remove = remove;

        function goAdd() {
            $location.path('/employees/add');
        }

        function goEdit(employeeId) {
            $location.path(`/employees/${employeeId}/edit`);
        }

        function remove(employee) {
            confirmModalService
                .show()
                .then(function (result) {
                    if (result) {
                        $log.info(employee);

                        let data = {
                            worker: {
                                id: employee.id,
                                active: STATUS.DISABLED
                            }
                        };

                        $alert.async(userService.updateWorker(data), { showFeedback: true })
                            .then(function (data) {
                                if (data.success) {
                                    $route.reload();
                                }
                            });
                    }
                });
        }

    };

    orms.app
        .controller('employeeController', employeeController);

})(orms);