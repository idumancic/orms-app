﻿(function (orms) {

    'use strict';

    orms.app
        .config(function ($routeProvider, FOLDER_PATH, NAVIGATION, VIEW_TYPE) {

            $routeProvider
                .when('/employees', {
                    templateUrl: FOLDER_PATH.ORMS + 'employee/templates/employees.html',
                    controller: 'employeeController',
                    controllerAs: 'ec',
                    title: NAVIGATION.EMPLOYEES.title,
                    activeTab: NAVIGATION.EMPLOYEES,
                    resolve: {
                        workers: ['$alert', 'userService', function ($alert, userService) {
                            return $alert
                                .async(userService.getWorkersEdit())
                                .then(function (data) {
                                    if (data.success) {
                                        return data.workers;
                                    }
                                });
                        }]
                    }
                })
                .when('/employees/add', {
                    templateUrl: FOLDER_PATH.ORMS + 'employee/templates/employee-edit.html',
                    controller: 'employeeEditController',
                    controllerAs: 'eec',
                    title: NAVIGATION.EMPLOYEES.title,
                    activeTab: NAVIGATION.EMPLOYEES,
                    viewType: VIEW_TYPE.CREATE,
                    resolve: {
                        viewType: ['$route', function ($route) {
                            return $route.current.viewType;
                        }],
                        offices: ['officeService', function (officeService) {
                            return officeService.getAllEdit().then(function (response) {
                                let data = response.data;
                                if (data.success) {
                                    return data.offices;
                                }
                            });
                        }],
                        roles: ['roleService', function (roleService) {
                            return roleService.getAll().then(function (response) {
                                let data = response.data;
                                if (data.success) {
                                    return data.roles;
                                }
                            });
                        }],
                        worker: ['$q', function ($q) {
                            return $q.resolve(undefined);
                        }]
                    }
                })
                .when('/employees/:id/edit', {
                    templateUrl: FOLDER_PATH.ORMS + 'employee/templates/employee-edit.html',
                    controller: 'employeeEditController',
                    controllerAs: 'eec',
                    title: NAVIGATION.EMPLOYEES.title,
                    activeTab: NAVIGATION.EMPLOYEES,
                    viewType: VIEW_TYPE.EDIT,
                    resolve: {
                        viewType: ['$route', function ($route) {
                            return $route.current.viewType;
                        }],
                        offices: ['officeService', function (officeService) {
                            return officeService.getAllEdit().then(function (response) {
                                let data = response.data;
                                if (data.success) {
                                    return data.offices;
                                }
                            });
                        }],
                        roles: ['roleService', function (roleService) {
                            return roleService.getAll().then(function (response) {
                                let data = response.data;
                                if (data.success) {
                                    return data.roles;
                                }
                            });
                        }],
                        worker: ['$alert', '$route', 'userService', function ($alert, $route, userService) {
                            return $alert
                                .async(userService.getWorker($route.current.params.id))
                                .then(function (data) {
                                    if (data.success) {
                                        return data.worker;
                                    }
                                });
                        }]
                    }
                });
        });

})(orms);