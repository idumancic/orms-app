﻿(function (orms) {

    'use strict';

    const employeeEditController = function ($alert, $route, $scope, $document, $location, offices, roles, worker, viewType, userService, VIEW_TYPE, STATUS) {

        let vm = this;
        orms.eec = this;

        let datePickerDefaultOptions = {
            language: 'hr',
            autoclose: true,
            format: 'dd.mm.yyyy.',
            orientation: 'bottom auto'
        };

        const $dateOfBirth = $document.find('#dateOfBirth');
        const $sanitaryIdCreatedDate = $document.find('#sanitaryIdCreatedDate');
        const $sanitaryIdExpirationDate = $document.find('#sanitaryIdExpirationDate');

        let _dateOfBirth, _sanitaryIdCreatedDate, _sanitaryIdExpirationDate;

        vm.offices = offices;
        vm.roles = roles;
        vm.worker = worker;

        vm.save = save;
        vm.goBack = goBack;

        vm.isEdit = isEdit;
        vm.isCreate = isCreate;
        vm.clearSanitaryIdDates = clearSanitaryIdDates;

        init();

        function init() {
            if (angular.isDefined(worker) && worker.active === STATUS.DISABLED) {
                $location.path('/employees');
            }

            $dateOfBirth.datepicker(datePickerDefaultOptions);
            $sanitaryIdCreatedDate.datepicker(datePickerDefaultOptions);
            $sanitaryIdExpirationDate.datepicker(datePickerDefaultOptions);

            $dateOfBirth.datepicker().on('changeDate', function (e) {
                _dateOfBirth = e.date;
            });

            $sanitaryIdCreatedDate.datepicker().on('changeDate', function (e) {
                _sanitaryIdCreatedDate = e.date;
            });

            $sanitaryIdExpirationDate.datepicker().on('changeDate', function (e) {
                _sanitaryIdExpirationDate = e.date;
            });
        }

        function save() {
            getDates();
            let data = {
                worker: vm.worker
            };

            let saveFunction = angular.isDefined(vm.worker.id) ? userService.updateWorker : userService.addWorker;
            $alert.async(saveFunction(data), { showFeedback: true })
                .then(function (data) {
                    if (data.success) {
                        if (vm.isCreate()) {
                            $location.path(`/employees/${data.data}/edit`);
                        } else {
                            $route.reload();
                        }

                    }
                });

            function getDates() {
                if (angular.isDefined(_dateOfBirth)) {
                    vm.worker.dateOfBirth = _dateOfBirth;
                }

                if (angular.isDefined(_sanitaryIdCreatedDate)) {
                    vm.worker.sanitaryIdCreatedDate = _sanitaryIdCreatedDate;
                }

                if (angular.isDefined(_sanitaryIdExpirationDate)) {
                    vm.worker.sanitaryIdExpirationDate = _sanitaryIdExpirationDate;
                }
            }
        }

        function goBack() {
            $location.path('/employees').replace();
        }

        function isEdit() {
            return viewType === VIEW_TYPE.EDIT;
        }

        function isCreate() {
            return viewType === VIEW_TYPE.CREATE;
        }

        function clearSanitaryIdDates() {
            if (vm.worker.sanitaryIdVerified === 0) {
                vm.worker.sanitaryIdCreatedDate = undefined;
                vm.worker.sanitaryIdExpirationDate = undefined;
            }
        }

    };

    orms.app
        .controller('employeeEditController', employeeEditController);

})(orms);