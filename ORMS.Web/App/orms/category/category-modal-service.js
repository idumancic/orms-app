﻿(function (orms) {

    'use strict';

    const categoryModalService = function () {

        let data = {
            categories: []
        };

        return {
            init: init,
            data: data
        };

        function init(categories) {
            data.categories = categories;
        }

    };

    orms.app
        .service('categoryModalService', categoryModalService);

})(orms);