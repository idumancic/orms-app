﻿(function (orms) {

    'use strict';

    const categoryService = function ($http, API_PREFIX) {

        const url = API_PREFIX + '/category';

        return {
            getAll: getAll,
            add: add,
            update: update
        };

        function getAll() {
            return $http.get(url);
        }

        function add(category) {
            return $http.post(url + '/add', category);
        }

        function update(category) {
            return $http.post(url + '/update', category);
        }

    };

    orms.app
        .service('categoryService', categoryService);

})(orms);