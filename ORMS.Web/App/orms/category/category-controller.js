﻿(function (orms) {

    'use strict';

    const categoryController = function ($rootScope, $document, $window, $alert, confirmModalService, categoryService, categoryModalService, STATUS) {

        let vm = this;
        orms.cc = this;

        vm.addCategory = addCategory;
        vm.editCategory = editCategory;
        vm.removeCategory = removeCategory;

        vm.categories = categoryModalService.data.categories;

        function addCategory(category) {
            $alert
                .async(categoryService.add(category), { showFeedback: true })
                .then(function (data) {
                    if (data.success) {
                        vm.categoryAddModal.hide();
                        $window.location.reload();
                    }
                });
        }

        function editCategory(category) {
            $alert
                .async(categoryService.update(category), { showFeedback: true })
                .then(function (data) {
                    if (data.success) {
                        vm.categoryEditModal.hide();
                        $window.location.reload();
                    }
                });
        }

        function removeCategory(category) {
            vm.categoryEditModal.invisible();
            confirmModalService
                .show({ calledFromModal: true })
                .then(function (result) {
                    if (result) {
                        category.active = STATUS.DISABLED;
                        $alert
                            .async(categoryService.update(category), { showFeedback: true })
                            .then(function (data) {
                                if (data.success) {
                                    vm.categoryEditModal.hide();
                                    $window.location.reload();
                                }
                            });
                    } else {
                        vm.categoryEditModal.visible();
                    }
                });
        }

        $rootScope.$on("$includeContentLoaded", function (event, templateName) {
            vm.categoryEditModal = $document.find('div#categoryEditModal');
            vm.categoryAddModal = $document.find('div#categoryAddModal');

            vm.categoryEditModal.hide = function () {
                vm.categoryEditModal.modal('hide');
            };

            vm.categoryEditModal.invisible = function () {
                vm.categoryEditModal.addClass('modal-hide');
            };

            vm.categoryEditModal.visible = function () {
                vm.categoryEditModal.removeClass('modal-hide');
            };
        });


    };

    orms.app
        .controller('categoryController', categoryController);

})(orms);