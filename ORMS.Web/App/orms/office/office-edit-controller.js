﻿(function (orms) {

    'use strict';

    const officeEditController = function ($log, $route, $location, $alert, viewType, office, confirmModalService, officeService, VIEW_TYPE, STATUS) {

        let vm = this;
        orms.oec = this;

        vm.goBack = goBack;
        vm.office = office;

        vm.save = save;
        vm.remove = remove;

        vm.isEdit = isEdit;
        vm.isCreate = isCreate;

        init();

        function init() {
            if (angular.isDefined(office) && office.active === STATUS.DISABLED) {
                $location.path('/offices');
            }
        }

        function save() {
            let data = {
                office: vm.office
            };

            let saveFunction = angular.isDefined(vm.office.id) ? officeService.update : officeService.add;
            $alert.async(saveFunction(data), { showFeedback: true })
                .then(function (data) {
                    if (data.success) {
                        if (vm.isCreate()) {
                            $location.path(`/offices/${data.data}/edit`);
                        } else {
                            $route.reload();
                        }
                    }
                });
        }

        function remove() {
            confirmModalService
                .show()
                .then(function (result) {
                    if (result) {
                        let data = {
                            office: {
                                id: office.id,
                                active: STATUS.DISABLED
                            }
                        };

                        $alert.async(officeService.update(data), { showFeedback: true })
                            .then(function (data) {
                                if (data.success) {
                                    $route.reload();
                                }
                            });
                    }
                });
        }

        function goBack() {
            $location.path('/offices').replace();
        }

        function isEdit() {
            return viewType === VIEW_TYPE.EDIT;
        }

        function isCreate() {
            return viewType === VIEW_TYPE.CREATE;
        }

    };

    orms.app
        .controller('officeEditController', officeEditController);

})(orms);
