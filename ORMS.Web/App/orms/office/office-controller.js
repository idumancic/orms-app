﻿(function (orms) {

    'use strict';

    const officeController = function ($location, offices) {

        let vm = this;
        orms.oc = this;

        vm.offices = offices;

        vm.goAdd = goAdd;
        vm.goEdit = goEdit;

        function goAdd() {
            $location.path('/offices/add');
        }

        function goEdit(officeId) {
            $location.path(`/offices/${officeId}/edit`);
        }

    };

    orms.app
        .controller('officeController', officeController);

})(orms);