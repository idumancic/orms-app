﻿(function (orms) {

    'use strict';

    orms.app
        .config(function ($routeProvider, FOLDER_PATH, NAVIGATION, VIEW_TYPE) {

            $routeProvider
                .when('/offices', {
                    templateUrl: FOLDER_PATH.ORMS + 'office/templates/offices.html',
                    controller: 'officeController',
                    controllerAs: 'oc',
                    title: NAVIGATION.OFFICES.title,
                    activeTab: NAVIGATION.OFFICES,
                    resolve: {
                        offices: ['$alert', 'officeService', function ($alert, officeService) {
                            return $alert.async(officeService.getAllEdit()).then(function (data) {
                                if (data.success) {
                                    return data.offices;
                                }
                            });
                        }]
                    }
                })
                .when('/offices/add', {
                    templateUrl: FOLDER_PATH.ORMS + 'office/templates/office-edit.html',
                    controller: 'officeEditController',
                    controllerAs: 'oec',
                    title: NAVIGATION.OFFICES.title,
                    viewType: VIEW_TYPE.CREATE,
                    activeTab: NAVIGATION.OFFICES,
                    resolve: {
                        viewType: ['$route', function ($route) {
                            return $route.current.viewType;
                        }],
                        office: ['$q', function ($q) {
                            return $q.resolve(undefined);
                        }]
                    }
                })
                .when('/offices/:id/edit', {
                    templateUrl: FOLDER_PATH.ORMS + 'office/templates/office-edit.html',
                    controller: 'officeEditController',
                    controllerAs: 'oec',
                    title: NAVIGATION.OFFICES.title,
                    viewType: VIEW_TYPE.EDIT,
                    activeTab: NAVIGATION.OFFICES,
                    resolve: {
                        viewType: ['$route', function ($route) {
                            return $route.current.viewType;
                        }],
                        office: ['$alert', '$route', 'officeService', function ($alert, $route, officeService) {
                            return $alert.async(officeService.get($route.current.params.id)).then(function (data) {
                                if (data.success) {
                                    return data.office;
                                }
                            });
                        }]
                    }
                });

        });

})(orms);