﻿(function (orms) {

    'use strict';

    const officeService = function ($http, API_PREFIX) {

        const url = API_PREFIX + '/office';

        return {
            getAll: getAll,
            getAllEdit: getAllEdit,
            get: get,
            add: add,
            update: update
        };

        function getAll() {
            return $http.get(url + '/getall');
        }

        function getAllEdit() {
            return $http.get(url + '/getalledit');
        }

        function get(officeId) {
            return $http.get(url + '/getoffice', {
                params: {
                    officeId: officeId
                }
            });
        }

        function add(office) {
            return $http.post(url + '/add', office);
        }

        function update(office) {
            return $http.post(url + '/update', office);
        }

    };

    orms.app
        .service('officeService', officeService);

})(orms);