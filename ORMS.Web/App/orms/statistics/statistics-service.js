﻿(function (orms) {

    'use strict';

    const statisticService = function ($http, API_PREFIX) {

        const url = API_PREFIX + '/statistics';

        return {
            getYearlyEarnings: getYearlyEarnings,
            getMonthlyEarnings: getMonthlyEarnings,
            getTop10Items: getTop10Items,
            getShiftEarnings: getShiftEarnings
        };

        function getYearlyEarnings() {
            return $http.get(url + '/yearlyearnings');
        }

        function getMonthlyEarnings() {
            return $http.get(url + '/monthlyearnings');
        }

        function getTop10Items() {
            return $http.get(url + '/top10items');
        }

        function getShiftEarnings() {
            return $http.get(url + '/shiftearnings');
        }

    };

    orms.app
        .service('statisticService', statisticService);


})(orms);