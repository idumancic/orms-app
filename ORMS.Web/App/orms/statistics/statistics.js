﻿(function (orms) {

    'use strict';

    orms.app
        .config(function ($routeProvider, FOLDER_PATH, NAVIGATION) {

            $routeProvider
                .when('/statistics', {
                    templateUrl: FOLDER_PATH.ORMS + 'statistics/templates/statistics.html',
                    controller: 'statisticsController',
                    controllerAs: 'sc',
                    title: NAVIGATION.STATISTICS.title,
                    activeTab: NAVIGATION.STATISTICS,
                    resolve: {
                        statistics: ['$q', '$log', '$alert', 'statisticService', function ($q, $log, $alert, statisticService) {
                            let promises = $q.all({
                                yearlyEarnings: statisticService.getYearlyEarnings(),
                                monthlyEarnings: statisticService.getMonthlyEarnings(),
                                shiftEarnings: statisticService.getShiftEarnings(),
                                top10Items: statisticService.getTop10Items()
                            });

                            return $alert.async(promises).then(function (response) {
                                return {
                                    yearlyEarnings: response.yearlyEarnings.data.data,
                                    monthlyEarnings: response.monthlyEarnings.data.data,
                                    shiftEarnings: response.shiftEarnings.data.data,
                                    top10Items: response.top10Items.data.data
                                };
                            });
                        }]
                    }
                });

        });

})(orms);