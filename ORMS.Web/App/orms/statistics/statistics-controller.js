﻿(function (orms) {

    'use strict';

    const statisticsController = function ($alert, statistics) {

        let vm = this;
        orms.sc = this;

        const ORMS_BLUE_COLOR = ["#17a2b8"];
        const MONTH_LABELS = ["Siječanj", "Veljača", "Ožujak", "Travanj", "Svibanj", "Lipanj", "Srpanj", "Kolovoz", "Rujan", "Listopad", "Studeni", "Prosinac"];

        vm.colors = ORMS_BLUE_COLOR;
        vm.yearlyEarnings = statistics.yearlyEarnings;
        vm.monthlyEarnings = statistics.monthlyEarnings;
        vm.top10Items = statistics.top10Items;
        vm.shiftEarnings = statistics.shiftEarnings;

        vm.barLabels = vm.yearlyEarnings.labels;
        vm.barSeries = vm.yearlyEarnings.series;
        vm.barData = [vm.yearlyEarnings.earnings];

        vm.itemLabels = vm.top10Items.labels;
        vm.itemData = vm.top10Items.earnings;

        vm.shiftLabels = vm.shiftEarnings.labels;
        vm.shiftData = vm.shiftEarnings.earnings;

        vm.linearSeries = ['Ukupna zarada'];
        vm.linearLabels = MONTH_LABELS;
        vm.linearData = [
            vm.monthlyEarnings.earnings
        ];
        vm.linearDatasetOverride = [{ yAxisID: 'y-axis-1' }, { yAxisID: 'y-axis-2' }];
        vm.linearOptions = {
            scales: {
                yAxes: [
                    {
                        id: 'y-axis-1',
                        type: 'linear',
                        display: true,
                        position: 'left'
                    },
                    {
                        id: 'y-axis-2',
                        type: 'linear',
                        display: true,
                        position: 'right'
                    }
                ]
            }
        };

    };

    orms.app
        .controller('statisticsController', statisticsController);

})(orms);