﻿(function (orms) {

    'use strict';

    const userService = function ($http, API_PREFIX) {

        const url = API_PREFIX + '/user';

        return {
            getWorkers: getWorkers,
            getWorkersEdit: getWorkersEdit,
            getWorker: getWorker,
            addWorker: addWorker,
            updateWorker: updateWorker
        };

        function getWorkers() {
            return $http.get(url + '/workers');
        }

        function getWorkersEdit() {
            return $http.get(url + '/workersedit');
        }

        function getWorker(workerId) {
            return $http.get(url + '/worker', {
                params: {
                    workerId: workerId
                }
            });
        }

        function addWorker(worker) {
            return $http.post(url + '/addworker', worker);
        }

        function updateWorker(worker) {
            return $http.post(url + '/updateworker', worker);
        }
    };

    orms.app
        .service('userService', userService);

})(orms);