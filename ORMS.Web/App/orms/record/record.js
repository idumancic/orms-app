﻿(function (orms) {

    'use strict';

    orms.app
        .config(function ($routeProvider, FOLDER_PATH, NAVIGATION, VIEW_TYPE) {

            $routeProvider
                .when('/records', {
                    templateUrl: FOLDER_PATH.ORMS + 'record/templates/records.html',
                    controller: 'recordController',
                    controllerAs: 'rc',
                    title: NAVIGATION.RECORDS.title,
                    activeTab: NAVIGATION.RECORDS,
                    resolve: {
                        workers: ['userService', '$alert', function (userService, $alert) {
                            return $alert.async(userService.getWorkers()).then(function (data) {
                                if (data.success) {
                                    return data.workers;
                                }
                            });

                        }]
                    }
                })
                .when('/records/add', {
                    templateUrl: FOLDER_PATH.ORMS + 'record/templates/record-edit.html',
                    controller: 'recordEditController',
                    controllerAs: 'rec',
                    title: NAVIGATION.RECORDS.title,
                    activeTab: NAVIGATION.RECORDS,
                    viewType: VIEW_TYPE.CREATE,
                    resolve: {
                        viewType: ['$route', function ($route) {
                            return $route.current.viewType;
                        }],
                        workers: ['userService', function (userService) {
                            return userService.getWorkers().then(function (response) {
                                let data = response.data;
                                if (data.success) {
                                    return data.workers;
                                }
                            });
                        }],
                        offices: ['officeService', function (officeService) {
                            return officeService.getAll().then(function (response) {
                                let data = response.data;
                                if (data.success) {
                                    return data.offices;
                                }
                            });
                        }],
                        info: ['$q', function ($q) {
                            return $q.resolve({});
                        }],
                        itemCount: ['itemService', function (itemService) {
                            return itemService.getTotalCount().then(function (response) {
                                let data = response.data;
                                if (data.success) {
                                    return response.data.data;
                                }
                            });
                        }]
                    }
                })
                .when('/records/:id/edit', {
                    templateUrl: FOLDER_PATH.ORMS + 'record/templates/record-edit.html',
                    controller: 'recordEditController',
                    controllerAs: 'rec',
                    title: NAVIGATION.RECORDS.title,
                    activeTab: NAVIGATION.RECORDS,
                    viewType: VIEW_TYPE.EDIT,
                    resolve: {
                        viewType: ['$route', function ($route) {
                            return $route.current.viewType;
                        }],
                        workers: ['userService', function (userService) {
                            return userService.getWorkers().then(function (response) {
                                let data = response.data;
                                if (data.success) {
                                    return data.workers;
                                }
                            });
                        }],
                        offices: ['officeService', function (officeService) {
                            return officeService.getAll().then(function (response) {
                                let data = response.data;
                                if (data.success) {
                                    return data.offices;
                                }
                            });
                        }],
                        info: ['recordService', '$route', function (recordService, $route) {
                            return recordService.getInfo($route.current.params.id).then(function (response) {
                                let data = response.data;
                                if (data.success) {
                                    return data.record;
                                }
                            });
                        }],
                        itemCount: ['recordService', '$route', function (recordService, $route) {
                            return recordService.getRecordItemsTotalCount($route.current.params.id).then(function (response) {
                                let data = response.data;
                                if (data.success) {
                                    return response.data.data;
                                }
                            });
                        }]
                    }
                })
                .when('/records/:id/show', {
                    templateUrl: FOLDER_PATH.ORMS + 'record/templates/record-show.html',
                    controller: 'recordShowController',
                    controllerAs: 'rsc',
                    title: NAVIGATION.RECORDS.title,
                    activeTab: NAVIGATION.RECORDS,
                    viewType: VIEW_TYPE.SHOW,
                    resolve: {
                        recordItems: ['$alert', '$route', 'recordService', function ($alert, $route, recordService) {
                            return $alert
                                .async(recordService.getRecordItems($route.current.params.id))
                                .then(function (data) {
                                    if (data.success) {
                                        return data.data;
                                    }
                                });
                        }],
                        workers: ['userService', function (userService) {
                            return userService.getWorkers().then(function (response) {
                                let data = response.data;
                                if (data.success) {
                                    return data.workers;
                                }
                            });
                        }],
                        offices: ['officeService', function (officeService) {
                            return officeService.getAll().then(function (response) {
                                let data = response.data;
                                if (data.success) {
                                    return data.offices;
                                }
                            });
                        }],
                        info: ['recordService', '$route', function (recordService, $route) {
                            return recordService.getInfo($route.current.params.id).then(function (response) {
                                let data = response.data;
                                if (data.success) {
                                    return data.record;
                                }
                            });
                        }]
                    }
                });

        });

})(orms);