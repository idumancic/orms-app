﻿(function (orms) {

    'use strict';

    const recordController = function ($rootScope, $log, $document, $location, workers, $alert, recordService, authService) {

        let vm = this;
        orms.rc = this;

        const $fromDate = $document.find('.input-daterange input:eq(0)');
        const $toDate = $document.find('.input-daterange input:eq(1)');
        const $recordsDatepicker = $document.find('.input-daterange');

        let defaultDatepickerOptions = {
            language: 'hr',
            todayBtn: "linked",
            clearBtn: true,
            todayHighlight: true,
            format: 'dd.mm.yyyy.',
            orientation: 'bottom-left'
        };

        let defaultSearchParams = {
            fromDate: undefined,
            toDate: undefined,
            workerId: undefined
        };

        let defaultPagingOptions = {
            pageNumber: 1,
            pageSize: 20,
            total: 0
        };

        let fromDate_dpValue = undefined;
        let toDate_dpValue = undefined;

        vm.records = [];
        vm.workers = workers;
        vm.paging = angular.extend({}, defaultPagingOptions);
        vm.searchParams = angular.extend({}, defaultSearchParams);

        vm.goAddNew = goAddNew;
        vm.goEdit = goEdit;
        vm.goShow = goShow;
        vm.goBack = goBack;
        vm.goToPage = goToPage;
        vm.search = search;

        vm.isWorker = authService.isWorker;
        vm.isManager = authService.isManager;
        vm.searchDisabled = searchDisabled;
        vm.resetSearch = resetSearch;

        init();
        function init() {
            initDatepicker();
            vm.goToPage(defaultPagingOptions.pageNumber);
            vm.searchParamsCache = angular.extend({}, defaultSearchParams);
        }

        function initDatepicker() {
            $recordsDatepicker.datepicker(defaultDatepickerOptions);
            $fromDate.datepicker().on('changeDate', function (e) {
                fromDate_dpValue = e.date;
            });

            $toDate.datepicker().on('changeDate', function (e) {
                toDate_dpValue = e.date;
            });
        }

        function goToPage(page) {
            let data = {
                pageNumber: page,
                pageSize: vm.paging.pageSize
            };

            if (angular.isDefined(vm.searchParamsCache)) {
                data = angular.extend({}, data, vm.searchParamsCache);
            }

            $alert
                .async(recordService.getPage(data))
                .then(function (data) {
                    if (data.success) {
                        vm.records = data.records;
                        vm.paging.total = data.totalCount;
                    }
                });
        }

        function search(searchParams) {
            let data = angular.extend({}, defaultPagingOptions, searchParams);
            vm.searchParamsCache = angular.copy(searchParams);

            if (angular.isDefined(searchParams.fromDate) && angular.isDefined(searchParams.toDate)) {
                data.fromDate = fromDate_dpValue;
                data.toDate = toDate_dpValue;
            }

            $alert
                .async(recordService.search(data))
                .then(function (data) {
                    if (data.success) {
                        vm.records = data.records;
                        vm.paging.total = data.totalCount;
                    }
                });
        }

        function goEdit(recordId) {
            $location.path(`/records/${recordId}/edit`);
        }

        function goShow(recordId) {
            $location.path(`/records/${recordId}/show`);
        }

        function goAddNew() {
            $location.path("/records/add");
        }

        function goBack() {
            $location.path('/records').replace();
        }

        function resetSearch() {
            vm.searchParams = angular.copy({}, defaultSearchParams);
            vm.searchParamsCache = angular.copy({}, defaultSearchParams);
            vm.goToPage(defaultPagingOptions.pageNumber);
        }

        function searchDisabled() {
            let fromDate = vm.searchParams.fromDate;
            let toDate = vm.searchParams.toDate;
            let worker = vm.searchParams.workerId;

            let fromDateUndefined = angular.isUndefined(fromDate) || fromDate === "";
            let toDateUndefined = angular.isUndefined(toDate) || toDate === "";
            let workerUndefined = angular.isUndefined(worker) || worker === null;

            return fromDateUndefined && toDateUndefined && workerUndefined;
        }

    };

    orms.app
        .controller('recordController', recordController);

})(orms);