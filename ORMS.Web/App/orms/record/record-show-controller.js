﻿(function (orms) {

    'use strict';

    const recordShowController = function ($log, $location, $routeParams, $alert, workers, offices, info, recordItems, recordService) {

        let vm = this;
        orms.rsc = this;

        vm.recordItems = recordItems;
        vm.workers = workers;
        vm.offices = offices;
        vm.info = info;

        vm.goBack = goBack;
        vm.downloadCSV = downloadCSV;

        init();

        function init() {

            angular.forEach(workers, function (worker) {
                if (info.firstShift.workerId === worker.id) {
                    vm.firstShiftWorkerName = worker.fullName;
                } else if (info.secondShift.workerId === worker.id) {
                    vm.secondShiftWorkerName = worker.fullName;
                }
            });

            angular.forEach(offices, function (office) {
                if (info.officeId === office.id) {
                    vm.officeName = office.name;
                }
            });

        }

        function goBack() {
            $location.path('/records').replace();
        }

        function downloadCSV() {
            let data = {
                recordId: $routeParams.id,
                dateCreated: info.dateCreated
            };

            recordService.getCSV(data).then(function (response) {
                const contentDispositionHeaderValue = response.headers('Content-Disposition');
                const regex = /(filename)=((\w+).(\w+))/g;
                let fileName = regex.exec(contentDispositionHeaderValue)[2];

                // TODO: find a better format to send back
                // web api is returning string value of base64 string so we have to remove quotes 
                let data = response.data.substring(1, response.data.length - 1);
                let dataUrl = 'data:application/octet-stream;base64,' + data;
                let link = document.createElement('a');
                angular.element(link)
                    .attr('href', dataUrl)
                    .attr('download', fileName)
                    .attr('target', '_blank');
                link.click();

            });
        }


    };

    orms.app
        .controller('recordShowController', recordShowController);

})(orms);