﻿(function (orms) {

    'use strict';

    const recordEditController = function ($log, $scope, $location, $document, $routeParams, $alert, viewType, workers, offices, info, STATUS,
        VIEW_TYPE, HTTP_STATUS_CODE, confirmModalService, recordService, ormsStorageService, itemService, itemCount) {

        let vm = this;
        orms.rec = this;

        const DATE_CREATED_CHANGED_VALIDATION_MSG = "Promijenili ste datum liste! Osvježite stanje od jučer.";
        const RECORD_LIST_ITEMS_UNDEFINED_MSG = "Niste popunili sva polja na svim stranicama";

        let defaultPagingOptions = {
            pageNumber: 1,
            pageSize: 50,
            total: itemCount
        };

        let pulledYesterdaysState = false;
        let yesterdaysStateOfTheBar = [];

        vm.recordItems = [];
        vm.workers = workers;
        vm.offices = offices;
        vm.info = info;

        vm.save = save;
        vm.remove = remove;

        vm.goToPage = goToPage;
        vm.goBack = goBack;

        vm.isCreate = isCreate;
        vm.isEdit = isEdit;

        vm.getYesterdaysStateOfTheBar = getYesterdaysStateOfTheBar;

        vm.updateTotalEarnings = updateTotalEarnings;
        vm.updateSoldOut = updateSoldOut;
        vm.updateInTotal = updateInTotal;
        vm.updateSumOfSoldArticlesPrice = updateSumOfSoldArticlesPrice;

        init();

        function init() {
            if (angular.isDefined(info) && info.active === STATUS.DISABLED) {
                $location.path('/records');
            }

            vm.paging = angular.extend({}, defaultPagingOptions);
            vm.goToPage(defaultPagingOptions.pageNumber);
            ormsStorageService.removeRecordItems();

            if (vm.isCreate()) {
                const $recordDateCreated = $document.find('button#recordDateCreated');
                $recordDateCreated.datepicker({
                    language: 'hr',
                    autoclose: true,
                    todayBtn: "linked",
                    todayHighlight: true,
                    orientation: 'bottom auto'
                });

                $recordDateCreated.datepicker().on('changeDate', function (e) {
                    yesterdaysStateOfTheBar = [];

                    // clears yesterdays state in cache and updates view to the current state of the cache if user changes date of creation
                    if (angular.isDefined(vm.info.dateCreated) && yesterdaysStateOfTheBar.length < 1 && pulledYesterdaysState) {
                        $alert.showValidation(DATE_CREATED_CHANGED_VALIDATION_MSG);
                        pulledYesterdaysState = false;
                        ormsStorageService.clearYesterdaysState();
                        vm.recordItems = ormsStorageService.getRecordItems(vm.paging.pageNumber);
                    }

                    vm.info.dateCreated = e.date;
                    $scope.$apply();
                });
            }
        }

        function goToPage(page) {
            let data = {
                pageNumber: page,
                pageSize: vm.paging.pageSize,
                recordId: $routeParams.id
            };

            // check if there is existing previous page and update cached content
            if (ormsStorageService.checkIfRecordItemPageExists(vm.paging.pageNumber) && vm.recordItems.length > 0) {
                ormsStorageService.setRecordItems(vm.paging.pageNumber, vm.recordItems);
            }

            // check if there is current page in cache and set record items to that value
            if (ormsStorageService.checkIfRecordItemPageExists(page)) {
                let cachedRecordItems = ormsStorageService.getRecordItems(page);
                vm.recordItems = cachedRecordItems;
            }
            // if there are no previous pages to update and no current pages to set content to -> return data from server
            else {
                let getItemFunction = angular.isDefined(data.recordId) ? recordService.getItems : itemService.getPage;
                $alert
                    .async(getItemFunction(data))
                    .then(function (data) {
                        if (data.success) {
                            vm.recordItems = data.recordItems || data.items;
                            vm.paging.total = data.totalCount;
                            return vm.recordItems;
                        }
                    })
                    .then(function (items) {
                        if (vm.isCreate()) {
                            // if its creating of new record prepare items to match the view model in the template
                            recordService
                                .prepareItems(items, yesterdaysStateOfTheBar)
                                .then(function (items) {
                                    vm.recordItems = items;
                                    ormsStorageService.setRecordItems(page, vm.recordItems);
                                });
                        } else {
                            ormsStorageService.setRecordItems(page, items);
                        }
                    });
            }
        }

        function save() {
            let record = {
                info: vm.info,
                recordItems: [],
                totalRecordItems: vm.paging.total
            };

            // check if there is current page and update cached content
            if (ormsStorageService.checkIfRecordItemPageExists(vm.paging.pageNumber)) {
                ormsStorageService.setRecordItems(vm.paging.pageNumber, vm.recordItems);
            }

            // if there are record items in cache add them to the array of record items if not just take the view model property
            let cachedRecordItems = ormsStorageService.getRecordItems();
            if (angular.isDefined(cachedRecordItems) && cachedRecordItems !== null) {
                for (var key in cachedRecordItems) {
                    record.recordItems = record.recordItems.concat(cachedRecordItems[key]);
                }
            } else {
                record.recordItems = vm.recordItems;
            }

            recordService
                .validate(record)
                .then(function (valid) {
                    if (valid) {
                        let saveFunction = angular.isDefined(record.info.id) ? recordService.update : recordService.add;

                        $alert
                            .async(saveFunction(record), { showFeedback: true })
                            .then(function (data) {
                                if (data.success) {
                                    if (vm.isCreate() && data.record) {
                                        $location.path(`/records/${data.record.id}/edit`);
                                    }
                                }
                            });

                    } else {
                        $alert.showValidation(RECORD_LIST_ITEMS_UNDEFINED_MSG);
                    }
                });
        }

        function remove() {
            confirmModalService
                .show()
                .then(function (result) {
                    if (result) {
                        vm.info.active = STATUS.DISABLED;
                        $alert
                            .async(recordService.update({ info: vm.info }), { showFeedback: true })
                            .then(function (data) {
                                if (data.success) {
                                    $location.path('/records').replace();
                                }
                            });
                    }
                });
        }

        function getYesterdaysStateOfTheBar() {
            $alert
                .async(recordService.getYesterdaysStateOfTheBar(vm.info.dateCreated), { showFeedback: true })
                .then(function (data) {
                    if (data.success) {
                        yesterdaysStateOfTheBar = data.data;
                        pulledYesterdaysState = true;
                        return yesterdaysStateOfTheBar;
                    }
                })
                .then(function (yesterdaysState) {
                    recordService
                        .prepareItems(vm.recordItems, yesterdaysState)
                        .then(function (items) {
                            vm.recordItems = items;

                            // check if there is current page and update cached content
                            if (ormsStorageService.checkIfRecordItemPageExists(vm.paging.pageNumber)) {
                                ormsStorageService.setRecordItems(vm.paging.pageNumber, vm.recordItems);
                            }
                        });
                });
        }

        function isCreate() {
            return viewType === VIEW_TYPE.CREATE;
        }

        function isEdit() {
            return viewType === VIEW_TYPE.EDIT;
        }

        function updateTotalEarnings() {
            let firstShift = vm.info.firstShift ? vm.info.firstShift.shiftEarnings || 0 : 0;
            let secondShift = vm.info.secondShift ? vm.info.secondShift.shiftEarnings || 0 : 0;

            vm.info.totalEarnings = firstShift + secondShift;
        }

        function updateInTotal(recordItem) {
            let yesterdaysStateOfTheBar = recordItem.yesterdaysStateOfTheBar || 0;
            let received = recordItem.received || 0;

            recordItem.inTotal = yesterdaysStateOfTheBar + received;
            updateSoldOut(recordItem);
        }

        function updateSoldOut(recordItem) {
            let inTotal = recordItem.inTotal || 0;
            let todaysStateOfTheBar = recordItem.todaysStateOfTheBar || 0;

            recordItem.soldOut = inTotal - todaysStateOfTheBar;

            if (recordItem.soldOut < 0 || todaysStateOfTheBar === 0)
                recordItem.soldOut = 0;

            if (todaysStateOfTheBar === 0 && inTotal > 0) {
                recordItem.soldOut = inTotal;
            }

            updateSumOfSoldArticlesPrice(recordItem);
        }

        function updateSumOfSoldArticlesPrice(recordItem) {
            let soldOut = recordItem.soldOut || 0;
            recordItem.sumOfSoldArticlesPrice = soldOut * recordItem.item.price;
        }

        function goBack() {
            $location.path('/records').replace();
            ormsStorageService.removeRecordItems();
        }

    };

    orms.app
        .controller('recordEditController', recordEditController);

})(orms);