﻿(function (orms) {

    'use strict';

    const recordService = function ($q, $http, API_PREFIX) {

        const url = API_PREFIX + '/record';

        return {
            getPage: getPage,
            getItems: getItems,
            getInfo: getInfo,
            search: search,
            add: add,
            update: update,
            getYesterdaysStateOfTheBar: getYesterdaysStateOfTheBar,
            getRecordItemsTotalCount: getRecordItemsTotalCount,
            getRecordItems: getRecordItems,
            getCSV: getCSV,
            prepareItems: prepareItems,
            validate: validate
        };

        function getPage(data) {
            return $http.get(url + '/page', {
                params: {
                    pageNumber: data.pageNumber,
                    pageSize: data.pageSize,
                    orderBy: data.orderBy
                }
            });
        }

        function getItems(data) {
            return $http.get(url + '/items', {
                params: {
                    pageNumber: data.pageNumber,
                    pageSize: data.pageSize,
                    orderBy: data.orderBy,
                    recordId: data.recordId
                }
            });
        }

        function getInfo(recordId) {
            return $http.get(url + '/info', {
                params: {
                    recordId: recordId
                }
            });
        }

        function search(data) {
            return $http.post(url + '/search', data);
        }

        function add(record) {
            return $http.post(url + '/add', record);
        }

        function update(record) {
            return $http.post(url + '/update', record);
        }

        function getYesterdaysStateOfTheBar(dateCreated) {
            return $http.get(url + '/yesterdaysstate', {
                params: {
                    dateCreated: dateCreated
                }
            });
        }

        function getRecordItemsTotalCount(recordId) {
            return $http.get(url + '/recorditemscount', {
                params: {
                    recordId: recordId
                }
            });
        }

        function getRecordItems(recordId) {
            return $http.get(url + '/itemsshow', {
                params: {
                    recordId: recordId
                }
            });
        }

        function getCSV(data) {
            return $http.get(url + '/getcsv', {
                params: {
                    recordId: data.recordId,
                    dateCreated: data.dateCreated
                }
            });
        }

        function prepareItems(items, yesterdaysState) {
            let deferred = $q.defer();
            let recordItems = [];
            let _yesterdaysState = [];

            /*
                Checks if yesterdaysState param is defined if it is checks if the array has states and if its bigger then items size.
                Ff all of the above conditions are true it slices the yesterdaysState array to the beginning size of items array since
                we are getting items in pages and yesterdaysState in one pull from the server. If startRowIndex has value 0 then it means its
                first page and we don't have to slice anything, otherwise we slice.
            */
            if (angular.isDefined(yesterdaysState) && yesterdaysState.length > 0 && items.length < yesterdaysState.length) {
                let startRowIndex = items[0].rowIndex;
                if (startRowIndex > 0) {
                    _yesterdaysState = yesterdaysState.slice(startRowIndex, yesterdaysState.length);
                } else {
                    _yesterdaysState = yesterdaysState;
                }
            } else {
                _yesterdaysState = yesterdaysState || [];
            }

            angular.forEach(items, function (_item, index) {
                let recordItem = {
                    rowIndex: _item.rowIndex,
                    received: _item.received || undefined,
                    todaysStateOfTheBar: _item.todaysStateOfTheBar || undefined,
                    inTotal: _item.inTotal || 0,
                    soldOut: _item.soldOut || 0,
                    sumOfSoldArticlesPrice: _item.sumOfSoldArticlesPrice || 0
                };

                /*
                   We check if item is defined because on pulling of yesterdays state we already have items in prepared state
                   for the view. If there are not defined we set the items to proper object.
                */
                if (angular.isUndefined(_item.item)) {
                    recordItem.item = {
                        id: _item.id,
                        name: _item.name,
                        price: _item.price
                    };
                } else {
                    recordItem.item = _item.item;
                }

                /*
                   If yesterdays state is pulled and it has states in the array, we assign that values to the proper property
                   in items array. If not we assign an undefined value.
                */
                if (_yesterdaysState.length > 0) {
                    if (recordItem.rowIndex === _yesterdaysState[index].rowIndex) {
                        recordItem.yesterdaysStateOfTheBar = yesterdaysState[index].yesterdaysStateOfTheBar;
                    }
                } else {
                    recordItem.yesterdaysStateOfTheBar = undefined;
                }

                recordItems.push(recordItem);
                if (recordItems.length === items.length) {
                    deferred.resolve(recordItems);
                }
            });

            return deferred.promise;
        }

        function validate(record) {
            let deferred = $q.defer();
            let recordItems = record.recordItems;

            let _isValid = true;

            /*
               We are using this type of breaking of loop because javascript is throwing angular
               foreach error when trying to break loop with break statement.
               Error: Illegal statement.
            */
            let _isBreaked = false;

            /*
                We are checking if all the required entry fields on all pages have values.

                User case:
                We left one field blank on one of the pages and if go to the other page it wouldn't throw field required validation
                with this checking we are making sure it does through validation message if all fields are not inputed.

                TODO: mark the inputs which don't have inputed value.
            */
            angular.forEach(recordItems, function (recordItem, index) {
                if (angular.isUndefined(recordItem.yesterdaysStateOfTheBar) ||
                    angular.isUndefined(recordItem.received) ||
                    angular.isUndefined(recordItem.todaysStateOfTheBar)) {
                    _isValid = false;
                    _isBreaked = true;
                }

                if (!_isValid && _isBreaked) {
                    deferred.resolve(_isValid);
                } else if (_isValid && !_isBreaked && index === recordItems.length - 1) {
                    deferred.resolve(_isValid);
                }
            });

            return deferred.promise;
        }

    };

    orms.app
        .service('recordService', recordService);

})(orms);