﻿(function (orms) {

    'use strict';

    const itemService = function ($http, API_PREFIX) {

        const url = API_PREFIX + '/item';

        return {
            getPage: getPage,
            search: search,
            add: add,
            update: update,
            getTotalCount: getTotalCount
        };

        function getPage(data) {
            return $http.post(url + '/page', data);
        }

        function search(data) {
            return $http.post(url + '/search', data);
        }

        function add(item) {
            return $http.post(url + '/add', item);
        }

        function update(item) {
            return $http.post(url + '/update', item);
        }

        function getTotalCount() {
            return $http.get(url + '/count');
        }

    };

    orms.app
        .service('itemService', itemService);

})(orms);