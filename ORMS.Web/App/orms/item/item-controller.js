﻿(function (orms) {

    'use strict';

    const itemController = function ($rootScope, $location, $log, $document, categories,
        $alert, confirmModalService, itemService, categoryService, categoryModalService, HTTP_STATUS_CODE, STATUS) {

        let vm = this;
        orms.ic = this;

        const ORDER_BY = {
            NAME: 'Name',
            PRICE: 'Price',
            CATEGORY: 'Category'
        };

        let defaultSearchParams = {
            categoryId: undefined,
            name: undefined
        };

        let defaultPagingOptions = {
            orderBy: undefined,
            pageNumber: 1,
            pageSize: 20,
            totalItems: 0
        };

        categoryModalService.init(categories);

        vm.items = [];
        vm.categories = categories;
        vm.ORDER_BY = ORDER_BY;
        vm.paging = angular.extend({}, defaultPagingOptions);
        vm.searchParams = angular.extend({}, defaultSearchParams);

        vm.goToPage = goToPage;
        vm.search = search;

        vm.addItem = addItem;
        vm.editItem = editItem;
        vm.removeItem = removeItem;

        vm.sortBy = sortBy;
        vm.resetSearch = resetSearch;
        vm.setSelected = setSelected;
        vm.searchDisabled = searchDisabled;

        init();
        function init() {
            vm.goToPage(defaultPagingOptions.pageNumber);
            vm.searchParamsCache = angular.extend({}, defaultSearchParams);
        }

        function goToPage(page) {
            let data = {
                pageNumber: page,
                pageSize: vm.paging.pageSize,
                orderBy: vm.paging.orderBy
            };

            if (angular.isDefined(vm.searchParamsCache)) {
                data = angular.extend({}, data, vm.searchParamsCache);
            }

            $alert
                .async(itemService.getPage(data))
                .then(function (data) {
                    if (data.success) {
                        vm.items = data.items;
                        vm.paging.totalItems = data.totalCount;
                    }
                });
        }

        function search(searchParams) {
            let data = angular.extend({}, defaultPagingOptions, searchParams);
            vm.searchParamsCache = angular.copy(searchParams);

            $alert
                .async(itemService.search(data))
                .then(function (data) {
                    if (data.success) {
                        vm.items = data.items;
                        vm.paging.totalItems = data.totalCount;
                    }
                });
        }

        function addItem(item) {
            let _item = angular.extend({ categoryId: item.category.id }, item);
            _item.name = _item.name.toUpperCase();
            $alert
                .async(itemService.add(_item), { showFeedback: true })
                .then(function (data) {
                    if (data.success) {
                        vm.itemAddModal.hide();
                        vm.goToPage(vm.paging.pageNumber);
                        vm.item = {};
                    }
                });
        }

        function editItem(item) {
            let _item = angular.extend({ categoryId: item.category.id }, item);
            $alert
                .async(itemService.update(_item), { showFeedback: true })
                .then(function (data) {
                    if (data.success) {
                        vm.itemEditModal.hide();
                        vm.goToPage(vm.paging.pageNumber);
                    }
                });
        }

        function removeItem(item) {
            vm.itemEditModal.invisible();
            confirmModalService
                .show({ calledFromModal: true })
                .then(function (result) {
                    if (result) {
                        let _item = angular.extend({ categoryId: item.category.id }, item);
                        _item.active = STATUS.DISABLED;

                        $alert
                            .async(itemService.update(_item), { showFeedback: true })
                            .then(function (data) {
                                if (data.success) {
                                    vm.itemEditModal.hide();
                                    vm.goToPage(vm.paging.pageNumber);
                                }
                            });
                    } else {
                        vm.itemEditModal.visible();
                    }
                });
        }

        function setSelected(item) {
            vm.selectedItem = angular.copy(item);
        }

        function sortBy(orderBy) {
            vm.paging.orderBy = orderBy;
            goToPage(vm.paging.pageNumber);
        }

        function resetSearch() {
            vm.searchParams = angular.copy({}, defaultSearchParams);
            vm.searchParamsCache = angular.copy({}, defaultSearchParams);
            vm.paging.orderBy = undefined;
            vm.goToPage(defaultPagingOptions.pageNumber);
        }

        function searchDisabled() {
            let category = vm.searchParams.categoryId;
            let name = vm.searchParams.name;

            let categoryUndefined = angular.isUndefined(category) || category === null;
            let nameUndefined = angular.isUndefined(name) || name === "";

            return categoryUndefined && nameUndefined;
        }

        $rootScope.$on("$includeContentLoaded", function (event, templateName) {
            vm.itemEditModal = $document.find('div#itemEditModal');
            vm.itemAddModal = $document.find('div#itemAddModal');

            vm.itemEditModal.hide = function () {
                vm.itemEditModal.modal('hide');
            };

            vm.itemEditModal.invisible = function () {
                vm.itemEditModal.addClass('modal-hide');
            };

            vm.itemEditModal.visible = function () {
                vm.itemEditModal.removeClass('modal-hide');
            };

            vm.itemAddModal.hide = function () {
                vm.itemAddModal.modal('hide');
            };
        });


    };

    orms.app
        .controller('itemController', itemController);

})(orms);