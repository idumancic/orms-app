﻿(function (orms) {

    'use strict';

    orms.app
        .config(function ($routeProvider, FOLDER_PATH, NAVIGATION) {

            $routeProvider
                .when('/items', {
                    templateUrl: FOLDER_PATH.ORMS + 'item/templates/items.html',
                    controller: 'itemController',
                    controllerAs: 'ic',
                    title: NAVIGATION.ITEMS.title,
                    activeTab: NAVIGATION.ITEMS,
                    resolve: {
                        categories: ['categoryService', function (categoryService) {
                            return categoryService.getAll().then(function (response) {
                                let data = response.data;
                                if (data.success) {
                                    return data.categories;
                                }
                            });
                        }]
                    }
                });

        });

})(orms);