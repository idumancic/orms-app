﻿(function (orms) {

    'use strict';

    const roleService = function ($http, API_PREFIX) {

        const url = API_PREFIX + '/role';

        return {
            getAll: getAll
        };

        function getAll() {
            return $http.get(url);
        }

    };

    orms.app
        .service('roleService', roleService);

})(orms);