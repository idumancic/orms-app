﻿(function (orms) {

    'use strict';

    const homeController = function (authService) {

        let vm = this;
        orms.hc = this;

        vm.firstName = authService.getFirstName();
        vm.isWorker = authService.isWorker();
        vm.isAdminOrOwner = authService.isAdmin() || authService.isOwner();

    };

    orms.app
        .controller('homeController', homeController);

})(orms);