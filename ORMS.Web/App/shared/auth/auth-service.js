﻿(function (orms) {

    'use strict';

    const authService = function ($http, $rootScope, $cookies, $base64, COOKIE_KEY, ORMS_ROLES, API_PREFIX) {

        const AUTHORIZATION_HEADER = 'Authorization';
        const url = API_PREFIX + '/authentication';

        return {
            AUTHORIZATION_HEADER: AUTHORIZATION_HEADER,
            ORMS_ROLES: ORMS_ROLES,
            login: login,
            logout: logout,
            setCredentials: setCredentials,
            clearCredentials: clearCredentials,
            getUsername: getUsername,
            getFullName: getFullName,
            getFirstName: getFirstName,
            getLastName: getLastName,
            getRole: getRole,
            isAdmin: isAdmin,
            isOwner: isOwner,
            isWorker: isWorker,
            isManager: isManager
        };

        function login(user) {
            return $http.post(url + '/login', user);
        }

        function logout() {
            return $http.post(url + '/logout');
        }

        function setCredentials(credentials, userData) {
            var authorizationToken = $base64.encode(credentials.username + ':' + credentials.password);

            $rootScope.globals = {
                currentUser: {
                    username: credentials.username,
                    authToken: authorizationToken,
                    fullName: userData.firstName + " " + userData.lastName,
                    firstName: userData.firstName,
                    lastName: userData.lastName,
                    role: userData.role
                }
            };

            $cookies.putObject(COOKIE_KEY.GLOBALS, $rootScope.globals);
        }

        function clearCredentials() {
            $rootScope.globals = {};
            $cookies.remove(COOKIE_KEY.GLOBALS);
        }

        function getUsername() {
            if ($rootScope.globals.currentUser) {
                return $rootScope.globals.currentUser.username || "";
            }
        }

        function getFullName() {
            if ($rootScope.globals.currentUser) {
                return $rootScope.globals.currentUser.fullName || "";
            }
        }

        function getFirstName() {
            if ($rootScope.globals.currentUser) {
                return $rootScope.globals.currentUser.firstName || "";
            }
        }

        function getLastName() {
            if ($rootScope.globals.currentUser) {
                return $rootScope.globals.currentUser.lastName || "";
            }
        }

        function getRole() {
            if ($rootScope.globals.currentUser) {
                return $rootScope.globals.currentUser.role || "";
            }
        }

        function isAdmin() {
            let role = getRole();
            return role === ORMS_ROLES.ADMIN;
        }

        function isOwner() {
            let role = getRole();
            return role === ORMS_ROLES.OWNER;
        }

        function isWorker() {
            let role = getRole();
            return role === ORMS_ROLES.WORKER;
        }

        function isManager() {
            let role = getRole();
            return role === ORMS_ROLES.MANAGER;
        }

    };

    orms.app
        .service('authService', authService);

})(orms);