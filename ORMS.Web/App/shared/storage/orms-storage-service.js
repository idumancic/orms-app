﻿(function (orms) {

    'use strict';

    const ormsStorageService = function () {

        const LOCAL_STORAGE_KEY = {
            RECORD_ITEMS: "record_items"
        };

        return {
            LOCAL_STORAGE_KEY: LOCAL_STORAGE_KEY,
            getRecordItems: getRecordItems,
            setRecordItems: setRecordItems,
            clearYesterdaysState: clearYesterdaysState,
            checkIfRecordItemPageExists: checkIfRecordItemPageExists,
            removeRecordItems: removeRecordItems
        };

        function getRecordItems(pageNumber) {
            let items = localStorage.getItem(LOCAL_STORAGE_KEY.RECORD_ITEMS);

            if (items !== null && angular.isDefined(items)) {
                items = JSON.parse(items);

                if (angular.isDefined(pageNumber)) {
                    return items[pageNumber];
                }
            }

            return items;
        }

        function setRecordItems(pageNumber, recordItems) {
            let storage = getRecordItems();
            let items = storage || {};
            items[pageNumber] = recordItems;

            localStorage.setItem(LOCAL_STORAGE_KEY.RECORD_ITEMS, JSON.stringify(items));
        }

        function clearYesterdaysState() {
            let items = getRecordItems();

            for (var pageNumber in items) {
                let recordItems = items[pageNumber];

                angular.forEach(recordItems, function (recordItem, index) {
                    recordItem.yesterdaysStateOfTheBar = undefined;
                });

                setRecordItems(pageNumber, recordItems);
            }
        }

        function checkIfRecordItemPageExists(pageNumber) {
            let items = getRecordItems(pageNumber) || undefined;
            return angular.isDefined(items);
        }

        function removeRecordItems() {
            localStorage.removeItem(LOCAL_STORAGE_KEY.RECORD_ITEMS);
        }

    };

    orms.app
        .service('ormsStorageService', ormsStorageService);

})(orms);