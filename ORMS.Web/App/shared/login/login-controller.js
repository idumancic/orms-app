﻿(function (orms) {

    'use strict';

    const loginController = function ($scope, $location, authService, $alert, HTTP_STATUS_CODE) {

        let vm = this;
        orms.lc = this;

        vm.user = {};
        vm.login = login;

        function login(user) {
            $alert
                .async(authService.login(user), { showFeedback: true })
                .then(function (data) {
                    if (data.success) {
                        authService.setCredentials(vm.user, data.user);
                        $location.path('/home').replace();
                    }
                });
        }
    };

    orms.app
        .controller('loginController', loginController);

})(orms);