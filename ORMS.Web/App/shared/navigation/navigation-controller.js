﻿(function (orms) {

    'use strict';

    const navigationController = function ($route, $rootScope,  $location, authService, $alert, HTTP_STATUS_CODE, NAVIGATION) {

        let vm = this;
        orms.nc = this;

        vm.$route = $route;
        vm.NAVIGATION = NAVIGATION;
        vm.logout = logout;

        vm.firstName = authService.getFirstName();
        vm.isAdminOrOwner = authService.isAdmin() || authService.isOwner();
        vm.isWorker = authService.isWorker();

        function logout() {
            $alert
                .async(authService.logout(), { showFeedback: true })
                .then(function(response) {
                    authService.clearCredentials();
                    $location.path('/login').replace();
                });
        }


        // Hotfix for collapsing navbar after pressing link
        $(".navbar-nav .nav-item .nav-link").click(function (event) {
            $(".navbar-collapse").collapse('hide');
        });

    };

    orms.app
        .controller('navigationController', navigationController);

})(orms);