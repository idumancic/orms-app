﻿(function (orms) {

    'use strict';

    const navigationService = function ($location) {

        return {
            $go: $go
        };

        function $go(url, id) {
            let urlWithParams = url.replace(':id', id);
            $location.path(urlWithParams);
        }

    };

    orms.app
        .service('navigationService', navigationService);

})(orms);