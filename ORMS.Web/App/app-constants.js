﻿(function (orms) {

    'use strict';

    orms.app
        .constant('API_PREFIX', '/api')
        .constant('FOLDER_PATH', {
            COMPONENTS: 'App/components/',
            ORMS: 'App/orms/',
            SHARED: 'App/shared/'
        })
        .constant('ORMS_ROLES', {
            ADMIN: "ORMS_Admin",
            OWNER: "ORMS_Owner",
            MANAGER: "ORMS_Manager",
            WORKER: "ORMS_Worker"
        })
        .constant('HTTP_STATUS_CODE', {
            SUCCESS: 200,
            NOT_FOUND: 404,
            NO_CONTENT: 204,
            UNAUTHORIZED: 401,
            BAD_REQUEST: 400
        })
        .constant('STATUS', {
            ACTIVE: true,
            DISABLED: false
        })
        .constant('VIEW_TYPE', {
            CREATE: 'CREATE',
            EDIT: 'EDIT',
            SHOW: 'SHOW'
        })
        .constant('ORMS_COLOR',{
            GRAY: '#212121',
            WHITE: '#FFFFFFF',
            TURQUOISE_BLUE: '#17a2b8'
        })
        .constant('NAVIGATION', {
            STATISTICS: {
                id: 0,
                title: 'Statistics'
            },
            RECORDS: {
                id: 1,
                title: 'Records'
            },
            ITEMS: {
                id: 2,
                title: 'Items'
            },
            EMPLOYEES: {
                id: 3,
                title: 'Employees'
            },
            OFFICES: {
                id: 4,
                title: 'Offices'
            },
            LOGIN: {
                id: 5,
                title: 'Login'
            },
            ERROR: {
                id: 6,
                title: 'Error'
            },
            HOME: {
                id: 7,
                title: 'Home'
            }
        })
        .constant('COOKIE_KEY', {
            GLOBALS: 'globals'
        });


})(orms);