﻿const orms = {};

(function (orms) {

    'use strict';

    //let _ormsItem = angular.module('ormsItem', []);

    let _app = angular.module('ormsApp', [
        'ngRoute',
        'ngCookies',
        'base64',
        'bw.paging',
        'chart.js'
    ]);

    _app.config(function ($routeProvider, $locationProvider, $httpProvider, $cookiesProvider, FOLDER_PATH, NAVIGATION) {

        const DEFAULT_MINUTES = 45;
        let cookieExpirationTime = new Date();
        cookieExpirationTime.setMinutes(cookieExpirationTime.getMinutes() + DEFAULT_MINUTES);

        $cookiesProvider.defaults.expires = cookieExpirationTime;
        $httpProvider.interceptors.push('tokenInterceptor');

        /*
           Overrided toISOString() because when JSON.stringify(date) is called on sending data through http post method to server in
           background it uses toISOString() to convert passed date object to ISO string and removes one day because of the timezone
           NOTE: this is a quick fix, not really the best practice and recommended to do
        */
        Date.prototype.toISOString = function () {
            return this.toLocaleDateString();
        };

        $routeProvider
            .when('/', {
                redirectTo: '/login'
            })
            .when('/login', {
                templateUrl: FOLDER_PATH.SHARED + 'login/templates/login.html',
                controller: 'loginController',
                controllerAs: 'lc',
                title: NAVIGATION.LOGIN.title
            })
            .when('/home', {
                templateUrl: FOLDER_PATH.SHARED + 'home/templates/home.html',
                controller: 'homeController',
                controllerAs: 'hc',
                title: NAVIGATION.HOME.title
            })
            .when('/error', {
                title: NAVIGATION.ERROR.title,
                templateUrl: FOLDER_PATH.SHARED + 'error/templates/page-not-found.html'
            })
            .when('/not-authorized', {
                title: NAVIGATION.ERROR.title,
                templateUrl: FOLDER_PATH.SHARED + 'error/templates/not-authorized.html'
            })
            .otherwise({
                redirectTo: '/login'
            });

        $locationProvider.hashPrefix('');
    });

    _app.run(function ($rootScope, $log, $location, $cookies, ormsStorageService, COOKIE_KEY) {
        $rootScope.$on('$locationChangeStart', function (event, next, current) {
            let globals = $cookies.getObject(COOKIE_KEY.GLOBALS) || {};
            const loginUrlPath = '/login';

            if ($location.path() !== loginUrlPath && angular.isUndefined(globals.currentUser)) {
                $location.path('/login');
            } else if ($location.path() === loginUrlPath && angular.isDefined(globals.currentUser)) {
                event.preventDefault();
            } else if ($location.path() === '/') {
                $location.path('/home');
            }
        });


        $rootScope.$on('$routeChangeSuccess', function (event, current, previous) {
            const recordEditPath = '/records/:id/edit';
            const recordAddPath = '/records/add';

            if (angular.isDefined(previous)) {
                $log.info("FROM: " + previous.originalPath);
                if (previous.originalPath === recordEditPath || previous.originalPath === recordAddPath) {
                    clearRecordsCache();
                }
            } else {
                $log.info("TO: " + current.originalPath);
                if (current.originalPath === recordAddPath || current.originalPath === recordAddPath) {
                    clearRecordsCache();
                }
            }

            ///////////////////
            function clearRecordsCache() {
                ormsStorageService.removeRecordItems();
                $log.info("Clearing records cache...");
            }
        });
    });

    orms.app = _app;

})(orms);