﻿(function (orms) {

    'use strict';

    const confirmModalController = function ($scope, $log, close) {

        orms.confirmModal = this;

        $scope.continueWith = continueWith;
        $scope.cancel = cancel;

        function cancel() {
            $log.info("Closing modal...");
            close(false);
        }

        function continueWith() {
            $log.info("Continuing...");
            close(true);
        }
        
    };

    orms.app
        .controller('confirmModalController', confirmModalController);

})(orms);