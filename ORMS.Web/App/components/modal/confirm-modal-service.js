﻿(function (orms) {

    'use strict';

    const confirmModalService = function ($q, $http, $rootScope, $timeout, $controller, $compile, $document, FOLDER_PATH) {

        const body = $document.find('body');
        const controller = 'confirmModalController';
        const templateUrl = FOLDER_PATH.COMPONENTS + 'modal/templates/confirm-modal.html';

        let templateCache = undefined;

        let defaultModalOptions = {
            backdrop: 'static',
            keyboard: false,
            show: true
        };

        return {
            show: show
        };

        function show(options) {
            let deferred = $q.defer();
            getTemplate()
                .then(function (template) {
                    let modalOptions = angular.extend({}, options, defaultModalOptions);
                    let modalScope = $rootScope.$new();

                    let closeDeferred = $q.defer();
                    let inputs = {
                        $scope: modalScope,
                        close: function (result) {
                            deferred.resolve(result);
                            closeDeferred.resolve(result);
                        }
                    }

                    $controller(controller, inputs);
                    let modalTemplate = angular.element(template);
                    let link = $compile(modalTemplate);
                    let modalElement = link(modalScope);

                    body.append(modalElement);

                    let modal = {
                        element: modalElement,
                        close: closeDeferred.promise
                    };

                    modal.close.then(function (result) {
                        modalElement.removeClass('fade');
                        modalElement.modal('hide');
                    });

                    modal.element.on('shown.bs.modal', function (e) {
                        body.addClass('modal-open');
                    });

                    modal.element.on('hidden.bs.modal', function (e) {
                        body.removeClass('modal-open');
                        modalScope.$destroy();
                        modalElement.remove();
                    });

                    modal.element.modal(modalOptions);
                });

            return deferred.promise;
        }

        function getTemplate() {
            let deferred = $q.defer();

            if (angular.isDefined(templateCache)) {
                deferred.resolve(templateCache);
            } else {
                $http.get(templateUrl, { cache: true })
                    .then(function (response) {
                        templateCache = response.data;
                        deferred.resolve(response.data);
                    })
                    .catch(function (error) {
                        deferred.reject(error);
                    });
            }

            return deferred.promise;
        }
    };

    orms.app
        .service('confirmModalService', confirmModalService);

})(orms);