﻿(function (orms) {

    'use strict';

    const $alert = function ($q, $location, HTTP_STATUS_CODE) {

        const ALERT_TYPE = {
            SUCCESS: "success",
            ERROR: "error",
            WARNING: "warning",
            INFO: "info",
            QUESTION: "question"
        };

        const DEFAULT_TIMER = 2500;
        const DEFAULT_ERROR_MESSAGE = "Došlo je do greške u radu aplikacije.";

        let defaultAlert = {
            title: "",
            text: "",
            width: "350px",
            timer: DEFAULT_TIMER,
            showConfirmButton: false
        };

        return {
            show: show,
            showValidation: showValidation,
            async: async,
            ALERT_TYPE: ALERT_TYPE
        };

        function async(promise, options) {
            let deferred = $q.defer();
            let _async = angular.extend({}, defaultAlert, {
                title: "Pričekajte...",
                timer: null,
                onOpen: () => {
                    swal.clickConfirm();
                },
                showLoaderOnConfirm: true,
                preConfirm: () => {
                    return promise.then(function (response) {
                        return response.data || response || {};
                    });
                },
                allowOutsideClick: () => !swal.isLoading()
            });

            swal(_async)
                .then(function (response) {
                    if (angular.isDefined(options) && options.showFeedback === true) {
                        let value = response.value;
                        show({ text: value.message }, value.success).then(function () {
                            deferred.resolve(value);
                        });
                    } else {
                        deferred.resolve(response.value);
                    }
                })
                .catch(function (error) {
                    if (angular.isDefined(error.status) && error.status !== HTTP_STATUS_CODE.UNAUTHORIZED) {
                        if (angular.isDefined(error.data) && error.data !== null) {
                            let data = error.data;
                            showValidation(data.message, error.status);
                        } else {
                            show({ text: DEFAULT_ERROR_MESSAGE }, error.status);
                        }
                    }
                });

            return deferred.promise;
        }

        function showValidation(message, status) {
            return show({ text: message, timer: null, showConfirmButton: true }, status || HTTP_STATUS_CODE.BAD_REQUEST);
        }

        function show(alert, status) {
            let _alert = angular.extend({
                type: getAlertType(status)
            }, defaultAlert, alert);

            return swal(_alert);
        }

        function getAlertType(status) {
            if (typeof status === "boolean") {
                return getAlertTypeBool(status);
            } else {
                return getAlertTypeHttpCode(status);
            }

            //////////
            function getAlertTypeBool(status) {
                return status ? ALERT_TYPE.SUCCESS : ALERT_TYPE.ERROR;
            }

            function getAlertTypeHttpCode(status) {
                let type = undefined;
                switch (status) {
                    case HTTP_STATUS_CODE.SUCCESS:
                        type = ALERT_TYPE.SUCCESS;
                        break;
                    case HTTP_STATUS_CODE.UNAUTHORIZED:
                    case HTTP_STATUS_CODE.NO_CONTENT:
                        type = ALERT_TYPE.ERROR;
                        break;
                    case HTTP_STATUS_CODE.BAD_REQUEST:
                        type = ALERT_TYPE.WARNING;
                        break;
                    default:
                        type = ALERT_TYPE.ERROR;
                        break;
                }

                return type;
            }
        }
    };

    orms.app
        .service('$alert', $alert);

})(orms);