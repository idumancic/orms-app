﻿(function (orms) {

    'use strict';

    const tokenInterceptor = ['$q', '$log', '$rootScope', '$cookies', '$location', 'COOKIE_KEY', 'HTTP_STATUS_CODE',
        function ($q, $log, $rootScope, $cookies, $location, COOKIE_KEY, HTTP_STATUS_CODE) {
            return {
                'request': request,
                'responseError': responseError
            };

            function request(config) {
                $log.info("TOKEN INTERCEPTOR: Checking token...");
                $rootScope.globals = $cookies.getObject(COOKIE_KEY.GLOBALS) || {};
                const currentUser = $rootScope.globals.currentUser;

                if (angular.isDefined(currentUser)) {
                    $log.info("TOKEN INTERCEPTOR: Token found");
                    config.headers['Authorization'] = 'Basic ' + currentUser.authToken;
                }
                else {
                    $log.warn("TOKEN INTERCEPTOR: Token not found");
                    config.headers['Authorization'] = 'Basic ';
                }

                return config || $q.when(config);
            }

            function responseError(response) {
                $rootScope.globals = $cookies.getObject(COOKIE_KEY.GLOBALS) || {};
                const currentUser = $rootScope.globals.currentUser;

                if ($location.path() !== '/login' && response.status === HTTP_STATUS_CODE.UNAUTHORIZED) {
                    if (angular.isDefined(currentUser)) {
                        $log.warn("TOKEN INTERCEPTOR: Not authorized, redirecting...");
                        $location.path('/not-authorized');
                        return;
                    }

                    $log.warn("TOKEN INTERCEPTOR: Token not found, redirecting...");
                    $location.path('/login');
                }

                return $q.reject(response);
            }
        }];

    orms.app
        .service('tokenInterceptor', tokenInterceptor);

})(orms);