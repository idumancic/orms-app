﻿(function (orms) {

    'use strict';

    const appController = function ($log, $rootScope, $route, $location, $window) {

        let vm = this;
        window.ac = this;

        const rootUrlPath = '/';
        const loginUrlPath = '/login';
        const errorUrlPath = '/error';

        vm.notLoginView = notLoginView;
        vm.errorView = errorView;
        vm.notErrorView = notErrorView;
        vm.goToTheTop = goToTheTop;

        init();

        function init() {
            setTitle();
        }

        function notLoginView() {
            return $location.path() !== loginUrlPath;
        }

        function errorView() {
            return $location.path() === errorUrlPath;
        }

        function notErrorView() {
            return !errorView();
        }

        function setTitle() {
            if (angular.isDefined($route.current))
                $window.document.title = "ORMS - " + $route.current.title;
            else
                $window.document.title = "ORMS";
        }

        // When the user clicks on the button, scroll to the top of the document
        function goToTheTop() {
            $('body,html').animate({ scrollTop: 0 }, 700);
        }

        $rootScope.$on('$routeChangeSuccess', function () {
            $log.info("CURRENT ROUTE: " + $location.path());
            setTitle();
        });

    };

    orms.app
        .controller('appController', appController);

})(orms);