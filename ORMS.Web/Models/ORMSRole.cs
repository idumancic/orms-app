﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ORMS.Web.Models
{
    public static class ORMSRole
    {
        public const string ADMIN = "ORMS_Admin";
        public const string OWNER = "ORMS_Owner";
        public const string MANAGER = "ORMS_Manager";
        public const string WORKER = "ORMS_Worker";
    }
}