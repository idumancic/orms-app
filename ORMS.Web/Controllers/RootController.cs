﻿using ORMS.Web.Models;
using ORMS.Web.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ORMS.Web.Controllers
{
    [Roles(ORMSRole.ADMIN, ORMSRole.OWNER, ORMSRole.WORKER, ORMSRole.MANAGER)]
    public class RootController : Controller
    {
        public ActionResult Root()
        {
            return View();
        }
    }
}