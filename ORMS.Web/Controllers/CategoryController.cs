﻿using ORMS.Service;
using ORMS.Service.Category.Interface;
using ORMS.Service.Category.Request;
using ORMS.Web.Models;
using ORMS.Web.Modules;
using ORMS.Web.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ORMS.Web.Controllers
{
    [Roles(ORMSRole.ADMIN, ORMSRole.OWNER)]
    public class CategoryController : ApiController
    {
        private ICategoryService categoryService = ServiceFactory.CreateCategoryService();

        public IHttpActionResult Get()
        {
            var response = categoryService.GetAll();

            if (response.Success)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, response));
            }

            response.Message = ORMSMessage.GENERIC_GET_FAILED_MESSAGE;
            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, response));
        }

        [HttpPost]
        public IHttpActionResult Add(CategoryUpdateRequest request)
        {
            var response = categoryService.Add(request);

            if (response.Success)
            {
                response.Message = ORMSMessage.SAVE_SUCCESS;
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, response));
            }

            response.Message = ORMSMessage.GENERIC_FAILED_MESSAGE;
            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, response));
        }

        [HttpPost]
        public IHttpActionResult Update(CategoryUpdateRequest request)
        {
            var response = categoryService.Update(request);

            if (response.Success)
            {
                response.Message = ORMSMessage.UPDATE_SUCCESS;
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, response));
            }

            response.Message = ORMSMessage.GENERIC_FAILED_MESSAGE;
            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, response));
        }

    }
}
