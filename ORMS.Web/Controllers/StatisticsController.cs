﻿using ORMS.Service;
using ORMS.Service.Statistic;
using ORMS.Service.Statistic.Interface;
using ORMS.Web.Models;
using ORMS.Web.Modules;
using ORMS.Web.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ORMS.Web.Controllers
{
    [Roles(ORMSRole.ADMIN, ORMSRole.OWNER)]
    public class StatisticsController : ApiController
    {
        private IStatisticsService statisticService = ServiceFactory.CreateStatisticsService();

        [HttpGet]
        public IHttpActionResult YearlyEarnings()
        {
            var response = statisticService.GetYearlyEarnings();

            if (response.Success)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, response));
            }

            response.Message = ORMSMessage.GENERIC_GET_FAILED_MESSAGE;
            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, response));
        }

        [HttpGet]
        public IHttpActionResult MonthlyEarnings()
        {
            var response = statisticService.GetMonthlyEarnings();

            if (response.Success)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, response));
            }

            response.Message = ORMSMessage.GENERIC_GET_FAILED_MESSAGE;
            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, response));
        }

        [HttpGet]
        public IHttpActionResult ShiftEarnings()
        {
            var response = statisticService.GetShiftEarnings();

            if (response.Success)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, response));
            }

            response.Message = ORMSMessage.GENERIC_GET_FAILED_MESSAGE;
            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, response));
        }

        [HttpGet]
        public IHttpActionResult Top10Items()
        {
            var response = statisticService.GetTop10Items();

            if (response.Success)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, response));
            }

            response.Message = ORMSMessage.GENERIC_GET_FAILED_MESSAGE;
            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, response));
        }
    }
}
