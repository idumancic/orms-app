﻿using ORMS.Service;
using ORMS.Service.Role;
using ORMS.Service.Role.Interface;
using ORMS.Web.Models;
using ORMS.Web.Modules;
using ORMS.Web.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ORMS.Web.Controllers
{
    [Roles(ORMSRole.ADMIN, ORMSRole.OWNER)]
    public class RoleController : ApiController
    {
        private IRoleService roleService = ServiceFactory.CreateRoleService();

        public IHttpActionResult Get()
        {
            var roles = roleService.GetRoles();

            if (roles.Success)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, roles));
            }

            roles.Message = ORMSMessage.GENERIC_GET_FAILED_MESSAGE;
            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, roles));
        }
    }
}
