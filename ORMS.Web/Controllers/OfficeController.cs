﻿using ORMS.Service;
using ORMS.Service.Office.Interface;
using ORMS.Service.Office.Request;
using ORMS.Web.Models;
using ORMS.Web.Modules;
using ORMS.Web.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ORMS.Web.Controllers
{
    public class OfficeController : ApiController
    {
        private IOfficeService officeService = ServiceFactory.CreateOfficeService();

        [HttpGet]
        [Roles(ORMSRole.ADMIN, ORMSRole.OWNER, ORMSRole.WORKER, ORMSRole.MANAGER)]
        public IHttpActionResult GetAll()
        {
            var offices = officeService.GetAll();

            if (offices.Success)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, offices));
            }

            offices.Message = ORMSMessage.GENERIC_GET_FAILED_MESSAGE;
            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, offices));
        }

        [HttpGet]
        [Roles(ORMSRole.ADMIN, ORMSRole.OWNER)]
        public IHttpActionResult GetAllEdit()
        {
            var offices = officeService.GetAll();

            if (offices.Success)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, offices));
            }

            offices.Message = ORMSMessage.GENERIC_GET_FAILED_MESSAGE;
            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, offices));
        }

        [HttpGet]
        [Roles(ORMSRole.ADMIN, ORMSRole.OWNER)]
        public IHttpActionResult GetOffice([FromUri]OfficeRequest request)
        {
            var office = officeService.Get(request);

            if (office.Success)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, office));
            }

            office.Message = ORMSMessage.GENERIC_GET_FAILED_MESSAGE;
            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, office));
        }

        [HttpPost]
        [Roles(ORMSRole.ADMIN, ORMSRole.OWNER)]
        public IHttpActionResult Add(OfficeUpdateRequest request)
        {
            var validation = officeService.Validate(request);

            if (validation.Success)
            {
                if (validation.OIBExists)
                {
                    validation.Message = ORMSMessage.USER_OIB_EXISTS;
                    return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, validation));
                }
            }

            var office = officeService.Add(request);

            if (office.Success)
            {
                office.Message = ORMSMessage.SAVE_SUCCESS;
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, office));
            }

            office.Message = ORMSMessage.GENERIC_FAILED_MESSAGE;
            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, office));
        }

        [HttpPost]
        [Roles(ORMSRole.ADMIN, ORMSRole.OWNER)]
        public IHttpActionResult Update(OfficeUpdateRequest request)
        {
            var office = officeService.Update(request);

            if (office.Success)
            {
                office.Message = ORMSMessage.UPDATE_SUCCESS;
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, office));
            }

            office.Message = ORMSMessage.GENERIC_FAILED_MESSAGE;
            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, office));
        }
    }
}
