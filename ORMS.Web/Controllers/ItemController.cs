﻿using ORMS.Infrastructure;
using ORMS.Service;
using ORMS.Service.Item.Interface;
using ORMS.Service.Item.Request;
using ORMS.Web.Models;
using ORMS.Web.Modules;
using ORMS.Web.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web.Http;

namespace ORMS.Web.Controllers
{
    public class ItemController : ApiController
    {
        private IItemService itemService = ServiceFactory.CreateItemService();

        [HttpPost]
        [Roles(ORMSRole.ADMIN, ORMSRole.OWNER, ORMSRole.WORKER, ORMSRole.MANAGER)]
        public IHttpActionResult Page([FromBody]ItemPagingRequest request)
        {
            var page = itemService.GetPage(request);

            if (page.Success)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, page));
            }

            page.Message = ORMSMessage.GENERIC_GET_FAILED_MESSAGE;
            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, page));
        }

        [HttpPost]
        [Roles(ORMSRole.ADMIN, ORMSRole.OWNER)]
        public IHttpActionResult Search([FromBody]ItemPagingRequest request)
        {
            var page = itemService.GetPageBySearch(request);

            if (page.Success)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, page));
            }

            page.Message = ORMSMessage.GENERIC_GET_FAILED_MESSAGE;
            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, page));
        }

        [HttpPost]
        [Roles(ORMSRole.ADMIN, ORMSRole.OWNER)]
        public IHttpActionResult Add([FromBody]ItemUpdateRequest request)
        {
            var response = itemService.Add(request);

            if (response.Success)
            {
                response.Message = ORMSMessage.SAVE_SUCCESS;
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, response));
            }

            response.Message = ORMSMessage.GENERIC_FAILED_MESSAGE;
            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, response));
        }

        [HttpPost]
        [Roles(ORMSRole.ADMIN, ORMSRole.OWNER)]
        public IHttpActionResult Update([FromBody]ItemUpdateRequest request)
        {
            var response = itemService.Update(request);

            if (response.Success)
            {
                response.Message = ORMSMessage.UPDATE_SUCCESS;
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, response));
            }

            response.Message = ORMSMessage.GENERIC_FAILED_MESSAGE;
            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, response));
        }

        [HttpGet]
        [Roles(ORMSRole.ADMIN, ORMSRole.OWNER, ORMSRole.WORKER, ORMSRole.MANAGER)]
        public IHttpActionResult Count()
        {
            var response = itemService.GetTotalItemCount();

            if (response.Success)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, response));
            }

            response.Message = ORMSMessage.GENERIC_FAILED_MESSAGE;
            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, response));
        }

    }
}
