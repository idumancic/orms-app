﻿using ORMS.Service;
using ORMS.Service.Authentication.Interface;
using ORMS.Service.Authentication.Request;
using ORMS.Service.Base;
using ORMS.Web.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ORMS.Web.Controllers
{
    public class AuthenticationController : ApiController
    {
        private IAuthenticationService authService = ServiceFactory.CreateAuthenticationService();

        [HttpPost]
        public IHttpActionResult Login([FromBody]AuthenticationRequest authRequest)
        {
            var authResponse = authService.Authenticate(authRequest);

            if (authResponse.Success)
            {
                authResponse.Message = ORMSMessage.LOGIN_SUCCESS;
                var response = Request.CreateResponse(HttpStatusCode.OK, authResponse);
                return ResponseMessage(response);
            }

            authResponse.Message = ORMSMessage.LOGIN_FAILED;
            var error = Request.CreateResponse(HttpStatusCode.OK, authResponse);
            return ResponseMessage(error);
        }

        [HttpPost]
        public IHttpActionResult Logout()
        {
            var response = new GeneralResponse()
            {
                Success = true,
                Message = ORMSMessage.LOGOUT_SUCCESS
            };

            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, response));
        }
    }
}
