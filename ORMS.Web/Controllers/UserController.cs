﻿using ORMS.Service;
using ORMS.Service.User;
using ORMS.Service.User.Interface;
using ORMS.Service.User.Request;
using ORMS.Web.Models;
using ORMS.Web.Modules;
using ORMS.Web.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ORMS.Web.Controllers
{
    public class UserController : ApiController
    {
        private IUserService userService = ServiceFactory.CreateUserService();

        [HttpGet]
        [Roles(ORMSRole.ADMIN, ORMSRole.OWNER, ORMSRole.WORKER, ORMSRole.MANAGER)]
        public IHttpActionResult Workers()
        {
            var workers = userService.GetWorkers();

            if (workers.Success)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, workers));
            }

            workers.Message = ORMSMessage.GENERIC_GET_FAILED_MESSAGE;
            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, workers));
        }


        [HttpGet]
        [Roles(ORMSRole.ADMIN, ORMSRole.OWNER)]
        public IHttpActionResult WorkersEdit()
        {
            var workers = userService.GetWorkers();

            if (workers.Success)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, workers));
            }

            workers.Message = ORMSMessage.GENERIC_GET_FAILED_MESSAGE;
            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, workers));
        }

        [HttpGet]
        [Roles(ORMSRole.ADMIN, ORMSRole.OWNER)]
        public IHttpActionResult Worker([FromUri]UserWorkerRequest request)
        {
            var worker = userService.GetWorker(request);

            if (worker.Success)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, worker));
            }

            worker.Message = ORMSMessage.GENERIC_GET_FAILED_MESSAGE;
            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, worker));
        }

        [HttpPost]
        [Roles(ORMSRole.ADMIN, ORMSRole.OWNER)]
        public IHttpActionResult AddWorker([FromBody]UserWorkerUpdateRequest request)
        {
            var validation = userService.Validate(request);

            if (validation.Success)
            {
                if (validation.UsernameTaken)
                {
                    validation.Message = ORMSMessage.USER_USERNAME_TAKEN;
                    return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, validation));
                }
                else if (validation.OIBExists)
                {
                    validation.Message = ORMSMessage.USER_OIB_EXISTS;
                    return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, validation));
                }
                else if (validation.JMBGExists)
                {
                    validation.Message = ORMSMessage.USER_JMBG_EXISTS;
                    return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, validation));
                }
            }

            var worker = userService.AddWorker(request);

            if (worker.Success)
            {
                worker.Message = ORMSMessage.SAVE_SUCCESS;
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, worker));
            }

            worker.Message = ORMSMessage.GENERIC_FAILED_MESSAGE;
            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, worker));
        }

        [HttpPost]
        [Roles(ORMSRole.ADMIN, ORMSRole.OWNER)]
        public IHttpActionResult UpdateWorker([FromBody]UserWorkerUpdateRequest request)
        {
            var worker = userService.UpdateWorker(request);

            if (worker.Success)
            {
                worker.Message = ORMSMessage.UPDATE_SUCCESS;
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, worker));
            }

            worker.Message = ORMSMessage.GENERIC_FAILED_MESSAGE;
            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, worker));
        }
    }
}
