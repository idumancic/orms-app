﻿using ORMS.Service;
using ORMS.Service.Record.Interface;
using ORMS.Service.Record.Request;
using ORMS.Service.RecordItem.Interface;
using ORMS.Service.RecordItem.Request;
using ORMS.Web.Models;
using ORMS.Web.Modules;
using ORMS.Web.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;

namespace ORMS.Web.Controllers
{
    [Roles(ORMSRole.ADMIN, ORMSRole.OWNER, ORMSRole.WORKER, ORMSRole.MANAGER)]
    public class RecordController : ApiController
    {
        private IRecordService recordService = ServiceFactory.CreateRecordService();
        private IRecordItemService recordItemService = ServiceFactory.CreateRecordItemService();

        [HttpGet]
        public IHttpActionResult Page([FromUri]RecordPagingRequest request)
        {
            var page = recordService.GetPage(request);

            if (page.Success)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, page));
            }

            page.Message = ORMSMessage.GENERIC_GET_FAILED_MESSAGE;
            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, page));
        }

        [HttpPost]
        public IHttpActionResult Search([FromBody]RecordPagingRequest request)
        {
            var page = recordService.GetPageBySearch(request);

            if (page.Success)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, page));
            }

            page.Message = ORMSMessage.GENERIC_GET_FAILED_MESSAGE;
            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, page));
        }

        [HttpGet]
        public IHttpActionResult Items([FromUri]RecordItemPagingRequest request)
        {
            var page = recordItemService.GetPage(request);

            if (page.Success)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, page));
            }

            page.Message = ORMSMessage.GENERIC_GET_FAILED_MESSAGE;
            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, page));
        }

        [HttpGet]
        [Roles(ORMSRole.ADMIN, ORMSRole.OWNER, ORMSRole.MANAGER)]
        public IHttpActionResult ItemsShow([FromUri]RecordRequest request)
        {
            var recordItems = recordItemService.GetAllItemsForRecord(request);

            if (recordItems.Success)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, recordItems));
            }

            recordItems.Message = ORMSMessage.GENERIC_GET_FAILED_MESSAGE;
            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, recordItems));
        }

        [HttpGet]
        public IHttpActionResult Info([FromUri]RecordRequest request)
        {
            var recordInfo = recordService.GetInfo(request);

            if (recordInfo.Success)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, recordInfo));
            }

            recordInfo.Message = ORMSMessage.GENERIC_GET_FAILED_MESSAGE;
            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, recordInfo));
        }

        [HttpGet]
        public IHttpActionResult RecordItemsCount([FromUri]RecordRequest request)
        {
            var response = recordItemService.GetTotalRecordItemsCount(request);

            if (response.Success)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, response));
            }

            response.Message = ORMSMessage.GENERIC_FAILED_MESSAGE;
            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, response));
        }

        [HttpGet]
        public IHttpActionResult YesterdaysState([FromUri]RecordRequest request)
        {
            var response = recordItemService.GetYesterdaysState(request);

            if (response.Success)
            {
                var list = (IEnumerable<object>)response.Data;
                if (list.Count() < 1)
                {
                    response.Message = ORMSMessage.RECORD_YESTERDAYS_STATE_NO_RESULTS;
                    return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, response));
                }

                response.Message = ORMSMessage.RECORD_YESTERDAYS_STATE_RESULTS;
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, response));
            }

            response.Message = ORMSMessage.GENERIC_FAILED_MESSAGE;
            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, response));
        }

        [HttpPost]
        public IHttpActionResult Add([FromBody]RecordUpdateRequest request)
        {
            var validation = recordService.Validate(request);

            if (validation.Success)
            {
                if (validation.DateCreatedAlreadyExists)
                {
                    validation.Message = ORMSMessage.RECORD_DATE_CREATED_EXISTS;
                    return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, validation));
                }
                else if (validation.RecordItemListDoesNotMatch)
                {
                    validation.Message = ORMSMessage.RECORD_ITEM_LIST_DOES_NOT_MATCH;
                    return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, validation));
                }
            }

            var response = recordService.Add(request);

            if (response.Success)
            {
                response.Message = ORMSMessage.SAVE_SUCCESS;
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, response));
            }

            response.Message = ORMSMessage.GENERIC_FAILED_MESSAGE;
            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, response));
        }

        [HttpPost]
        public IHttpActionResult Update([FromBody]RecordUpdateRequest request)
        {
            var response = recordService.Update(request);

            if (response.Success)
            {
                response.Message = ORMSMessage.UPDATE_SUCCESS;
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, response));
            }

            response.Message = ORMSMessage.GENERIC_FAILED_MESSAGE;
            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, response));
        }

        [HttpGet]
        [Roles(ORMSRole.ADMIN, ORMSRole.OWNER)]
        public IHttpActionResult GetCSV([FromUri]RecordItemsCSVRequest request)
        {
            var response = recordItemService.GetCSVData(request);

            HttpResponseMessage result = Request.CreateResponse(HttpStatusCode.OK, response.Stream);
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
            result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment") { FileName = response.FileName };
            return ResponseMessage(result);
        }


    }
}
