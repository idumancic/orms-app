﻿using ORMS.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ORMS.Infrastructure.Interfaces.Specific;
using ORMS.Infrastructure.Repositories.Specific;
using System.Data.Entity;

namespace ORMS.Infrastructure
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ORMSContext _context;

        public ICategoryRepository Categories { get; set; }

        public IItemRepository Items { get; private set; }

        public IOfficeRepository Offices { get; private set; }

        public IRecordItemRepository RecordItems { get; private set; }

        public IRecordRepository Records { get; private set; }

        public IRoleRepository Roles { get; private set; }

        public IShiftRepository Shifts { get; private set; }

        public IUserRepository Users { get; private set; }

        public IWorkerLogRepository WorkerLogs { get; private set; }

        public UnitOfWork(ORMSContext context)
        {
            _context = context;
            InitializeRepositories();
        }

        private void InitializeRepositories()
        {
            Categories = new CategoryRepository(_context);
            Items = new ItemRepository(_context);
            Offices = new OfficeRepository(_context);
            RecordItems = new RecordItemRepository(_context);
            Records = new RecordRepository(_context);
            Roles = new RoleRepository(_context);
            Shifts = new ShiftRepository(_context);
            Users = new UserRepository(_context);
            WorkerLogs = new WorkerLogRepository(_context);
        }

        private bool _disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this._disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }

            this._disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
        }

        public int Commit()
        {
            return _context.SaveChanges();
        }
    }
}
