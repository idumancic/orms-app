﻿using ORMS.Infrastructure.Interfaces.Specific;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMS.Infrastructure.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        ICategoryRepository Categories { get; }
        IItemRepository Items { get; }
        IOfficeRepository Offices { get; }
        IRecordItemRepository RecordItems { get; }
        IRecordRepository Records { get; }
        IRoleRepository Roles { get; }
        IShiftRepository Shifts { get; }
        IUserRepository Users { get; }
        IWorkerLogRepository WorkerLogs { get; }
        int Commit();
    }
}
