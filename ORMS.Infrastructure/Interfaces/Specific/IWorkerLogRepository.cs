﻿using ORMS.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMS.Infrastructure.Interfaces.Specific
{
    public interface IWorkerLogRepository : IRepository<WorkerLog>
    {
        IEnumerable<WorkerLog> GetByRecord(int recordId);
        Dictionary<int, int> GetShiftEarnings();
    }
}
