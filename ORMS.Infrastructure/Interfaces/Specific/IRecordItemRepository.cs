﻿using ORMS.Core;
using ORMS.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMS.Infrastructure.Interfaces.Specific
{
    public interface IRecordItemRepository : IRepository<RecordItem>
    {
        IEnumerable<RecordItem> GetAll(int recordId);
        IEnumerable<IndexedEntity<RecordItem>> GetAllWithRowIndex(int recordId);
        Page<RecordItem> GetPage(PageProperties pageProp, int recordId);
        int GetTotalItemsForRecord(int recordId);
        IEnumerable<object> GetYesterdaysStateOfTheBar(DateTime dateCreated);
        Dictionary<string, int> GetTop10Items();
    }
}
