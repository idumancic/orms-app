﻿using ORMS.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMS.Infrastructure.Interfaces.Specific
{
    public interface IOfficeRepository : IRepository<Office>
    {
        bool CheckOIB(string oib);
    }
}
