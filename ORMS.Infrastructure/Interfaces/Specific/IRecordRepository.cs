﻿using ORMS.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ORMS.Infrastructure.Models;

namespace ORMS.Infrastructure.Interfaces.Specific
{
    public interface IRecordRepository : IRepository<Record>
    {
        Page<Record> GetPage(PageProperties pageProp, DateTime? fromDate, DateTime? toDate, int? workerId);
        Page<Record> GetPageBySearch(PageProperties pageProp, DateTime? fromDate, DateTime? toDate, int? workerId);
        bool CheckDateCreated(DateTime dateCreated);
        Dictionary<int, int> GetYearlyEarnings();
        Dictionary<int, int> GetMonthlyEarnings();
    }
}
