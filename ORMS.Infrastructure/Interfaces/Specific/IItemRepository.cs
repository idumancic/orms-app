﻿using ORMS.Core;
using ORMS.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMS.Infrastructure.Interfaces.Specific
{
    public interface IItemRepository : IRepository<Item>
    {
        Page<Item> GetPage(PageProperties pageProp, string name = null, int? categordyId = null);
        Page<Item> GetPageBySearch(PageProperties pageProp, string name, int? categoryId);
    }
}
