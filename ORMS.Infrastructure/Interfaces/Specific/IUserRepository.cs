﻿using ORMS.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMS.Infrastructure.Interfaces.Specific
{
    public interface IUserRepository : IRepository<User>
    {
        User Get(string username, string password);
        User GetWorker(int workerId);
        bool CheckUsername(string username);
        bool CheckOIB(string oib);
        bool CheckJMBG(string jmbg);
        IEnumerable<User> GetWorkers();
    }
}
