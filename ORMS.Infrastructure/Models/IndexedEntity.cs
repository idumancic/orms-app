﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMS.Infrastructure.Models
{
    public class IndexedEntity<T> where T : class
    {
        public int Index { get; set; }
        public T Entity { get; set; }
    }
}
