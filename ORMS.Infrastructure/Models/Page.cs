﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMS.Infrastructure.Models
{
    public class Page<T> where T : class
    {
        public int TotalCount { get; set; }
        public IEnumerable<IndexedEntity<T>> Data { get; set; }
    }
}
