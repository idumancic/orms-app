﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ORMS.Infrastructure.Helpers
{
    public static class QueryHelper
    {
        public static IOrderedQueryable<T> OrderBy<T>(this IQueryable<T> query, string propertyName, SortType sortType = SortType.ASC)
        {
            var property = GetProperty<T>(propertyName);
            if (property != null)
            {
                var propertType = property.PropertyType;
                if (propertType == typeof(decimal))
                {
                    switch (sortType)
                    {
                        case SortType.ASC:
                            query = query.OrderBy(GetPropertyExpressionDecimal<T>(propertyName));
                            break;
                        case SortType.DESC:
                            query = query.OrderByDescending(GetPropertyExpressionDecimal<T>(propertyName));
                            break;
                    }
                }
                else if (propertType == typeof(int))
                {
                    switch (sortType)
                    {
                        case SortType.ASC:
                            query = query.OrderBy(GetPropertyExpressionInt<T>(propertyName));
                            break;
                        case SortType.DESC:
                            query = query.OrderByDescending(GetPropertyExpressionInt<T>(propertyName));
                            break;
                    }
                }
                else if (propertType == typeof(string))
                {
                    switch (sortType)
                    {
                        case SortType.ASC:
                            query = query.OrderBy(GetPropertyExpressionString<T>(propertyName));
                            break;
                        case SortType.DESC:
                            query = query.OrderByDescending(GetPropertyExpressionString<T>(propertyName));
                            break;
                    }
                }
            }

            return (IOrderedQueryable<T>)query;
        }

        public static PropertyInfo GetProperty<T>(string propertyName)
        {
            var propertyParentType = typeof(T);
            if (propertyParentType.GetProperty(propertyName, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance) == null)
            {
                return null;
            }

            return propertyParentType.GetProperty(propertyName);
        }

        public static Type GetPropertType<T>(string propertyName)
        {
            var property = GetProperty<T>(propertyName);

            if (property != null)
            {
                return property.PropertyType;
            }

            return null;
        }

        private static Expression<Func<T, object>> GetPropertyExpression<T>(string propertyName)
        {
            var parameterExpression = Expression.Parameter(typeof(T));
            var property = Expression.PropertyOrField(parameterExpression, propertyName);

            return (Expression<Func<T, object>>)Expression.Lambda(property, parameterExpression);
        }

        private static Expression<Func<T, string>> GetPropertyExpressionString<T>(string propertyName)
        {
            var parameterExpression = Expression.Parameter(typeof(T));
            var property = Expression.PropertyOrField(parameterExpression, propertyName);

            return (Expression<Func<T, string>>)Expression.Lambda(property, parameterExpression);
        }

        private static Expression<Func<T, int>> GetPropertyExpressionInt<T>(string propertyName)
        {
            var parameterExpression = Expression.Parameter(typeof(T));
            var property = Expression.PropertyOrField(parameterExpression, propertyName);

            return (Expression<Func<T, int>>)Expression.Lambda(property, parameterExpression);
        }

        private static Expression<Func<T, decimal>> GetPropertyExpressionDecimal<T>(string propertyName)
        {
            var parameterExpression = Expression.Parameter(typeof(T));
            var property = Expression.PropertyOrField(parameterExpression, propertyName);

            return (Expression<Func<T, decimal>>)Expression.Lambda(property, parameterExpression);
        }
    }
}
