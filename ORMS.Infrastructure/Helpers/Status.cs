﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMS.Infrastructure.Helpers
{
    public enum Status
    {
        ACTIVE = 1,
        DISABLED = 0
    }
}
