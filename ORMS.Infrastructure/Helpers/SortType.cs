﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMS.Infrastructure.Helpers
{
    public enum SortType
    {
        ASC = 0,
        DESC = 1
    }
}
