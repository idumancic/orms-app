SET XACT_ABORT ON
BEGIN TRANSACTION ORMS_INSERT_DB

INSERT INTO [Roles] ([RoleCode], [Name]) VALUES ('ORMS_Admin', 'Administrator')

INSERT INTO [Roles] ([RoleCode], [Name]) VALUES ('ORMS_Owner', 'Owner')

INSERT INTO [Roles] ([RoleCode], [Name]) VALUES ('ORMS_Manager', 'Manager')

INSERT INTO [Roles] ([RoleCode], [Name]) VALUES ('ORMS_Worker', 'Worker')

SET IDENTITY_INSERT [Offices] ON

INSERT INTO [Offices] ([OfficeId], [Name], [OIB], [Address], [PostalCode], [Place], [City], [UserCreatedId], [UserUpdatedId]) VALUES (1, 'Oldtimer', '12345678901', 'Ul. Ante Starčevića 60', '10360', 'Sesvete', 'Zagreb', 1, 1)

SET IDENTITY_INSERT [Offices] OFF

INSERT INTO [Users] ([Username], [Password], [FirstName], [LastName], [RoleCode], [UserCreatedId], [UserUpdatedId]) VALUES ('admin', 'Oldtimer123', 'Admin', 'Admin', 'ORMS_Admin', 1, 1)

INSERT INTO [Users] ([Username], [Password], [FirstName], [LastName], [RoleCode], [UserCreatedId], [UserUpdatedId]) VALUES ('sdumancic', 'Stipe123', 'Stipe' , 'Dumančić', 'ORMS_Owner', 1, 1)

INSERT INTO [Users] ([Username], [Password], [FirstName], [LastName], [RoleCode], [UserCreatedId], [UserUpdatedId]) VALUES ('ddumancic', 'Daniela123', 'Daniela', 'Dumančić', 'ORMS_Owner', 1, 1)

INSERT INTO [Users] ([Username], [Password], [FirstName], [LastName], [RoleCode], [OfficeId], [UserCreatedId], [UserUpdatedId]) VALUES ('iprezime', 'Igor123', 'Igor', 'Prezime', 'ORMS_Worker', 1, 1, 1)

INSERT INTO [Users] ([Username], [Password], [FirstName], [LastName], [RoleCode], [OfficeId], [UserCreatedId], [UserUpdatedId]) VALUES ('vprezime', 'Vatra123', 'Vatra', 'Prezime', 'ORMS_Worker', 1, 1, 1)

INSERT INTO [Users] ([Username], [Password], [FirstName], [LastName], [RoleCode], [OfficeId], [UserCreatedId], [UserUpdatedId]) VALUES ('test', 'test123', 'Test', 'Test', 'ORMS_Worker', 1, 1, 1)

SET IDENTITY_INSERT [Categories] ON

INSERT INTO [Categories] ([CategoryId], [Name], [UserCreatedId], [UserUpdatedId]) VALUES (1, 'Vrući napitak', 1, 1)

INSERT INTO [Categories] ([CategoryId], [Name], [UserCreatedId], [UserUpdatedId]) VALUES (2, 'Sok', 1, 1)

INSERT INTO [Categories] ([CategoryId], [Name], [UserCreatedId], [UserUpdatedId]) VALUES (3, 'Žestica', 1, 1)

INSERT INTO [Categories] ([CategoryId], [Name], [UserCreatedId], [UserUpdatedId]) VALUES (4, 'Vino', 1, 1)

INSERT INTO [Categories] ([CategoryId], [Name], [UserCreatedId], [UserUpdatedId]) VALUES (5, 'Pivo', 1, 1)

INSERT INTO [Categories] ([CategoryId], [Name], [UserCreatedId], [UserUpdatedId]) VALUES (6, 'Rakija', 1, 1)

INSERT INTO [Categories] ([CategoryId], [Name], [UserCreatedId], [UserUpdatedId]) VALUES (7, 'Liker', 1, 1)

INSERT INTO [Categories] ([CategoryId], [Name], [UserCreatedId], [UserUpdatedId]) VALUES (8, 'Šampanjac', 1, 1)

INSERT INTO [Categories] ([CategoryId], [Name], [UserCreatedId], [UserUpdatedId]) VALUES (9, 'Voda', 1, 1)

INSERT INTO [Categories] ([CategoryId], [Name], [UserCreatedId], [UserUpdatedId]) VALUES (10, 'Ostalo', 1, 1)

SET IDENTITY_INSERT [Categories] OFF

INSERT INTO [Items] ([Name], [Price], [UserCreatedId], [UserUpdatedId], [CategoryId]) VALUES ('KAVA KG', 0.0, 1, 1, 10)

INSERT INTO [Items] ([Name], [Price], [UserCreatedId], [UserUpdatedId], [CategoryId]) VALUES ('KAVA MLINAR', 0.0, 1, 1, 10)

INSERT INTO [Items] ([Name], [Price], [UserCreatedId], [UserUpdatedId], [CategoryId]) VALUES ('NESCAFFE INSTANT', 6, 1, 1, 1)

INSERT INTO [Items] ([Name], [Price], [UserCreatedId], [UserUpdatedId], [CategoryId]) VALUES ('NESCAFFE CLASSIC', 4, 1, 1, 1)

INSERT INTO [Items] ([Name], [Price], [UserCreatedId], [UserUpdatedId], [CategoryId]) VALUES ('VRUĆA ČOKOLADA', 8, 1, 1, 1)

INSERT INTO [Items] ([Name], [Price], [UserCreatedId], [UserUpdatedId], [CategoryId]) VALUES ('KAKAO', 5, 1, 1, 1)

INSERT INTO [Items] ([Name], [Price], [UserCreatedId], [UserUpdatedId], [CategoryId]) VALUES ('KAVA BEZ KOFEINA', 8, 1, 1, 1)

INSERT INTO [Items] ([Name], [Price], [UserCreatedId], [UserUpdatedId], [CategoryId]) VALUES ('MLIJEKO', 30, 1, 1, 10)

INSERT INTO [Items] ([Name], [Price], [UserCreatedId], [UserUpdatedId], [CategoryId]) VALUES ('ČAJ', 10, 1, 1, 1)

INSERT INTO [Items] ([Name], [Price], [UserCreatedId], [UserUpdatedId], [CategoryId]) VALUES ('LIMUNADA', 5, 1, 1, 2)

INSERT INTO [Items] ([Name], [Price], [UserCreatedId], [UserUpdatedId], [CategoryId]) VALUES ('COCTA', 12, 1, 1, 2)

INSERT INTO [Items] ([Name], [Price], [UserCreatedId], [UserUpdatedId], [CategoryId]) VALUES ('SPRITE', 12, 1, 1, 2)

INSERT INTO [Items] ([Name], [Price], [UserCreatedId], [UserUpdatedId], [CategoryId]) VALUES ('COCA COLA', 12, 1, 1, 2)

INSERT INTO [Items] ([Name], [Price], [UserCreatedId], [UserUpdatedId], [CategoryId]) VALUES ('FANTA', 12, 1, 1, 2)

INSERT INTO [Items] ([Name], [Price], [UserCreatedId], [UserUpdatedId], [CategoryId]) VALUES ('BITTER LEMON', 12, 1, 1, 2)

INSERT INTO [Items] ([Name], [Price], [UserCreatedId], [UserUpdatedId], [CategoryId]) VALUES ('TONIC', 12, 1, 1, 2)

INSERT INTO [Items] ([Name], [Price], [UserCreatedId], [UserUpdatedId], [CategoryId]) VALUES ('TANGERINA', 12, 1, 1, 2)

INSERT INTO [Items] ([Name], [Price], [UserCreatedId], [UserUpdatedId], [CategoryId]) VALUES ('ORANGINA', 14, 1, 1, 2)

INSERT INTO [Items] ([Name], [Price], [UserCreatedId], [UserUpdatedId], [CategoryId]) VALUES ('NARANČA JUICE', 14, 1, 1, 2)

INSERT INTO [Items] ([Name], [Price], [UserCreatedId], [UserUpdatedId], [CategoryId]) VALUES ('MARELICA', 14, 1, 1, 2)

INSERT INTO [Items] ([Name], [Price], [UserCreatedId], [UserUpdatedId], [CategoryId]) VALUES ('JAGODA', 14, 1, 1, 2)

INSERT INTO [Items] ([Name], [Price], [UserCreatedId], [UserUpdatedId], [CategoryId]) VALUES ('JABUKA', 14, 1, 1, 2)

INSERT INTO [Items] ([Name], [Price], [UserCreatedId], [UserUpdatedId], [CategoryId]) VALUES ('CRNI RIBIZL', 14, 1, 1, 2)

INSERT INTO [Items] ([Name], [Price], [UserCreatedId], [UserUpdatedId], [CategoryId]) VALUES ('BOROVNICA', 14, 1, 1, 2)

INSERT INTO [Items] ([Name], [Price], [UserCreatedId], [UserUpdatedId], [CategoryId]) VALUES ('BRUSNICA', 14, 1, 1, 2)

INSERT INTO [Items] ([Name], [Price], [UserCreatedId], [UserUpdatedId], [CategoryId]) VALUES ('ANANAS', 14, 1, 1, 2)

INSERT INTO [Items] ([Name], [Price], [UserCreatedId], [UserUpdatedId], [CategoryId]) VALUES ('KRUŠKA', 14, 1, 1, 2)

INSERT INTO [Items] ([Name], [Price], [UserCreatedId], [UserUpdatedId], [CategoryId]) VALUES ('BANANA', 14, 1, 1, 2)

INSERT INTO [Items] ([Name], [Price], [UserCreatedId], [UserUpdatedId], [CategoryId]) VALUES ('VIŠNJA', 14, 1, 1, 2)

INSERT INTO [Items] ([Name], [Price], [UserCreatedId], [UserUpdatedId], [CategoryId]) VALUES ('CEDEVITA', 10, 1, 1, 2)

INSERT INTO [Items] ([Name], [Price], [UserCreatedId], [UserUpdatedId], [CategoryId]) VALUES ('LEDENI ČAJ', 12, 1, 1, 2)

INSERT INTO [Items] ([Name], [Price], [UserCreatedId], [UserUpdatedId], [CategoryId]) VALUES ('RED BULL', 20, 1, 1, 10)

INSERT INTO [Items] ([Name], [Price], [UserCreatedId], [UserUpdatedId], [CategoryId]) VALUES ('MINERALNA 0.25L', 7, 1, 1, 9)

INSERT INTO [Items] ([Name], [Price], [UserCreatedId], [UserUpdatedId], [CategoryId]) VALUES ('MINERALNA 1L', 20, 1, 1, 9)

INSERT INTO [Items] ([Name], [Price], [UserCreatedId], [UserUpdatedId], [CategoryId]) VALUES ('MINERALNA S OKUSOM', 10, 1, 1, 9)

INSERT INTO [Items] ([Name], [Price], [UserCreatedId], [UserUpdatedId], [CategoryId]) VALUES ('JANA 0.33L', 10, 1, 1, 9)

INSERT INTO [Items] ([Name], [Price], [UserCreatedId], [UserUpdatedId], [CategoryId]) VALUES ('SOMERSBY', 18, 1, 1, 5)

INSERT INTO [Items] ([Name], [Price], [UserCreatedId], [UserUpdatedId], [CategoryId]) VALUES ('OŽUJSKA 0.33L', 12, 1, 1, 5)

INSERT INTO [Items] ([Name], [Price], [UserCreatedId], [UserUpdatedId], [CategoryId]) VALUES ('OŽUJSKA 0.5L', 12, 1, 1, 5)

INSERT INTO [Items] ([Name], [Price], [UserCreatedId], [UserUpdatedId], [CategoryId]) VALUES ('HEINEKEN', 16, 1, 1, 5)

INSERT INTO [Items] ([Name], [Price], [UserCreatedId], [UserUpdatedId], [CategoryId]) VALUES ('STAROPRAMEN 0.33L', 12, 1, 1, 5)

INSERT INTO [Items] ([Name], [Price], [UserCreatedId], [UserUpdatedId], [CategoryId]) VALUES ('STAROPRAMEN 0.5L', 12, 1, 1, 5)

INSERT INTO [Items] ([Name], [Price], [UserCreatedId], [UserUpdatedId], [CategoryId]) VALUES ('KARLOVAČKA 0.5L', 12, 1, 1, 5)

INSERT INTO [Items] ([Name], [Price], [UserCreatedId], [UserUpdatedId], [CategoryId]) VALUES ('KARLOVAČKA RADLER', 12, 1, 1, 5)

INSERT INTO [Items] ([Name], [Price], [UserCreatedId], [UserUpdatedId], [CategoryId]) VALUES ('AMBER', 12, 1, 1, 5)

INSERT INTO [Items] ([Name], [Price], [UserCreatedId], [UserUpdatedId], [CategoryId]) VALUES ('PAN 0.5L', 12, 1, 1, 5)

INSERT INTO [Items] ([Name], [Price], [UserCreatedId], [UserUpdatedId], [CategoryId]) VALUES ('BECKS', 12, 1, 1, 5)

INSERT INTO [Items] ([Name], [Price], [UserCreatedId], [UserUpdatedId], [CategoryId]) VALUES ('COOL BEZALKOHOLNI', 12, 1, 1, 5)

INSERT INTO [Items] ([Name], [Price], [UserCreatedId], [UserUpdatedId], [CategoryId]) VALUES ('PAULANER 0.33L', 16, 1, 1, 5)

INSERT INTO [Items] ([Name], [Price], [UserCreatedId], [UserUpdatedId], [CategoryId]) VALUES ('PAULANER 0.5L', 16, 1, 1, 5)

INSERT INTO [Items] ([Name], [Price], [UserCreatedId], [UserUpdatedId], [CategoryId]) VALUES ('BUDWAISER 0.5L', 16, 1, 1, 5)

INSERT INTO [Items] ([Name], [Price], [UserCreatedId], [UserUpdatedId], [CategoryId]) VALUES ('RIZLING', 70, 1, 1, 4)

INSERT INTO [Items] ([Name], [Price], [UserCreatedId], [UserUpdatedId], [CategoryId]) VALUES ('GRAŠEVINA', 70, 1, 1, 4)

INSERT INTO [Items] ([Name], [Price], [UserCreatedId], [UserUpdatedId], [CategoryId]) VALUES ('STOLNO CRNO', 70, 1, 1, 4)

INSERT INTO [Items] ([Name], [Price], [UserCreatedId], [UserUpdatedId], [CategoryId]) VALUES ('BUTELJA 0.2L', 18, 1, 1, 4)

INSERT INTO [Items] ([Name], [Price], [UserCreatedId], [UserUpdatedId], [CategoryId]) VALUES ('BUTELJA 0.75L', 100, 1, 1, 4)

INSERT INTO [Items] ([Name], [Price], [UserCreatedId], [UserUpdatedId], [CategoryId]) VALUES ('VERMONT', 10, 1, 1, 4)

INSERT INTO [Items] ([Name], [Price], [UserCreatedId], [UserUpdatedId], [CategoryId]) VALUES ('ŠAMPANJAC 0.2L', 20, 1, 1, 8)

INSERT INTO [Items] ([Name], [Price], [UserCreatedId], [UserUpdatedId], [CategoryId]) VALUES ('ŠAMPANJAC 0.75L', 80, 1, 1, 8)

INSERT INTO [Items] ([Name], [Price], [UserCreatedId], [UserUpdatedId], [CategoryId]) VALUES ('MARTINI BIJELI', 9, 1, 1, 7)

INSERT INTO [Items] ([Name], [Price], [UserCreatedId], [UserUpdatedId], [CategoryId]) VALUES ('VIŠNJEVAC', 12, 1, 1, 6)

INSERT INTO [Items] ([Name], [Price], [UserCreatedId], [UserUpdatedId], [CategoryId]) VALUES ('KONJAK BADEL', 7, 1, 1, 3)

INSERT INTO [Items] ([Name], [Price], [UserCreatedId], [UserUpdatedId], [CategoryId]) VALUES ('SAX DRY GIN', 7, 1, 1, 3)

INSERT INTO [Items] ([Name], [Price], [UserCreatedId], [UserUpdatedId], [CategoryId]) VALUES ('PELINKOVAC', 7, 1, 1, 7)

INSERT INTO [Items] ([Name], [Price], [UserCreatedId], [UserUpdatedId], [CategoryId]) VALUES ('LAVOV', 7, 1, 1, 7)

INSERT INTO [Items] ([Name], [Price], [UserCreatedId], [UserUpdatedId], [CategoryId]) VALUES ('RUM DOMAĆI', 7, 1, 1, 3)

INSERT INTO [Items] ([Name], [Price], [UserCreatedId], [UserUpdatedId], [CategoryId]) VALUES ('AMARO', 7, 1, 1, 7)

INSERT INTO [Items] ([Name], [Price], [UserCreatedId], [UserUpdatedId], [CategoryId]) VALUES ('ISTRA BITTER', 7, 1, 1, 7)

INSERT INTO [Items] ([Name], [Price], [UserCreatedId], [UserUpdatedId], [CategoryId]) VALUES ('DALMATINO', 7, 1, 1, 7)

INSERT INTO [Items] ([Name], [Price], [UserCreatedId], [UserUpdatedId], [CategoryId]) VALUES ('VIGOR', 7, 1, 1, 3)

INSERT INTO [Items] ([Name], [Price], [UserCreatedId], [UserUpdatedId], [CategoryId]) VALUES ('TRAVARICA', 7, 1, 1, 6)

INSERT INTO [Items] ([Name], [Price], [UserCreatedId], [UserUpdatedId], [CategoryId]) VALUES ('KOMOVICA', 7, 1, 1, 6)

INSERT INTO [Items] ([Name], [Price], [UserCreatedId], [UserUpdatedId], [CategoryId]) VALUES ('VILJAMOVKA', 20, 1, 1, 6)

INSERT INTO [Items] ([Name], [Price], [UserCreatedId], [UserUpdatedId], [CategoryId]) VALUES ('CHIVAS', 20, 1, 1, 3)

INSERT INTO [Items] ([Name], [Price], [UserCreatedId], [UserUpdatedId], [CategoryId]) VALUES ('BALANTINES', 12, 1, 1, 3)

INSERT INTO [Items] ([Name], [Price], [UserCreatedId], [UserUpdatedId], [CategoryId]) VALUES ('JACKY DANIELS', 15, 1, 1, 3)

INSERT INTO [Items] ([Name], [Price], [UserCreatedId], [UserUpdatedId], [CategoryId]) VALUES ('JIM BEAM', 12, 1, 1, 3)

INSERT INTO [Items] ([Name], [Price], [UserCreatedId], [UserUpdatedId], [CategoryId]) VALUES ('MARTEL', 20, 1, 1, 3)

INSERT INTO [Items] ([Name], [Price], [UserCreatedId], [UserUpdatedId], [CategoryId]) VALUES ('STOCK', 9, 1, 1, 3)

INSERT INTO [Items] ([Name], [Price], [UserCreatedId], [UserUpdatedId], [CategoryId]) VALUES ('BACARDI', 12, 1, 1, 3)

INSERT INTO [Items] ([Name], [Price], [UserCreatedId], [UserUpdatedId], [CategoryId]) VALUES ('SMIRNOFF', 12, 1, 1, 3)

INSERT INTO [Items] ([Name], [Price], [UserCreatedId], [UserUpdatedId], [CategoryId]) VALUES ('JAGERMEISTER', 12, 1, 1, 7)

INSERT INTO [Items] ([Name], [Price], [UserCreatedId], [UserUpdatedId], [CategoryId]) VALUES ('ŠLJIVOVICA', 10, 1, 1, 6)

INSERT INTO [Items] ([Name], [Price], [UserCreatedId], [UserUpdatedId], [CategoryId]) VALUES ('VOĆNI LIKER', 9, 1, 1, 7)

INSERT INTO [Items] ([Name], [Price], [UserCreatedId], [UserUpdatedId], [CategoryId]) VALUES ('CAMPARI', 12, 1, 1, 7)

INSERT INTO [Items] ([Name], [Price], [UserCreatedId], [UserUpdatedId], [CategoryId]) VALUES ('MEDENICA', 12, 1, 1, 6)

INSERT INTO [Items] ([Name], [Price], [UserCreatedId], [UserUpdatedId], [CategoryId]) VALUES ('BLACKY', 7, 1, 1, 7)

INSERT INTO [Items] ([Name], [Price], [UserCreatedId], [UserUpdatedId], [CategoryId]) VALUES ('BOROVNICA RAKIJA', 12, 1, 1, 6)

INSERT INTO [Records] (TotalEarnings, Comment, OfficeId, UserCreatedId, UserUpdatedId, DateCreated) VALUES (1200, 'Ovo je komentar za ovaj record 1', 1, 1, 1, '20180203')

INSERT INTO [Records] (TotalEarnings, Comment, OfficeId, UserCreatedId, UserUpdatedId, DateCreated) VALUES (2200, 'Ovo je komentar za ovaj record 2', 1, 1, 1, '20180204')

INSERT INTO [Records] (TotalEarnings, Comment, OfficeId, UserCreatedId, UserUpdatedId, DateCreated) VALUES (3200, 'Ovo je komentar za ovaj record 3', 1, 1, 1, '20180205')

INSERT INTO [Records] (TotalEarnings, Comment, OfficeId, UserCreatedId, UserUpdatedId, DateCreated) VALUES (4200, 'Ovo je komentar za ovaj record 3', 1, 1, 1, '20180206')

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (1, 1, 31, 18, 54, 38, 92, 24);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (1, 2, 82, 56, 8, 41, 17, 61);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (1, 3, 62, 66, 45, 23, 61, 32);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (1, 4, 39, 91, 38, 38, 96, 14);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (1, 5, 13, 1, 96, 35, 9, 97);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (1, 6, 9, 69, 39, 90, 79, 88);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (1, 7, 92, 99, 20, 35, 83, 71);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (1, 8, 60, 51, 96, 70, 65, 17);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (1, 9, 81, 91, 90, 44, 71, 59);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (1, 10, 15, 45, 51, 91, 97, 97);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (1, 11, 20, 33, 37, 26, 46, 31);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (1, 12, 3, 99, 31, 66, 16, 54);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (1, 13, 19, 80, 85, 80, 18, 50);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (1, 14, 87, 71, 10, 51, 40, 45);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (1, 15, 82, 70, 55, 24, 51, 93);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (1, 16, 70, 6, 25, 9, 35, 82);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (1, 17, 95, 85, 71, 97, 61, 8);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (1, 18, 19, 33, 68, 24, 57, 81);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (1, 19, 77, 65, 63, 70, 48, 7);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (1, 20, 58, 18, 72, 2, 63, 5);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (1, 21, 41, 16, 17, 20, 3, 7);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (1, 22, 83, 23, 58, 3, 78, 21);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (1, 23, 94, 67, 48, 21, 69, 20);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (1, 24, 31, 30, 24, 100, 62, 57);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (1, 25, 18, 80, 49, 85, 85, 33);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (1, 26, 8, 81, 9, 86, 98, 68);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (1, 27, 72, 60, 59, 43, 64, 26);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (1, 28, 52, 83, 40, 29, 100, 5);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (1, 29, 16, 39, 40, 10, 5, 37);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (1, 30, 72, 11, 94, 10, 33, 83);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (1, 31, 24, 5, 8, 2, 39, 30);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (1, 32, 23, 15, 35, 2, 5, 39);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (1, 33, 38, 36, 4, 15, 39, 43);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (1, 34, 97, 89, 72, 71, 19, 9);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (1, 35, 65, 66, 10, 9, 27, 95);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (1, 36, 51, 50, 17, 55, 86, 51);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (1, 37, 59, 3, 1, 51, 98, 98);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (1, 38, 42, 75, 59, 24, 94, 21);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (1, 39, 89, 51, 97, 6, 62, 34);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (1, 40, 22, 77, 23, 19, 46, 13);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (1, 41, 10, 68, 48, 13, 73, 40);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (1, 42, 40, 83, 51, 1, 2, 49);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (1, 43, 89, 35, 69, 76, 47, 82);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (1, 44, 86, 40, 6, 68, 8, 87);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (1, 45, 79, 89, 98, 46, 56, 16);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (1, 46, 85, 99, 69, 23, 74, 6);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (1, 47, 92, 34, 31, 52, 22, 64);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (1, 48, 10, 77, 68, 30, 62, 19);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (1, 49, 72, 7, 88, 96, 7, 89);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (1, 50, 71, 86, 31, 2, 92, 33);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (1, 51, 32, 77, 30, 32, 14, 75);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (1, 52, 49, 9, 53, 13, 82, 78);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (1, 53, 39, 27, 74, 51, 26, 72);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (1, 54, 70, 9, 17, 1, 33, 75);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (1, 55, 58, 58, 97, 83, 74, 54);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (1, 56, 87, 64, 34, 29, 63, 33);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (1, 57, 7, 55, 67, 1, 29, 22);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (1, 58, 67, 30, 66, 33, 58, 50);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (1, 59, 73, 47, 9, 41, 66, 21);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (1, 60, 96, 95, 77, 76, 25, 40);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (1, 61, 6, 63, 55, 94, 71, 81);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (1, 62, 78, 34, 50, 29, 8, 49);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (1, 63, 65, 15, 50, 82, 80, 50);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (1, 64, 27, 88, 56, 40, 13, 94);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (1, 65, 22, 81, 69, 47, 80, 87);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (1, 66, 14, 24, 16, 5, 42, 81);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (1, 67, 2, 68, 55, 38, 37, 92);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (1, 68, 54, 45, 71, 80, 85, 48);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (1, 69, 21, 11, 55, 75, 68, 83);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (1, 70, 84, 49, 70, 70, 26, 91);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (1, 71, 43, 1, 26, 24, 94, 50);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (1, 72, 11, 95, 93, 19, 84, 30);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (1, 73, 50, 41, 33, 70, 1, 82);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (1, 74, 26, 47, 74, 23, 15, 83);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (1, 75, 61, 31, 80, 67, 7, 45);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (1, 76, 70, 12, 50, 1, 10, 22);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (1, 77, 14, 20, 39, 46, 72, 22);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (1, 78, 100, 10, 70, 45, 60, 84);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (1, 79, 49, 51, 97, 12, 96, 50);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (1, 80, 65, 99, 18, 77, 50, 93);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (1, 81, 38, 25, 87, 18, 65, 91);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (1, 82, 61, 50, 57, 38, 6, 59);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (1, 83, 70, 45, 69, 82, 22, 82);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (1, 84, 57, 43, 58, 31, 71, 75);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (1, 85, 5, 4, 37, 36, 100, 94);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (1, 86, 88, 93, 47, 9, 71, 25);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (1, 87, 12, 9, 60, 43, 98, 46);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (1, 88, 90, 66, 6, 86, 82, 100);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (2, 1, 41, 18, 8, 65, 86, 82);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (2, 2, 84, 84, 29, 76, 53, 70);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (2, 3, 80, 12, 12, 62, 29, 2);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (2, 4, 18, 31, 22, 29, 35, 11);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (2, 5, 97, 42, 98, 76, 31, 69);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (2, 6, 78, 14, 65, 94, 70, 35);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (2, 7, 74, 89, 100, 91, 62, 22);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (2, 8, 80, 86, 16, 47, 52, 62);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (2, 9, 18, 45, 39, 81, 12, 68);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (2, 10, 36, 47, 29, 37, 1, 15);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (2, 11, 52, 65, 80, 26, 86, 59);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (2, 12, 5, 69, 93, 33, 29, 81);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (2, 13, 37, 44, 56, 92, 14, 70);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (2, 14, 58, 31, 33, 96, 31, 16);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (2, 15, 96, 89, 58, 18, 8, 61);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (2, 16, 37, 82, 81, 46, 16, 15);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (2, 17, 24, 58, 57, 94, 6, 86);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (2, 18, 76, 84, 95, 40, 48, 52);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (2, 19, 41, 80, 81, 36, 39, 8);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (2, 20, 17, 78, 65, 5, 45, 58);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (2, 21, 82, 46, 54, 58, 13, 51);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (2, 22, 70, 30, 75, 56, 57, 46);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (2, 23, 8, 81, 88, 90, 87, 69);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (2, 24, 48, 36, 12, 40, 3, 29);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (2, 25, 84, 66, 92, 35, 23, 90);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (2, 26, 100, 70, 59, 55, 66, 51);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (2, 27, 32, 44, 52, 63, 13, 76);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (2, 28, 38, 31, 58, 70, 84, 64);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (2, 29, 90, 22, 87, 54, 67, 20);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (2, 30, 70, 65, 90, 3, 20, 13);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (2, 31, 70, 70, 8, 17, 52, 37);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (2, 32, 5, 72, 33, 21, 80, 9);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (2, 33, 70, 49, 32, 54, 25, 55);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (2, 34, 74, 65, 57, 90, 69, 70);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (2, 35, 24, 77, 47, 3, 84, 47);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (2, 36, 34, 72, 38, 80, 78, 85);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (2, 37, 50, 31, 14, 9, 22, 95);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (2, 38, 85, 63, 14, 40, 8, 11);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (2, 39, 40, 72, 5, 67, 17, 68);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (2, 40, 20, 91, 17, 40, 54, 50);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (2, 41, 47, 49, 6, 14, 36, 22);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (2, 42, 100, 89, 13, 91, 58, 98);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (2, 43, 21, 86, 36, 4, 45, 89);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (2, 44, 97, 61, 96, 75, 16, 36);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (2, 45, 62, 37, 27, 72, 70, 88);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (2, 46, 63, 66, 84, 50, 63, 96);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (2, 47, 95, 49, 32, 20, 85, 40);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (2, 48, 71, 61, 84, 39, 58, 70);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (2, 49, 17, 62, 33, 92, 44, 30);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (2, 50, 10, 94, 75, 96, 42, 65);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (2, 51, 24, 69, 14, 69, 39, 94);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (2, 52, 88, 31, 22, 6, 50, 28);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (2, 53, 26, 42, 56, 95, 85, 95);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (2, 54, 96, 4, 27, 99, 95, 50);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (2, 55, 5, 14, 1, 23, 74, 63);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (2, 56, 31, 83, 36, 10, 31, 60);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (2, 57, 67, 47, 92, 72, 13, 65);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (2, 58, 15, 41, 35, 92, 82, 67);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (2, 59, 72, 27, 85, 33, 68, 86);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (2, 60, 28, 80, 74, 14, 73, 68);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (2, 61, 63, 59, 88, 96, 64, 26);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (2, 62, 15, 43, 56, 53, 43, 8);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (2, 63, 21, 81, 47, 33, 38, 24);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (2, 64, 60, 98, 9, 61, 2, 92);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (2, 65, 48, 78, 58, 30, 27, 69);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (2, 66, 96, 60, 31, 57, 100, 30);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (2, 67, 100, 51, 85, 70, 56, 60);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (2, 68, 7, 8, 80, 78, 49, 57);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (2, 69, 54, 28, 20, 94, 27, 18);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (2, 70, 24, 9, 35, 57, 65, 17);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (2, 71, 93, 39, 84, 80, 27, 12);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (2, 72, 21, 24, 43, 64, 14, 70);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (2, 73, 79, 49, 70, 92, 31, 33);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (2, 74, 76, 21, 1, 18, 41, 3);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (2, 75, 5, 75, 54, 64, 80, 16);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (2, 76, 50, 36, 80, 71, 13, 52);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (2, 77, 20, 86, 68, 100, 3, 54);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (2, 78, 94, 66, 89, 91, 97, 29);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (2, 79, 74, 12, 51, 53, 75, 79);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (2, 80, 78, 88, 58, 95, 74, 90);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (2, 81, 46, 16, 87, 70, 9, 13);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (2, 82, 88, 15, 66, 39, 38, 49);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (2, 83, 69, 84, 13, 15, 21, 50);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (2, 84, 82, 71, 23, 36, 100, 8);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (2, 85, 8, 41, 79, 40, 90, 68);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (2, 86, 54, 30, 70, 99, 65, 73);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (2, 87, 22, 63, 37, 62, 56, 84);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (2, 88, 78, 38, 10, 56, 4, 17);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (3, 1, 87, 99, 36, 75, 98, 84);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (3, 2, 54, 27, 74, 1, 22, 92);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (3, 3, 13, 100, 95, 23, 61, 15);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (3, 4, 70, 49, 78, 61, 53, 70);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (3, 5, 27, 93, 86, 25, 11, 82);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (3, 6, 55, 57, 93, 55, 18, 10);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (3, 7, 31, 1, 57, 19, 49, 47);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (3, 8, 99, 53, 4, 93, 84, 61);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (3, 9, 96, 44, 80, 36, 36, 62);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (3, 10, 64, 15, 15, 15, 99, 35);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (3, 11, 99, 92, 90, 33, 59, 75);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (3, 12, 93, 88, 23, 99, 56, 48);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (3, 13, 53, 13, 54, 99, 87, 39);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (3, 14, 34, 52, 1, 86, 44, 50);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (3, 15, 51, 51, 33, 61, 2, 61);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (3, 16, 78, 90, 65, 29, 59, 74);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (3, 17, 96, 54, 36, 92, 4, 90);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (3, 18, 3, 94, 87, 46, 69, 7);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (3, 19, 20, 42, 16, 30, 61, 21);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (3, 20, 59, 86, 16, 77, 68, 25);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (3, 21, 80, 42, 13, 31, 75, 61);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (3, 22, 31, 61, 31, 12, 32, 42);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (3, 23, 26, 19, 15, 69, 2, 11);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (3, 24, 66, 96, 70, 97, 54, 6);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (3, 25, 1, 9, 29, 44, 47, 21);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (3, 26, 12, 13, 5, 32, 55, 87);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (3, 27, 71, 6, 100, 25, 44, 29);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (3, 28, 88, 44, 2, 38, 84, 66);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (3, 29, 54, 51, 8, 51, 85, 32);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (3, 30, 48, 57, 24, 70, 30, 88);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (3, 31, 71, 80, 61, 11, 59, 62);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (3, 32, 85, 42, 55, 20, 83, 4);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (3, 33, 60, 38, 47, 46, 13, 44);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (3, 34, 71, 18, 50, 69, 8, 74);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (3, 35, 77, 33, 26, 96, 16, 22);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (3, 36, 31, 19, 70, 69, 70, 37);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (3, 37, 40, 3, 38, 59, 57, 45);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (3, 38, 87, 55, 35, 69, 53, 17);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (3, 39, 89, 4, 6, 16, 93, 31);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (3, 40, 23, 10, 29, 7, 46, 58);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (3, 41, 2, 29, 98, 87, 33, 4);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (3, 42, 46, 83, 33, 42, 58, 91);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (3, 43, 51, 32, 24, 42, 20, 38);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (3, 44, 95, 27, 70, 52, 82, 69);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (3, 45, 13, 85, 64, 58, 48, 35);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (3, 46, 95, 9, 84, 23, 60, 64);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (3, 47, 5, 27, 72, 15, 19, 66);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (3, 48, 1, 56, 62, 18, 55, 62);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (3, 49, 9, 45, 1, 40, 5, 48);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (3, 50, 10, 32, 74, 44, 6, 13);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (3, 51, 37, 26, 56, 99, 78, 77);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (3, 52, 34, 34, 19, 43, 96, 81);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (3, 53, 11, 25, 25, 89, 37, 84);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (3, 54, 94, 6, 62, 54, 83, 58);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (3, 55, 26, 91, 33, 82, 70, 66);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (3, 56, 96, 87, 100, 9, 9, 7);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (3, 57, 76, 43, 21, 55, 63, 72);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (3, 58, 17, 5, 71, 10, 84, 11);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (3, 59, 9, 60, 64, 87, 48, 41);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (3, 60, 24, 59, 20, 44, 51, 37);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (3, 61, 66, 32, 65, 85, 77, 93);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (3, 62, 40, 33, 17, 96, 55, 51);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (3, 63, 70, 8, 94, 49, 52, 6);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (3, 64, 12, 83, 2, 41, 89, 65);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (3, 65, 69, 47, 66, 13, 54, 39);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (3, 66, 34, 88, 38, 97, 36, 62);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (3, 67, 15, 80, 35, 19, 27, 92);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (3, 68, 69, 72, 76, 1, 97, 2);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (3, 69, 59, 19, 35, 56, 92, 81);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (3, 70, 12, 64, 80, 89, 97, 67);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (3, 71, 73, 62, 30, 53, 29, 23);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (3, 72, 75, 75, 81, 20, 26, 15);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (3, 73, 19, 98, 56, 95, 63, 75);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (3, 74, 94, 62, 70, 82, 90, 92);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (3, 75, 6, 2, 27, 99, 97, 66);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (3, 76, 90, 61, 25, 28, 83, 55);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (3, 77, 64, 52, 68, 78, 69, 3);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (3, 78, 21, 61, 23, 83, 98, 32);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (3, 79, 10, 97, 19, 57, 96, 27);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (3, 80, 52, 34, 25, 2, 92, 87);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (3, 81, 66, 34, 29, 64, 84, 15);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (3, 82, 30, 41, 85, 12, 18, 92);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (3, 83, 13, 7, 34, 20, 2, 94);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (3, 84, 73, 42, 22, 47, 90, 50);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (3, 85, 73, 55, 89, 99, 19, 53);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (3, 86, 4, 64, 83, 61, 51, 54);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (3, 87, 70, 54, 26, 29, 92, 73);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (3, 88, 77, 100, 98, 52, 80, 21);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (4, 1, 11, 14, 53, 31, 36, 29);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (4, 2, 60, 93, 44, 78, 77, 46);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (4, 3, 14, 6, 36, 21, 78, 28);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (4, 4, 1, 1, 83, 97, 99, 90);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (4, 5, 96, 93, 45, 19, 58, 19);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (4, 6, 64, 43, 78, 64, 46, 59);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (4, 7, 10, 68, 21, 92, 61, 71);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (4, 8, 13, 25, 42, 13, 60, 78);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (4, 9, 97, 97, 84, 47, 88, 15);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (4, 10, 11, 40, 98, 54, 100, 1);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (4, 11, 1, 2, 100, 43, 40, 36);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (4, 12, 13, 95, 16, 82, 46, 66);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (4, 13, 91, 99, 10, 12, 2, 2);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (4, 14, 73, 28, 82, 14, 91, 61);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (4, 15, 37, 52, 83, 32, 13, 86);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (4, 16, 26, 20, 96, 62, 98, 33);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (4, 17, 81, 14, 61, 99, 32, 5);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (4, 18, 11, 23, 66, 26, 5, 7);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (4, 19, 53, 21, 43, 43, 27, 71);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (4, 20, 65, 65, 73, 43, 10, 37);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (4, 21, 71, 84, 43, 64, 48, 73);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (4, 22, 35, 1, 62, 27, 48, 2);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (4, 23, 92, 32, 64, 52, 48, 73);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (4, 24, 54, 44, 88, 79, 52, 77);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (4, 25, 98, 1, 94, 68, 25, 72);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (4, 26, 94, 33, 60, 68, 21, 92);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (4, 27, 72, 28, 50, 80, 35, 95);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (4, 28, 85, 1, 16, 62, 25, 97);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (4, 29, 68, 90, 72, 42, 6, 89);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (4, 30, 39, 31, 9, 18, 16, 18);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (4, 31, 68, 1, 40, 88, 21, 6);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (4, 32, 81, 44, 87, 80, 3, 46);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (4, 33, 100, 30, 93, 80, 10, 76);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (4, 34, 39, 81, 3, 48, 36, 1);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (4, 35, 94, 69, 1, 37, 98, 79);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (4, 36, 69, 98, 85, 95, 18, 42);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (4, 37, 62, 93, 62, 32, 40, 28);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (4, 38, 54, 42, 34, 72, 35, 44);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (4, 39, 96, 10, 97, 95, 8, 3);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (4, 40, 45, 53, 5, 65, 67, 26);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (4, 41, 22, 80, 44, 97, 94, 42);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (4, 42, 84, 76, 59, 7, 82, 57);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (4, 43, 23, 63, 47, 84, 67, 49);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (4, 44, 53, 41, 32, 95, 66, 1);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (4, 45, 7, 34, 97, 58, 24, 50);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (4, 46, 12, 44, 91, 53, 98, 97);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (4, 47, 92, 11, 16, 30, 19, 61);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (4, 48, 3, 40, 84, 75, 9, 99);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (4, 49, 51, 88, 56, 26, 27, 24);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (4, 50, 79, 1, 25, 20, 48, 81);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (4, 51, 24, 55, 41, 94, 78, 21);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (4, 52, 30, 95, 15, 33, 3, 74);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (4, 53, 3, 98, 48, 30, 78, 89);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (4, 54, 26, 87, 69, 80, 7, 77);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (4, 55, 42, 84, 5, 19, 17, 66);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (4, 56, 49, 77, 23, 17, 98, 38);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (4, 57, 80, 44, 73, 74, 100, 98);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (4, 58, 43, 84, 31, 8, 29, 100);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (4, 59, 66, 63, 40, 26, 24, 63);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (4, 60, 44, 81, 65, 32, 12, 85);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (4, 61, 93, 51, 25, 82, 85, 52);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (4, 62, 80, 12, 3, 21, 60, 57);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (4, 63, 74, 16, 56, 64, 14, 67);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (4, 64, 56, 90, 6, 71, 15, 53);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (4, 65, 72, 8, 91, 91, 100, 83);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (4, 66, 26, 57, 92, 7, 7, 18);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (4, 67, 41, 80, 84, 3, 75, 37);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (4, 68, 57, 34, 79, 10, 74, 31);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (4, 69, 85, 81, 56, 98, 44, 29);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (4, 70, 52, 90, 47, 38, 70, 59);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (4, 71, 67, 46, 38, 20, 72, 40);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (4, 72, 1, 48, 34, 95, 75, 31);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (4, 73, 48, 7, 12, 62, 33, 55);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (4, 74, 88, 11, 87, 83, 45, 85);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (4, 75, 94, 28, 4, 27, 89, 24);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (4, 76, 100, 45, 54, 6, 100, 96);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (4, 77, 9, 7, 68, 5, 100, 30);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (4, 78, 17, 27, 20, 67, 64, 9);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (4, 79, 24, 21, 60, 50, 53, 52);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (4, 80, 31, 50, 94, 98, 24, 54);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (4, 81, 63, 87, 4, 100, 64, 45);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (4, 82, 93, 85, 83, 94, 68, 16);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (4, 83, 56, 5, 98, 34, 37, 34);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (4, 84, 37, 1, 82, 82, 22, 84);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (4, 85, 55, 8, 39, 64, 18, 15);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (4, 86, 46, 97, 49, 87, 89, 82);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (4, 87, 40, 74, 3, 89, 84, 11);

INSERT INTO [RecordItems] (RecordId , ItemId, YesterdaysStateOfTheBar, Received, InTotal, TodaysStateOfTheBar, SoldOut, SumOfSoldArticlesPrice) VALUES (4, 88, 10, 9, 53, 64, 88, 24);

SET IDENTITY_INSERT [Shifts] ON

INSERT INTO [Shifts] (ShiftId, Name, Code, UserCreatedId, UserUpdatedId)  VALUES (1, 'Prva smjena', '1S', 1 , 1)

INSERT INTO [Shifts] (ShiftId, Name, Code, UserCreatedId, UserUpdatedId)  VALUES (2, 'Druga smjena', '2S', 1 , 1)

SET IDENTITY_INSERT [Shifts] OFF

INSERT INTO WorkerLogs(UserId, ShiftId, RecordId, ShiftEarnings) VALUES (4, 1, 1, 1200)

INSERT INTO WorkerLogs(UserId, ShiftId, RecordId, ShiftEarnings) VALUES (5, 2, 1, 700)

INSERT INTO WorkerLogs(UserId, ShiftId, RecordId, ShiftEarnings) VALUES (4, 1, 2, 1200)

INSERT INTO WorkerLogs(UserId, ShiftId, RecordId, ShiftEarnings) VALUES (5, 2, 2, 200)

INSERT INTO WorkerLogs(UserId, ShiftId, RecordId, ShiftEarnings) VALUES (5, 1, 3, 500)

INSERT INTO WorkerLogs(UserId, ShiftId, RecordId, ShiftEarnings) VALUES (4, 2, 3, 1600)

INSERT INTO WorkerLogs(UserId, ShiftId, RecordId, ShiftEarnings) VALUES (5, 1, 4, 1200)

INSERT INTO WorkerLogs(UserId, ShiftId, RecordId, ShiftEarnings) VALUES (4, 2, 4, 1200)

COMMIT TRANSACTION ORMS_INSERT_DB