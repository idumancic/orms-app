SET XACT_ABORT ON
BEGIN TRANSACTION ORMS_CONSTRAINTS_DB

ALTER TABLE [Users] WITH CHECK ADD CONSTRAINT [FK_Users_OfficeId] FOREIGN KEY([OfficeId])
REFERENCES [Offices] ([OfficeId])

ALTER TABLE [Users] CHECK CONSTRAINT [FK_Users_OfficeId]

ALTER TABLE [Users] WITH CHECK ADD CONSTRAINT [FK_Users_UserCreatedId] FOREIGN KEY([UserCreatedId])
REFERENCES [Users] ([UserId])

ALTER TABLE [Users] CHECK CONSTRAINT [FK_Users_UserCreatedId]

ALTER TABLE [Users] WITH CHECK ADD CONSTRAINT [FK_Users_UserUpdatedId] FOREIGN KEY([UserUpdatedId])
REFERENCES [Users] ([UserId])

ALTER TABLE [Users] CHECK CONSTRAINT [FK_Users_UserUpdatedId]

ALTER TABLE [Users] WITH CHECK ADD CONSTRAINT [FK_Users_Code] FOREIGN KEY([RoleCode])
REFERENCES [Roles] ([RoleCode])

ALTER TABLE [Users] CHECK CONSTRAINT [FK_Users_Code]

ALTER TABLE [Records] WITH CHECK ADD CONSTRAINT [FK_Records_OfficeId] FOREIGN KEY([OfficeId])
REFERENCES [Offices] ([OfficeId])

ALTER TABLE [Records] CHECK CONSTRAINT [FK_Records_OfficeId]

ALTER TABLE [Records] WITH CHECK ADD CONSTRAINT [FK_Records_UserCreatedId] FOREIGN KEY([UserCreatedId])
REFERENCES [Users] ([UserId])

ALTER TABLE [Records] CHECK CONSTRAINT [FK_Records_UserCreatedId]

ALTER TABLE [Records] WITH CHECK ADD CONSTRAINT [FK_Records_UserUpdatedId] FOREIGN KEY([UserUpdatedId])
REFERENCES [Users] ([UserId])

ALTER TABLE [Records] CHECK CONSTRAINT [FK_Records_UserUpdatedId]

ALTER TABLE [Categories] WITH CHECK ADD CONSTRAINT [FK_Categories_UserCreatedId] FOREIGN KEY([UserCreatedId])
REFERENCES [User] ([UserId])

ALTER TABLE [Categories] CHECK CONSTRAINT [FK_Categories_UserCreatedId]

ALTER TABLE [Categories] WITH CHECK ADD CONSTRAINT [FK_Categories_UserUpdatedId] FOREIGN KEY([UserUpdatedId])
REFERENCES [User] ([UserId])

ALTER TABLE [Categories] CHECK CONSTRAINT [FK_Categories_UserUpdatedId]

ALTER TABLE [Items] WITH CHECK ADD CONSTRAINT [FK_Items_UserCreatedId] FOREIGN KEY([UserCreatedId])
REFERENCES [Users] ([UserId])

ALTER TABLE [Items] CHECK CONSTRAINT [FK_Items_UserCreatedId]

ALTER TABLE [Items] WITH CHECK ADD CONSTRAINT [FK_Items_UserUpdatedId] FOREIGN KEY([UserUpdatedId])
REFERENCES [Users] ([UserId])

ALTER TABLE [Items] CHECK CONSTRAINT [FK_Items_UserUpdatedId]

ALTER TABLE [Items] WITH CHECK ADD CONSTRAINT [FK_Items_CategoryId] FOREIGN KEY([CategoryId])
REFERENCES [Categories] ([CategoryId])

ALTER TABLE [Items] CHECK CONSTRAINT [FK_Items_CategoryId]

ALTER TABLE [RecordItems] WITH CHECK ADD CONSTRAINT [FK_RecordItems_ItemId] FOREIGN KEY([ItemId])
REFERENCES [Items] ([ItemId])

ALTER TABLE [RecordItems] CHECK CONSTRAINT [FK_RecordItems_ItemId]

ALTER TABLE [RecordItems] WITH CHECK ADD CONSTRAINT [FK_RecordItems_RecordId] FOREIGN KEY([RecordId])
REFERENCES [Records] ([RecordId])

ALTER TABLE [RecordItems] CHECK CONSTRAINT [FK_RecordItems_RecordId]

ALTER TABLE [Shifts] WITH CHECK ADD CONSTRAINT [FK_Shifts_UserCreatedId] FOREIGN KEY([UserCreatedId])
REFERENCES [Users] ([UserId])

ALTER TABLE [Shifts] CHECK CONSTRAINT [FK_Shifts_UserCreatedId]

ALTER TABLE [Shifts] WITH CHECK ADD CONSTRAINT [FK_Shifts_UserUpdatedId] FOREIGN KEY([UserUpdatedId])
REFERENCES [Users] ([UserId])

ALTER TABLE [Shifts] CHECK CONSTRAINT [FK_Shifts_UserUpdatedId]

ALTER TABLE [WorkerLogs] WITH CHECK ADD CONSTRAINT [FK_WorkerLogs_UserId] FOREIGN KEY([UserId])
REFERENCES [Users] ([UserId])

ALTER TABLE [WorkerLogs] CHECK CONSTRAINT [FK_WorkerLogs_UserId]

ALTER TABLE [WorkerLogs] WITH CHECK ADD CONSTRAINT [FK_WorkerLogs_ShiftId] FOREIGN KEY([ShiftId])
REFERENCES [Shifts] ([ShiftId])

ALTER TABLE [WorkerLogs] CHECK CONSTRAINT [FK_WorkerLogs_ShiftId]

ALTER TABLE [WorkerLogs] WITH CHECK ADD CONSTRAINT [FK_WorkerLogs_RecordId] FOREIGN KEY([RecordId])
REFERENCES [Records] ([RecordId])

ALTER TABLE [WorkerLogs] CHECK CONSTRAINT [FK_WorkerLogs_RecordId]

ALTER TABLE [Offices] WITH CHECK ADD CONSTRAINT [FK_Offices_UserCreatedId] FOREIGN KEY([UserCreatedId])
REFERENCES [Users] ([UserId])

ALTER TABLE [Offices] CHECK CONSTRAINT [FK_Offices_UserCreatedId]

ALTER TABLE [Offices] WITH CHECK ADD CONSTRAINT [FK_Offices_UserUpdatedId] FOREIGN KEY([UserUpdatedId])
REFERENCES [Users] ([UserId])

ALTER TABLE [Offices] CHECK CONSTRAINT [FK_Offices_UserUpdatedId]

COMMIT TRANSACTION ORMS_CONSTRAINTS_DB