CREATE DATABASE ORMS_DB
GO

USE ORMS_DB
GO

SET XACT_ABORT ON
BEGIN TRANSACTION ORMS_CREATE_DB

CREATE TABLE [Users]
(
	[UserId] int IDENTITY(1,1) NOT NULL,
	[Username] varchar(255) NOT NULL,
	[Password] varchar(255) NOT NULL,
	[FirstName] nvarchar(255) NULL,
	[LastName] nvarchar(255) NULL,
	[DateOfBirth] date NULL,
	[JMBG] varchar(13) NULL,
	[OIB] varchar(11) NULL,
	[Address] nvarchar(255) NULL,
	[PostalCode] nvarchar(5) NULL,
	[City] nvarchar(255) NULL,
	[PhoneNumber] varchar(50) NULL,
	[Email] varchar(100) NULL,
	[OfficeId] int NULL,
	[SanitaryIdVerified] int NULL,
	[SanitaryIdCreatedDate] date NULL,
	[SanitaryIdExpirationDate] date NULL,
	[RoleCode] varchar(30) NOT NULL,
	[Active] int DEFAULT 1 NOT NULL,
	[UserCreatedId] int NOT NULL,
	[UserUpdatedId] int NOT NULL,
	[DateCreated] datetime DEFAULT GETDATE() NOT NULL,
	[DateUpdated] datetime DEFAULT GETDATE() NOT NULL,
	CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED ([UserId] ASC)
)

CREATE TABLE [Roles] 
(
	[RoleCode] varchar(30) NOT NULL,
	[Name] varchar(30) NOT NULL,
	[Active] int DEFAULT 1 NOT NULL,
	[DateCreated] datetime DEFAULT GETDATE() NOT NULL,
	[DateUpdated] datetime DEFAULT GETDATE() NOT NULL,
	CONSTRAINT [PK_Roles] PRIMARY KEY CLUSTERED ([RoleCode] ASC)
)

CREATE TABLE [Records]
(
	[RecordId] int IDENTITY(1,1) NOT NULL,
	[TotalEarnings] money NOT NULL,
	[Comment] nvarchar(300) NULL,
	[OfficeId] int NOT NULL,
	[Active] int DEFAULT 1 NOT NULL,
	[UserCreatedId] int NOT NULL,
	[UserUpdatedId] int NOT NULL,
	[DateCreated] datetime DEFAULT GETDATE() NOT NULL,
	[DateUpdated] datetime DEFAULT GETDATE() NOT NULL,
	CONSTRAINT [PK_Records] PRIMARY KEY CLUSTERED ([RecordId] ASC)
)

CREATE TABLE [Items]
(
	[ItemId] int IDENTITY(1,1) NOT NULL,
	[Name] nvarchar(255) NOT NULL,
	[Price] money NOT NULL,
	[CategoryId] int NOT NULL,
	[Active] int DEFAULT 1 NOT NULL,
	[UserCreatedId] int NOT NULL,
	[UserUpdatedId] int NOT NULL,
	[DateCreated] datetime DEFAULT GETDATE() NOT NULL,
	[DateUpdated] datetime DEFAULT GETDATE() NOT NULL,
	CONSTRAINT [PK_Items] PRIMARY KEY CLUSTERED ([ItemId] ASC)
)

CREATE TABLE [Categories]
(
	[CategoryId] int IDENTITY(1,1) NOT NULL,
	[Name] nvarchar(255) NOT NULL,
	[Active] int DEFAULT 1 NOT NULL,
	[UserCreatedId] int NOT NULL,
	[UserUpdatedId] int NOT NULL,
	[DateCreated] datetime DEFAULT GETDATE() NOT NULL,
	[DateUpdated] datetime DEFAULT GETDATE() NOT NULL,
	CONSTRAINT [PK_Categories] PRIMARY KEY CLUSTERED ([CategoryId] ASC)
)


CREATE TABLE [RecordItems]
(
	[RecordItemId] int IDENTITY(1,1) NOT NULL,
	[RecordId] int NOT NULL,
	[ItemId] int NOT NULL,
	[YesterdaysStateOfTheBar] int NOT NULL,
	[Received] int NULL,
	[InTotal] int NULL,
	[TodaysStateOfTheBar] int NULL,
	[SoldOut] int NULL,
	[SumOfSoldArticlesPrice] int NULL,
	CONSTRAINT [PK_RecordItems] PRIMARY KEY CLUSTERED ([RecordItemId] ASC)
)


CREATE TABLE [Shifts]
(
	[ShiftId] int IDENTITY(1,1) NOT NULL,
	[Name] varchar(50) NOT NULL,
	[Code] varchar(3) NOT NULL,
	[Active] int DEFAULT 1 NOT NULL,
	[UserCreatedId] int NOT NULL,
	[UserUpdatedId] int NOT NULL,
	[DateCreated] datetime DEFAULT GETDATE() NOT NULL,
	[DateUpdated] datetime DEFAULT GETDATE() NOT NULL,
	CONSTRAINT [PK_Shifts] PRIMARY KEY CLUSTERED ([ShiftId] ASC)
)


CREATE TABLE [WorkerLogs]
(
	[WorkerLogId] int IDENTITY(1,1) NOT NULL,
	[UserId] int NOT NULL,
	[ShiftId] int NOT NULL,
	[RecordId] int NOT NULL,
	[ShiftEarnings] money NOT NULL,
	[DateCreated] datetime DEFAULT GETDATE() NOT NULL,
	[DateUpdated] datetime DEFAULT GETDATE() NOT NULL,
	CONSTRAINT [PK_WorkerLogs] PRIMARY KEY CLUSTERED ([WorkerLogId] ASC)
)

CREATE TABLE [Offices]
(
	[OfficeId] int IDENTITY(1,1) NOT NULL,
	[Name] nvarchar(255) NOT NULL,
	[Address] nvarchar(255) NULL,
	[PostalCode] nvarchar(5) NULL,
	[Place] nvarchar(255) NULL,
	[City] nvarchar(255) NULL,
	[OIB] varchar(11) NOT NULL,
	[Active] int DEFAULT 1 NOT NULL,
	[UserCreatedId] int NOT NULL,
	[UserUpdatedId] int NOT NULL,
	[DateCreated] datetime DEFAULT GETDATE() NOT NULL,
	[DateUpdated] datetime DEFAULT GETDATE() NOT NULL,
	CONSTRAINT [PK_Offices] PRIMARY KEY CLUSTERED ([OfficeId] ASC)
)

COMMIT TRANSACTION ORMS_CREATE_DB

