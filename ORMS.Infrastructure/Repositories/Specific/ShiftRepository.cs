﻿using ORMS.Core;
using ORMS.Infrastructure.Interfaces.Specific;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMS.Infrastructure.Repositories.Specific
{
    public class ShiftRepository : GenericRepository<Shift>, IShiftRepository
    {
        public ORMSContext Context { get { return _context as ORMSContext; } }
        public ShiftRepository(ORMSContext context) : base(context) { }
    }
}
