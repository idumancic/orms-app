﻿using ORMS.Core;
using ORMS.Infrastructure.Helpers;
using ORMS.Infrastructure.Interfaces.Specific;
using ORMS.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMS.Infrastructure.Repositories.Specific
{
    public class ItemRepository : GenericRepository<Item>, IItemRepository
    {
        public ORMSContext Context { get { return _context as ORMSContext; } }

        public ItemRepository(ORMSContext context) : base(context) { }

        public Page<Item> GetPage(PageProperties pageProp, string name, int? categoryId)
        {
            var itemsQuery = Context.Items
                .Include("Category")
                .Where(x => x.Active == (int)Status.ACTIVE);

            if (name != null || categoryId.HasValue)
            {
                itemsQuery = SearchQueryByParams(name, categoryId, itemsQuery);
            }

            itemsQuery = OrderQueryByProperty(pageProp.OrderBy, itemsQuery);

            var startAt = (pageProp.PageNumber - 1) * pageProp.PageSize;
            var items = itemsQuery
                .Skip(startAt)
                .Take(pageProp.PageSize)
                .AsEnumerable()
                .Select((x, i) => new IndexedEntity<Item>() { Index = i + startAt, Entity = x })
                .ToList();

            Page<Item> page = new Page<Item>()
            {
                TotalCount = itemsQuery.Count(),
                Data = items
            };

            return page;
        }

        private static IQueryable<Item> SearchQueryByParams(string name, int? categoryId, IQueryable<Item> itemsQuery)
        {
            bool searchParamName = name != null && name != string.Empty;
            bool searchParamCategory = categoryId.HasValue;

            if (searchParamName && !searchParamCategory)
            {
                itemsQuery = itemsQuery.Where(x => x.Name.ToLower().Contains(name));
            }
            else if (!searchParamName && searchParamCategory)
            {
                itemsQuery = itemsQuery.Where(x => x.CategoryId == categoryId.Value);
            }
            else if (searchParamName && searchParamCategory)
            {
                itemsQuery = itemsQuery.Where(x => x.CategoryId == categoryId.Value && x.Name.ToLower().Contains(name));
            }

            return itemsQuery;
        }

        private static IOrderedQueryable<Item> OrderQueryByProperty(string orderBy, IQueryable<Item> itemsQuery)
        {
            if (orderBy != null && orderBy != string.Empty)
            {
                if (QueryHelper.GetPropertType<Item>(orderBy) == typeof(Category))
                {
                    itemsQuery = itemsQuery.OrderBy(x => x.Category.Name);
                }
                else
                {
                    itemsQuery = itemsQuery.OrderBy(orderBy);
                }

            }
            else
            {
                itemsQuery = itemsQuery.OrderBy(x => x.ItemId);
            }

            return (IOrderedQueryable<Item>)itemsQuery;
        }

        public Page<Item> GetPageBySearch(PageProperties pageProp, string name, int? categoryId)
        {
            return GetPage(pageProp, name, categoryId);
        }

        public override int Count()
        {
            return Context.Items.Where(x => x.Active == (int)Status.ACTIVE).Count();
        }

    }
}
