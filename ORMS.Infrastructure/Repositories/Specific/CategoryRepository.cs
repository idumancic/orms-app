﻿using ORMS.Core;
using ORMS.Infrastructure.Interfaces.Specific;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using ORMS.Infrastructure.Helpers;

namespace ORMS.Infrastructure.Repositories.Specific
{
    public class CategoryRepository : GenericRepository<Category>, ICategoryRepository
    {
        public ORMSContext Context { get { return _context as ORMSContext; } }
        public CategoryRepository(ORMSContext context) : base(context) { }

        public override IEnumerable<Category> GetAll()
        {
            return base.GetAll().Where(x => x.Active == (int)Status.ACTIVE);
        }
    }
}
