﻿using ORMS.Core;
using ORMS.Infrastructure.Helpers;
using ORMS.Infrastructure.Interfaces.Specific;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMS.Infrastructure.Repositories.Specific
{
    public class OfficeRepository : GenericRepository<Office>, IOfficeRepository
    {
        public ORMSContext Context { get { return _context as ORMSContext; } }

        public OfficeRepository(ORMSContext context) : base(context) { }

        public bool CheckOIB(string oib)
        {
            return Context.Offices.Any(x => x.OIB.Equals(oib) && x.Active == (int)Status.ACTIVE);
        }

        public override IEnumerable<Office> GetAll()
        {
            return base.GetAll().Where(x => x.Active == (int)Status.ACTIVE);
        }
    }
}
