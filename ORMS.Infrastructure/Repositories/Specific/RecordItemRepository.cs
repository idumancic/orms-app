﻿using ORMS.Core;
using ORMS.Infrastructure.Interfaces.Specific;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ORMS.Infrastructure.Models;
using System.Linq.Expressions;
using System.Data.Entity;

namespace ORMS.Infrastructure.Repositories.Specific
{
    public class RecordItemRepository : GenericRepository<RecordItem>, IRecordItemRepository
    {
        public ORMSContext Context { get { return _context as ORMSContext; } }
        public RecordItemRepository(ORMSContext context) : base(context) { }

        public Page<RecordItem> GetPage(PageProperties pageProp, int recordId)
        {
            var recordItemsQuery = Context.RecordItems
                .Include("Item")
                .Where(x => x.RecordId.Equals(recordId));

            var startAt = (pageProp.PageNumber - 1) * pageProp.PageSize;
            var recordItems = recordItemsQuery
                .OrderBy(x => x.ItemId)
                .Skip(startAt)
                .Take(pageProp.PageSize)
                .AsEnumerable()
                .Select((x, i) => new IndexedEntity<RecordItem>() { Index = i + startAt, Entity = x });

            Page<RecordItem> page = new Page<RecordItem>()
            {
                TotalCount = recordItemsQuery.Count(),
                Data = recordItems
            };

            return page;
        }

        public int GetTotalItemsForRecord(int recordId)
        {
            return Context.RecordItems.Where(x => x.RecordId.Equals(recordId)).Count();
        }

        public IEnumerable<RecordItem> GetAll(int recordId)
        {
            return Context.RecordItems.Where(x => x.RecordId.Equals(recordId)).ToList();
        }

        public IEnumerable<object> GetYesterdaysStateOfTheBar(DateTime dateCreated)
        {
            DateTime yesterday = dateCreated.AddDays(-1);
            return Context.RecordItems
                .Include("Record")
                .Where(x => DbFunctions.TruncateTime(x.Record.DateCreated) == yesterday)
                .AsEnumerable()
                .Select((x, i) => new { RowIndex = i, YesterdaysStateOfTheBar = x.YesterdaysStateOfTheBar })
                .ToList();
        }

        public IEnumerable<IndexedEntity<RecordItem>> GetAllWithRowIndex(int recordId)
        {
            return Context.RecordItems.Include("Item")
                .Where(x => x.RecordId.Equals(recordId))
                .AsEnumerable()
                .Select((x, i) => new IndexedEntity<RecordItem>() { Index = i, Entity = x })
                .ToList();
        }

        public Dictionary<string, int> GetTop10Items()
        {
            Dictionary<string, int> dictionary = new Dictionary<string, int>();

            var itemList = (from rit in Context.RecordItems
                            group rit.SumOfSoldArticlesPrice by rit.Item.Name into g
                            orderby g.Key
                            select new
                            {
                                Name = g.Key,
                                Total = g.Sum(x => x)
                            }).OrderByDescending(x => x.Total).Take(10);

            foreach (var itemGroup in itemList)
            {
                string itemName = itemGroup.Name;
                int _totalEarnings = (int)itemGroup.Total;

                dictionary.Add(itemName, _totalEarnings);
            }

            return dictionary;
        }
    }
}
