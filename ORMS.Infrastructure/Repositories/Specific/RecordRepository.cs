﻿using ORMS.Core;
using ORMS.Infrastructure.Interfaces.Specific;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ORMS.Infrastructure.Models;
using ORMS.Infrastructure.Helpers;
using System.Linq.Expressions;
using System.Data.Entity;

namespace ORMS.Infrastructure.Repositories.Specific
{
    public class RecordRepository : GenericRepository<Record>, IRecordRepository
    {
        public ORMSContext Context { get { return _context as ORMSContext; } }

        public RecordRepository(ORMSContext context) : base(context) { }

        public Page<Record> GetPage(PageProperties pageProp, DateTime? fromDate, DateTime? toDate, int? workerId)
        {
            var recordQuery = Context.Records
                .Include("Office")
                .Where(x => x.Active == (int)Status.ACTIVE);

            bool dateRangeDefined = fromDate.HasValue && toDate.HasValue;
            if (dateRangeDefined || workerId.HasValue)
            {
                recordQuery = SearchQueryByParams(fromDate, toDate, workerId, recordQuery);
            }

            var startAt = (pageProp.PageNumber - 1) * pageProp.PageSize;
            var records = recordQuery
                .OrderByDescending(x => x.DateCreated)
                .Skip(startAt)
                .Take(pageProp.PageSize)
                .AsEnumerable()
                .Select((x, i) => new IndexedEntity<Record>() { Index = i + startAt, Entity = x })
                .ToList();

            Page<Record> page = new Page<Record>()
            {
                TotalCount = recordQuery.Count(),
                Data = records
            };

            return page;
        }

        private IQueryable<Record> SearchQueryByParams(DateTime? fromDate, DateTime? toDate, int? workerId, IQueryable<Record> recordQuery)
        {
            bool dateRangeDefined = fromDate.HasValue && toDate.HasValue;
            bool workerDefined = workerId.HasValue;

            if (dateRangeDefined && !workerDefined)
            {
                recordQuery = recordQuery.Where(x => x.DateCreated >= fromDate.Value && x.DateCreated <= toDate.Value);
            }
            else if (!dateRangeDefined && workerDefined)
            {
                var records = Context.WorkerLogs
                    .Join(Context.Records, wl => wl.RecordId, r => r.RecordId, (wl, r) => new { WorkerLog = wl, Record = r })
                    .Where(x => x.WorkerLog.UserId == workerId.Value)
                    .Select(x => x.Record);

                recordQuery = records;
            }
            else if (dateRangeDefined && workerDefined)
            {
                var records = Context.WorkerLogs
                    .Join(Context.Records, wl => wl.RecordId, r => r.RecordId, (wl, r) => new { WorkerLog = wl, Record = r })
                    .Where(x => x.WorkerLog.UserId == workerId.Value && x.Record.DateCreated >= fromDate.Value && x.Record.DateCreated <= toDate.Value)
                    .Select(x => x.Record);

                recordQuery = records;
            }

            return recordQuery;
        }

        public Page<Record> GetPageBySearch(PageProperties pageProp, DateTime? fromDate, DateTime? toDate, int? workerId)
        {
            return GetPage(pageProp, fromDate, toDate, workerId);
        }

        public bool CheckDateCreated(DateTime dateCreated)
        {
            return Context.Records.Any(x => DbFunctions.TruncateTime(x.DateCreated).Value.Equals(dateCreated) && x.Active == (int)Status.ACTIVE);
        }

        public override Record Get(object id)
        {
            return Context.Records
                    .Include("UserCreated")
                    .Include("UserUpdated")
                    .FirstOrDefault(x => x.RecordId == (int)id);
        }

        public Dictionary<int, int> GetYearlyEarnings()
        {
            Dictionary<int, int> dictionary = new Dictionary<int, int>();

            var yearsList = from r in Context.Records
                          group r.TotalEarnings by r.DateCreated.Year into g
                          orderby g.Key
                          select new
                          {
                              Year = g.Key,
                              Total = g
                          };

            foreach (var yearGroup in yearsList)
            {
                int year = yearGroup.Year;
                decimal _totalEarnings = yearGroup.Total.Sum(x => x);

                dictionary.Add(year, (int)_totalEarnings);
            }

            return dictionary;
        }

        public Dictionary<int, int> GetMonthlyEarnings()
        {
            Dictionary<int, int> dictionary = new Dictionary<int, int>();

            var monthList = from r in Context.Records
                            group r.TotalEarnings by r.DateCreated.Month into g
                            orderby g.Key
                            select new
                            {
                                Month = g.Key,
                                Total = g
                            };

            foreach (var monthGroup in monthList)
            {
                int month = monthGroup.Month;
                decimal _totalEarnings = monthGroup.Total.Sum(x => x);

                dictionary.Add(month, (int)_totalEarnings);
            }

            return dictionary;
        }
    }
}
