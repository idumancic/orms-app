﻿using ORMS.Core;
using ORMS.Infrastructure.Interfaces.Specific;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMS.Infrastructure.Repositories.Specific
{
    public class WorkerLogRepository : GenericRepository<WorkerLog>, IWorkerLogRepository
    {
        public ORMSContext Context { get { return _context as ORMSContext; } }
        public WorkerLogRepository(ORMSContext context) : base(context) { }

        public IEnumerable<WorkerLog> GetByRecord(int recordId)
        {
            return Context
                .WorkerLogs
                .Include("User")
                .Include("Shift")
                .Where(x => x.RecordId == recordId)
                .ToList();
        }

        public Dictionary<int, int> GetShiftEarnings()
        {
            Dictionary<int, int> dictionary = new Dictionary<int, int>();

            var shiftList = from wl in Context.WorkerLogs
                            group wl.ShiftEarnings by wl.ShiftId into g
                            orderby g.Key
                            select new
                            {
                                Shift = g.Key,
                                Total = g
                            };

            foreach (var shiftGroup in shiftList)
            {
                int shiftId = shiftGroup.Shift;
                decimal _totalEarnings = shiftGroup.Total.Sum(x => x);

                dictionary.Add(shiftId, (int)_totalEarnings);
            }

            return dictionary;
        }
    }
}
