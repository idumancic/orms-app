﻿using ORMS.Core;
using ORMS.Infrastructure.Helpers;
using ORMS.Infrastructure.Interfaces.Specific;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ORMS.Infrastructure.Repositories.Specific
{
    public class UserRepository : GenericRepository<User>, IUserRepository
    {
        public ORMSContext Context { get { return _context as ORMSContext; } }

        public UserRepository(ORMSContext context) : base(context) { }

        public User Get(string username, string password)
        {
            return Context.Users.FirstOrDefault(x => x.Username == username && x.Password == password); ;
        }

        public IEnumerable<User> GetWorkers()
        {
            return Context.Users
                .Include("Role")
                .Include("Office")
                .Where(x => (x.RoleCode.Equals(Role.WORKER) || x.RoleCode.Equals(Role.MANAGER)) && x.Active == (int)Status.ACTIVE)
                .ToList();
        }

        public User GetWorker(int workerId)
        {
            return Context.Users
                .FirstOrDefault(x => (x.RoleCode.Equals(Role.WORKER) || x.RoleCode.Equals(Role.MANAGER)) && x.Active == (int)Status.ACTIVE && x.UserId.Equals(workerId));
        }

        public bool CheckUsername(string username)
        {
            return Context.Users.Any(x => x.Username.Equals(username) && x.Active == (int)Status.ACTIVE);
        }

        public bool CheckOIB(string oib)
        {
            return Context.Users.Any(x => x.OIB.Equals(oib) && x.Active == (int)Status.ACTIVE);
        }

        public bool CheckJMBG(string jmbg)
        {
            return Context.Users.Any(x => x.JMBG.Equals(jmbg) && x.Active == (int)Status.ACTIVE);
        }
    }
}
