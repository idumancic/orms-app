namespace ORMS.Infrastructure
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using ORMS.Core;

    public partial class ORMSContext : DbContext
    {
        public ORMSContext()
            : base("name=ORMSContext")
        {
        }

        public virtual DbSet<Category> Categories { get; set; }
        public virtual DbSet<Item> Items { get; set; }
        public virtual DbSet<Office> Offices { get; set; }
        public virtual DbSet<RecordItem> RecordItems { get; set; }
        public virtual DbSet<Record> Records { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<Shift> Shifts { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<WorkerLog> WorkerLogs { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Category>()
                .HasMany(e => e.Items)
                .WithRequired(e => e.Category)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Item>()
                .Property(e => e.Price)
                .HasPrecision(19, 4);

            modelBuilder.Entity<Item>()
                .HasMany(e => e.RecordItems)
                .WithRequired(e => e.Item)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Office>()
                .Property(e => e.OIB)
                .IsUnicode(false);

            modelBuilder.Entity<Office>()
                .HasMany(e => e.Records)
                .WithRequired(e => e.Office)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Office>()
                .HasMany(e => e.Users)
                .WithOptional(e => e.Office)
                .HasForeignKey(e => e.OfficeId);

            modelBuilder.Entity<Record>()
                .Property(e => e.TotalEarnings)
                .HasPrecision(19, 4);

            modelBuilder.Entity<Record>()
                .HasMany(e => e.RecordItems)
                .WithRequired(e => e.Record)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Record>()
                .HasMany(e => e.WorkerLogs)
                .WithRequired(e => e.Record)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Role>()
                .Property(e => e.RoleCode)
                .IsUnicode(false);

            modelBuilder.Entity<Role>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Role>()
                .HasMany(e => e.Users)
                .WithRequired(e => e.Role)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Shift>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Shift>()
                .Property(e => e.Code)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Shift>()
                .HasMany(e => e.WorkerLogs)
                .WithRequired(e => e.Shift)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .Property(e => e.Username)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.Password)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.JMBG)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.OIB)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.PhoneNumber)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.Email)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.RoleCode)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Categories)
                .WithRequired(e => e.UserCreated)
                .HasForeignKey(e => e.UserCreatedId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Categories1)
                .WithRequired(e => e.UserUpdated)
                .HasForeignKey(e => e.UserUpdatedId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Items)
                .WithRequired(e => e.UserCreated)
                .HasForeignKey(e => e.UserCreatedId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Items1)
                .WithRequired(e => e.UserUpdated)
                .HasForeignKey(e => e.UserUpdatedId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Offices)
                .WithRequired(e => e.UserCreated)
                .HasForeignKey(e => e.UserCreatedId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Offices1)
                .WithRequired(e => e.UserUpdated)
                .HasForeignKey(e => e.UserUpdatedId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Records)
                .WithRequired(e => e.UserCreated)
                .HasForeignKey(e => e.UserCreatedId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Records1)
                .WithRequired(e => e.UserUpdated)
                .HasForeignKey(e => e.UserUpdatedId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Shifts)
                .WithRequired(e => e.UserCreated)
                .HasForeignKey(e => e.UserCreatedId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Shifts1)
                .WithRequired(e => e.UserUpdated)
                .HasForeignKey(e => e.UserUpdatedId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Users1)
                .WithRequired(e => e.UserCreated)
                .HasForeignKey(e => e.UserCreatedId);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Users11)
                .WithRequired(e => e.UserUpdated)
                .HasForeignKey(e => e.UserUpdatedId);

            modelBuilder.Entity<User>()
                .HasMany(e => e.WorkerLogs)
                .WithRequired(e => e.User)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<WorkerLog>()
                .Property(e => e.ShiftEarnings)
                .HasPrecision(19, 4);
        }
    }
}
