﻿using ORMS.Service.Office.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ORMS.Infrastructure.Interfaces;
using ORMS.Service.Office.Response;
using System.Diagnostics;
using ORMS.Service.Mappers;
using ORMS.Service.Base;
using ORMS.Service.Office.Request;

namespace ORMS.Service.Office
{
    public class OfficeService : IOfficeService
    {
        private IUnitOfWork unitOfWork;

        public IUnitOfWork UnitOfWork { get { return unitOfWork; } }

        public OfficeService(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public OfficeListResponse GetAll()
        {
            OfficeListResponse response = new OfficeListResponse();

            try
            {
                var offices = unitOfWork.Offices.GetAll();
                response.Success = true;
                response.Offices = offices.ToListOfficeViewModel();
            }
            catch (Exception ex)
            {
                Debug.Print(string.Format($"OFFICE GET ALL FAILED: {ex.ToString()}"));
                response.Success = false;
            }

            return response;
        }

        public GeneralResponse Add(OfficeUpdateRequest request)
        {
            GeneralResponse response = new GeneralResponse();

            try
            {
                Core.Office office = new Core.Office();
                office.MapCreate(request.Office);
                office.PrepareForInsert();

                unitOfWork.Offices.Add(office);
                unitOfWork.Commit();

                response.Data = office.OfficeId;
                response.Success = true;
            }
            catch (Exception ex)
            {
                Debug.Print(string.Format($"OFFICE ADD FAILED: {ex.ToString()}"));
                response.Success = false;
            }

            return response;
        }

        public GeneralResponse Update(OfficeUpdateRequest request)
        {
            GeneralResponse response = new GeneralResponse();

            try
            {
                Core.Office office = unitOfWork.Offices.Get(request.Office.Id);
                office.MapUpdate(request.Office);
                office.PrepareForUpdate();

                unitOfWork.Offices.Update(office);
                unitOfWork.Commit();

                response.Success = true;
            }
            catch (Exception ex)
            {
                Debug.Print(string.Format($"OFFICE UPDATE FAILED: {ex.ToString()}"));
                response.Success = false;
            }


            return response;
        }

        public OfficeValidationResponse Validate(OfficeUpdateRequest request)
        {
            OfficeValidationResponse response = new OfficeValidationResponse();

            try
            {
                response.OIBExists = unitOfWork.Offices.CheckOIB(request.Office.OIB);
                response.Success = true;
            }
            catch (Exception ex)
            {
                Debug.Print(string.Format($"OFFICE VALIDATE FAILED: {ex.ToString()}"));
                response.Success = false;
            }


            return response;
        }

        public OfficeResponse Get(OfficeRequest request)
        {
            OfficeResponse response = new OfficeResponse();

            try
            {
                var office = unitOfWork.Offices.Get(request.OfficeId);
                response.Success = true;
                response.Office = office.ToOfficeViewModel();
            }
            catch (Exception ex)
            {
                Debug.Print(string.Format($"OFFICE GET FAILED: {ex.ToString()}"));
                response.Success = false;
            }

            return response;
        }
    }
}
