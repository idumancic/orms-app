﻿using ORMS.Service.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMS.Service.Office.Response
{
    public class OfficeValidationResponse : BaseResponse
    {
        public bool OIBExists { get; set; }
    }
}
