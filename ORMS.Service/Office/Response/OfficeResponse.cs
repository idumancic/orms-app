﻿using ORMS.Service.Base;
using ORMS.Service.Office.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMS.Service.Office.Response
{
    public class OfficeResponse : BaseResponse
    {
        public OfficeViewModel Office { get; set; }
    }
}
