﻿using ORMS.Service.Base;
using ORMS.Service.Office.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMS.Service.Office.Response
{
    public class OfficeListResponse : BaseResponse
    {
        public IList<OfficeListViewModel> Offices { get; set; }
    }
}
