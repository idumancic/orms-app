﻿using ORMS.Service.Office.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMS.Service.Office.Request
{
    public class OfficeUpdateRequest
    {
        public OfficeViewModel Office { get; set; }
    }
}
