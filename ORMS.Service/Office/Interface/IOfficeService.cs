﻿using ORMS.Service.Base;
using ORMS.Service.Office.Request;
using ORMS.Service.Office.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMS.Service.Office.Interface
{
    public interface IOfficeService : IBaseService
    {
        OfficeListResponse GetAll();
        OfficeResponse Get(OfficeRequest request);
        GeneralResponse Add(OfficeUpdateRequest request);
        GeneralResponse Update(OfficeUpdateRequest request);
        OfficeValidationResponse Validate(OfficeUpdateRequest request);
    }
}
