﻿using ORMS.Service.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMS.Service.Office.ViewModel
{
    public class OfficeListViewModel : BaseModel
    {
        public string Name { get; set; }
    }
}
