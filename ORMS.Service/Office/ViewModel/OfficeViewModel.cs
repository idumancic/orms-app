﻿using ORMS.Service.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMS.Service.Office.ViewModel
{
    public class OfficeViewModel : BaseModel
    {
        public string Name { get; set; }
        public string OIB { get; set; }
        public string Address { get; set; }
        public string PostalCode { get; set; }
        public string Place { get; set; }
        public string City { get; set; }
    }
}
