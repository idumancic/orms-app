﻿using ORMS.Service.Base;
using ORMS.Service.WorkerLog.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMS.Service.Record.ViewModel
{
    public class RecordViewModel : BaseModel
    {
        public int OfficeId { get; set; }
        public WorkerLogViewModel FirstShift { get; set; }
        public WorkerLogViewModel SecondShift { get; set; }
        public decimal TotalEarnings { get; set; }
        public string Comment { get; set; }
        public DateTime DateCreated { get; set; }
        public string UserCreated { get; set; }
        public string UserUpdated { get; set; }

        public RecordViewModel()
        {
            this.FirstShift = new WorkerLogViewModel()
            {
                ShiftId = 1
            };
            this.SecondShift = new WorkerLogViewModel()
            {
                ShiftId = 2
            };
        }

    }
}
