﻿using ORMS.Service.Base;
using ORMS.Service.User.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMS.Service.Record.ViewModel
{
    public class RecordTableViewModel : BaseModelWithRowIndex
    {
        public string OfficeName { get; set; }
        public decimal TotalEarnings { get; set; }
        public DateTime DateCreated { get; set; }
        public IList<UserViewModel> Workers { get; set; }
    }
}
