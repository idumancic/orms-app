﻿using ORMS.Service.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMS.Service.Record.Response
{
    public class RecordValidationResponse : BaseResponse
    {
        public bool DateCreatedAlreadyExists { get; set; }
        public bool RecordItemListDoesNotMatch { get; set; }
    }
}
