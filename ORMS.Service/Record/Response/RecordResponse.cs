﻿using ORMS.Service.Base;
using ORMS.Service.Record.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMS.Service.Record.Response
{
    public class RecordResponse : BaseResponse
    {
        public RecordViewModel Record { get; set; }
    }
}
