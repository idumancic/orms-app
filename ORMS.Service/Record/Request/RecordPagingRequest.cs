﻿using ORMS.Service.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMS.Service.Record.Request
{
    public class RecordPagingRequest : BasePagingRequest
    {
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public int? WorkerId { get; set; }
    }
}
