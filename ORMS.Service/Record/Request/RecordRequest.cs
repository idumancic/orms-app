﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMS.Service.Record.Request
{
    public class RecordRequest
    {
        public int RecordId { get; set; }
        public DateTime? DateCreated { get; set; }
    }
}
