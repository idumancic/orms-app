﻿using ORMS.Service.Record.ViewModel;
using ORMS.Service.RecordItem.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMS.Service.Record.Request
{
    public class RecordUpdateRequest
    {
        public RecordViewModel Info { get; set; }
        public IList<RecordItemTableViewModel> RecordItems { get; set; }
        public int TotalRecordItems { get; set; }
    }
}
