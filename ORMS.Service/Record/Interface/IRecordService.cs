﻿using ORMS.Service.Base;
using ORMS.Service.Record.Request;
using ORMS.Service.Record.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMS.Service.Record.Interface
{
    public interface IRecordService : IBaseService
    {
        RecordResponse GetInfo(RecordRequest request);
        RecordPagingResponse GetPage(RecordPagingRequest request);
        RecordPagingResponse GetPageBySearch(RecordPagingRequest request);
        RecordResponse Add(RecordUpdateRequest request);
        GeneralResponse Update(RecordUpdateRequest request);
        RecordValidationResponse Validate(RecordUpdateRequest request);
    }
}
