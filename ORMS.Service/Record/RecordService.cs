﻿using ORMS.Service.Record.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ORMS.Infrastructure.Interfaces;
using ORMS.Service.Record.Request;
using ORMS.Service.Record.Response;
using ORMS.Infrastructure.Models;
using ORMS.Service.Mappers;
using System.Diagnostics;
using ORMS.Service.Base;
using ORMS.Core;

namespace ORMS.Service.Record
{
    public class RecordService : IRecordService
    {
        private IUnitOfWork unitOfWork;

        public IUnitOfWork UnitOfWork { get { return unitOfWork; } }

        public RecordService(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public RecordResponse GetInfo(RecordRequest request)
        {
            RecordResponse response = new RecordResponse();

            try
            {
                var record = unitOfWork.Records.Get(request.RecordId);
                var workerLogs = unitOfWork.WorkerLogs.GetByRecord(record.RecordId);

                response.Success = true;
                response.Record = record.ToRecordViewModel(workerLogs);
            }
            catch (Exception ex)
            {
                Debug.Print($"RECORD GET FAILED: {ex.ToString()}");
                response.Success = false;
            }

            return response;
        }

        public RecordPagingResponse GetPage(RecordPagingRequest request)
        {
            RecordPagingResponse response = new RecordPagingResponse();
            try
            {
                var pageProp = new PageProperties(request.PageNumber, request.PageSize, request.OrderBy);
                var page = unitOfWork.Records.GetPage(pageProp, request.FromDate, request.ToDate, request.WorkerId);

                response.Success = true;
                response.Records = page.Data.ToListRecordViewModel(unitOfWork.WorkerLogs);
                response.TotalCount = page.TotalCount;
                response.TotalPages = page.Data.Pages(page.TotalCount, request.PageSize);
            }
            catch (Exception ex)
            {
                Debug.Print(string.Format($"RECORD PAGE FAILED: {ex.ToString()}"));
                response.Success = false;
            }

            return response;
        }

        public RecordPagingResponse GetPageBySearch(RecordPagingRequest request)
        {
            return GetPage(request);
        }

        public GeneralResponse Update(RecordUpdateRequest request)
        {
            GeneralResponse response = new GeneralResponse();

            try
            {
                var recordInfo = request.Info;
                Core.Record recordUpdate = unitOfWork.Records.Get(recordInfo.Id);
                recordUpdate.OfficeId = recordInfo.OfficeId;
                recordUpdate.Comment = recordInfo.Comment;
                recordUpdate.TotalEarnings = recordInfo.TotalEarnings;
                recordUpdate.Active = recordInfo.Active.ToInt();

                recordUpdate.PrepareForUpdate();
                unitOfWork.Records.Update(recordUpdate);

                #region Updating of worker logs
                var workerLogs = unitOfWork.WorkerLogs.GetByRecord(recordUpdate.RecordId);
                foreach (var log in workerLogs)
                {
                    if (log.Shift.Code == Shift.FIRST)
                    {
                        log.Map(recordInfo.FirstShift);
                    }
                    else if (log.Shift.Code == Shift.SECOND)
                    {
                        log.Map(recordInfo.SecondShift);
                    }

                    unitOfWork.WorkerLogs.Update(log);
                }
                #endregion

                // TODO: Potential code refactoring, because we are getting all the recorded items even though we are not updating all
                /* User case: 
                 * We go to record editing and we update something on first page of record items but don't go on second page
                 * we assume immediately that we didn't make any changes on second page so we don't take that items in the acount
                 * thats the case where don't make full update of record items 
                */
                #region Updating of record items
                if (request.RecordItems != null)
                {
                    var recordItemsModelList = request.RecordItems.ToList();
                    var recordItems = unitOfWork.RecordItems.GetAll(recordUpdate.RecordId);
                    foreach (var recordItem in recordItems)
                    {
                        var recordItemModel = recordItemsModelList.FirstOrDefault(x => x.Id == recordItem.RecordItemId);
                        if (recordItemModel != null)
                        {
                            recordItem.Map(recordItemModel);
                            unitOfWork.RecordItems.Update(recordItem);
                        }
                        else
                        {
                            break;
                        }
                    }
                } 
                #endregion

                unitOfWork.Commit();
                response.Success = true;
            }
            catch (Exception ex)
            {
                Debug.Print($"UPDATE RECORD FAILED: {ex.ToString()}");
                response.Success = false;
            }

            return response;
        }

        public RecordResponse Add(RecordUpdateRequest request)
        {
            RecordResponse response = new RecordResponse();

            try
            {
                var recordInfo = request.Info;
                Core.Record newRecord = new Core.Record
                {
                    OfficeId = recordInfo.OfficeId,
                    Comment = recordInfo.Comment,
                    TotalEarnings = recordInfo.TotalEarnings
                };

                newRecord.PrepareForInsert();
                newRecord.DateCreated = recordInfo.DateCreated;
                unitOfWork.Records.Add(newRecord);

                #region Adding of worker logs
                var workerLogs = new List<Core.WorkerLog>();
                Core.WorkerLog firstShiftLog = new Core.WorkerLog();
                recordInfo.FirstShift.RecordId = newRecord.RecordId;
                firstShiftLog.Map(recordInfo.FirstShift);
                workerLogs.Add(firstShiftLog);

                Core.WorkerLog secondShiftLog = new Core.WorkerLog();
                recordInfo.SecondShift.RecordId = newRecord.RecordId;
                secondShiftLog.Map(recordInfo.SecondShift);
                workerLogs.Add(secondShiftLog);

                unitOfWork.WorkerLogs.AddRange(workerLogs);

                #endregion

                #region Adding of record items
                var recordItemsToAdd = new List<Core.RecordItem>();
                var recordItemsModelList = request.RecordItems.ToList();
                foreach (var recordItem in recordItemsModelList)
                {
                    Core.RecordItem _recordItem = new Core.RecordItem();
                    recordItem.RecordId = newRecord.RecordId;
                    _recordItem.Map(recordItem);
                    recordItemsToAdd.Add(_recordItem);
                }
                unitOfWork.RecordItems.AddRange(recordItemsToAdd); 
                #endregion

                unitOfWork.Commit();
                response.Success = true;

                recordInfo.Id = newRecord.RecordId;
                response.Record = recordInfo;
            }
            catch (Exception ex)
            {
                Debug.Print($"ADD RECORD FAILED: {ex.ToString()}");
                response.Success = false;
            }

            return response;
        }

        public RecordValidationResponse Validate(RecordUpdateRequest request)
        {
            RecordValidationResponse response = new RecordValidationResponse();

            try
            {
                var recordInfo = request.Info;
                response.DateCreatedAlreadyExists = unitOfWork.Records.CheckDateCreated(recordInfo.DateCreated);
                response.RecordItemListDoesNotMatch = !request.RecordItems.Count.Equals(request.TotalRecordItems);
                response.Success = true;
            }
            catch (Exception ex)
            {
                Debug.Print($"RECORD VALIDATION FAILED: {ex.ToString()}");
                response.Success = false;
            }

            return response;
        }
    }
}
