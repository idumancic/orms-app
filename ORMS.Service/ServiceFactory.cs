﻿using ORMS.Core;
using ORMS.Infrastructure;
using ORMS.Infrastructure.Interfaces;
using ORMS.Service.Authentication;
using ORMS.Service.Authentication.Interface;
using ORMS.Service.Category;
using ORMS.Service.Category.Interface;
using ORMS.Service.Item;
using ORMS.Service.Item.Interface;
using ORMS.Service.Office;
using ORMS.Service.Office.Interface;
using ORMS.Service.Record;
using ORMS.Service.Record.Interface;
using ORMS.Service.RecordItem;
using ORMS.Service.RecordItem.Interface;
using ORMS.Service.Role;
using ORMS.Service.Role.Interface;
using ORMS.Service.Statistic;
using ORMS.Service.Statistic.Interface;
using ORMS.Service.User;
using ORMS.Service.User.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unity;

namespace ORMS.Service
{
    public static class ServiceFactory
    {
        public static IAuthenticationService CreateAuthenticationService()
        {
            return Initialize<AuthenticationService>();
        }

        public static ICategoryService CreateCategoryService()
        {
            return Initialize<CategoryService>();
        }

        public static IItemService CreateItemService()
        {
            return Initialize<ItemService>();
        }

        public static IOfficeService CreateOfficeService()
        {
            return Initialize<OfficeService>();
        }

        public static IRecordService CreateRecordService()
        {
            return Initialize<RecordService>();
        }

        public static IRecordItemService CreateRecordItemService()
        {
            return Initialize<RecordItemService>();
        }

        public static IRoleService CreateRoleService()
        {
            return Initialize<RoleService>();
        }

        public static IStatisticsService CreateStatisticsService()
        {
            return Initialize<StatisticsService>();
        }

        public static IUserService CreateUserService()
        {
            return Initialize<UserService>();
        }

        private static T Initialize<T>()
        {
            var container = new UnityContainer();
            container.RegisterType<IUnitOfWork, UnitOfWork>();
            container.RegisterType<ORMSContext>();
            return container.Resolve<T>();
        }
    }

}
