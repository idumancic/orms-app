﻿using ORMS.Service.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMS.Service.RecordItem.Request
{
    public class RecordItemPagingRequest : BasePagingRequest
    {
        public int RecordId { get; set; }
    }
}
