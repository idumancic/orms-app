﻿using ORMS.Service.RecordItem.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMS.Service.RecordItem.Request
{
    public class RecordItemsCSVRequest
    {
        public DateTime DateCreated { get; set; }
        public int RecordId { get; set; }
    }
}
