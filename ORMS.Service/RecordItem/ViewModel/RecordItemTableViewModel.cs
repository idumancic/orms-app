﻿using ORMS.Service.Base;
using ORMS.Service.Item.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMS.Service.RecordItem.ViewModel
{
    public class RecordItemTableViewModel : BaseModelWithRowIndex
    {
        public int RecordId { get; set; }
        public ItemViewModel Item { get; set; }
        public int YesterdaysStateOfTheBar { get; set; }
        public int Received { get; set; }
        public int InTotal { get; set; }
        public int TodaysStateOfTheBar { get; set; }
        public int SoldOut { get; set; }
        public int SumOfSoldArticlesPrice { get; set; }
    }
}
