﻿using ORMS.Service.Base;
using ORMS.Service.Record.Request;
using ORMS.Service.RecordItem.Request;
using ORMS.Service.RecordItem.Response;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMS.Service.RecordItem.Interface
{
    public interface IRecordItemService : IBaseService
    {
        RecordItemPagingResponse GetPage(RecordItemPagingRequest request);
        GeneralResponse GetTotalRecordItemsCount(RecordRequest request);
        GeneralResponse GetYesterdaysState(RecordRequest request);
        GeneralResponse GetAllItemsForRecord(RecordRequest request);
        FileResponse GetCSVData(RecordItemsCSVRequest request);
    }
}
