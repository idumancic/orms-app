﻿using ORMS.Service.Base;
using ORMS.Service.RecordItem.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMS.Service.RecordItem.Response
{
    public class RecordItemPagingResponse : BasePagingResponse
    {
        public IList<RecordItemTableViewModel> RecordItems { get; set; }
    }
}
