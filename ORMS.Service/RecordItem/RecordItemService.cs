﻿using ORMS.Service.RecordItem.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ORMS.Infrastructure.Interfaces;
using ORMS.Service.RecordItem.Request;
using ORMS.Service.RecordItem.Response;
using ORMS.Infrastructure.Models;
using ORMS.Service.Mappers;
using System.Diagnostics;
using ORMS.Service.Base;
using ORMS.Service.Record.Request;
using System.IO;

namespace ORMS.Service.RecordItem
{
    public class RecordItemService : IRecordItemService
    {
        private IUnitOfWork unitOfWork;
        public IUnitOfWork UnitOfWork { get { return unitOfWork; } }
        public RecordItemService(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public RecordItemPagingResponse GetPage(RecordItemPagingRequest request)
        {
            RecordItemPagingResponse response = new RecordItemPagingResponse();

            try
            {
                var pageProp = new PageProperties(request.PageNumber, request.PageSize, request.OrderBy);
                var page = unitOfWork.RecordItems.GetPage(pageProp, request.RecordId);

                response.Success = true;
                response.RecordItems = page.Data.ToListRecordItemViewModel();
                response.TotalCount = page.TotalCount;
                response.TotalPages = page.Data.Pages(page.TotalCount, request.PageSize);
            }
            catch (Exception ex)
            {
                Debug.Print($"RECORD ITEMS GET FAILED: {ex.ToString()}");
                response.Success = false;
            }

            return response;
        }

        public GeneralResponse GetTotalRecordItemsCount(RecordRequest request)
        {
            GeneralResponse response = new GeneralResponse();

            try
            {
                response.Data = unitOfWork.RecordItems.GetTotalItemsForRecord(request.RecordId);
                response.Success = true;
            }
            catch (Exception ex)
            {
                Debug.Print($"GET TOTAL RECORD ITEMS COUNT FAILED: {ex.ToString()}");
                response.Success = false;
            }

            return response;
        }

        public GeneralResponse GetYesterdaysState(RecordRequest request)
        {
            GeneralResponse response = new GeneralResponse();

            try
            {
                response.Data = unitOfWork.RecordItems.GetYesterdaysStateOfTheBar(request.DateCreated.Value);
                response.Success = true;
            }
            catch (Exception ex)
            {
                Debug.Print($"GET YESTERDAYS STATE FAILED: {ex.ToString()}");
                response.Success = false;
            }

            return response;
        }

        public GeneralResponse GetAllItemsForRecord(RecordRequest request)
        {
            GeneralResponse response = new GeneralResponse();

            try
            {
                var recordItems = unitOfWork.RecordItems.GetAllWithRowIndex(request.RecordId);
                response.Data = recordItems.ToListRecordItemViewModel();
                response.Success = true;
            }
            catch (Exception ex)
            {
                Debug.Print($"CHECK DATE CREATED FAILED: {ex.ToString()}");
                response.Success = false;
            }

            return response;
        }

        public FileResponse GetCSVData(RecordItemsCSVRequest request)
        {
            FileResponse response = new FileResponse();

            try
            {
                StringBuilder sb = new StringBuilder();

                var recordItems = unitOfWork
                    .RecordItems
                    .GetAllWithRowIndex(request.RecordId)
                    .ToListRecordItemViewModel();

                string header = string.Format("{0},{1},{2},{3},{4},{5},{6},{7}",
                    Core.RecordItem.ITEM_NAME,
                    Core.RecordItem.YESTERDAYS_STATE_OF_THE_BAR,
                    Core.RecordItem.RECEIVED,
                    Core.RecordItem.IN_TOTAL,
                    Core.RecordItem.TODAYS_STATE_OF_THE_BAR,
                    Core.RecordItem.SOLD_OUT,
                    Core.RecordItem.ITEM_PRICE,
                    Core.RecordItem.SUM_OF_SOLD_ARTICLES_PRICE);

                sb.AppendLine(header);

                foreach (var recordItem in recordItems)
                {
                    string recordItemLine = string.Format("{0},{1},{2},{3},{4},{5},{6},{7}", recordItem.Item.Name,
                        recordItem.YesterdaysStateOfTheBar,
                        recordItem.Received,
                        recordItem.InTotal,
                        recordItem.TodaysStateOfTheBar,
                        recordItem.SoldOut,
                        recordItem.Item.Price,
                        recordItem.SumOfSoldArticlesPrice);

                    sb.AppendLine(recordItemLine);
                }

                response.Stream = Encoding.UTF8.GetBytes(sb.ToString());
                response.FileName = string.Format($"EL_{request.DateCreated.ToFileDateString()}.csv");
                response.Success = true;
            }
            catch (Exception ex)
            {
                Debug.Print($"GET CSV DATA FAILED: {ex.ToString()}");
                response.Success = false;
            }

            return response;
        }
    }
}
