﻿using ORMS.Infrastructure.Models;
using ORMS.Service.Item.ViewModel;
using ORMS.Service.RecordItem.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMS.Service.Mappers
{
    public static class RecordItemMapperExtensionMethods
    {
        public static IList<RecordItemTableViewModel> ToListRecordItemViewModel(this IEnumerable<IndexedEntity<Core.RecordItem>> recordItems)
        {
            var list = new List<RecordItemTableViewModel>();

            foreach (var dto in recordItems)
            {
                var recordItem = dto.Entity;
                var model = new RecordItemTableViewModel
                {
                    Id = recordItem.RecordItemId,
                    RowIndex = dto.Index,
                    RecordId = recordItem.RecordId,
                    Item = new ItemViewModel()
                    {
                        Id = recordItem.Item.ItemId,
                        Name = recordItem.Item.Name,
                        Active = recordItem.Item.Active.ToBool(),
                        Price = recordItem.Item.Price
                    },
                    YesterdaysStateOfTheBar = recordItem.YesterdaysStateOfTheBar,
                    InTotal = recordItem.InTotal ?? 0,
                    Received = recordItem.Received ?? 0,
                    SoldOut = recordItem.SoldOut ?? 0,
                    SumOfSoldArticlesPrice = recordItem.SumOfSoldArticlesPrice ?? 0,
                    TodaysStateOfTheBar = recordItem.TodaysStateOfTheBar ?? 0,
                    Active = true
                };

                list.Add(model);
            }

            return list;
        }

        public static void Map(this Core.RecordItem recordItem, RecordItemTableViewModel model)
        {
            recordItem.RecordId = model.RecordId;
            recordItem.ItemId = model.Item.Id;
            recordItem.YesterdaysStateOfTheBar = model.YesterdaysStateOfTheBar;
            recordItem.Received = model.Received;
            recordItem.InTotal = model.InTotal;
            recordItem.TodaysStateOfTheBar = model.TodaysStateOfTheBar;
            recordItem.SoldOut = model.SoldOut;
            recordItem.SumOfSoldArticlesPrice = model.SumOfSoldArticlesPrice;
        }

        public static void Map(this RecordItemTableViewModel model, Core.RecordItem recordItem)
        {
            model.RecordId = recordItem.RecordId;
            model.Item.Id = recordItem.ItemId;
            model.YesterdaysStateOfTheBar = recordItem.YesterdaysStateOfTheBar;
            model.Received = recordItem.Received ?? 0;
            model.InTotal = recordItem.InTotal ?? 0;
            model.TodaysStateOfTheBar = recordItem.TodaysStateOfTheBar ?? 0;
            model.SoldOut = recordItem.SoldOut ?? 0;
            model.SumOfSoldArticlesPrice = recordItem.SumOfSoldArticlesPrice ?? 0;
        }
    }
}
