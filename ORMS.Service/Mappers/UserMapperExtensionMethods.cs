﻿using ORMS.Core;
using ORMS.Infrastructure.Helpers;
using ORMS.Service.User.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMS.Service.Mappers
{
    public static class UserMapperExtensionMethods
    {
        public static string GetFullName(this Core.User user)
        {
            return string.Format($"{user.FirstName} {user.LastName}");
        }

        public static UserViewModel ToUserViewModel(this Core.User user)
        {
            return new UserViewModel()
            {
                Id = user.UserId,
                Username = user.Username,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Role = user.RoleCode,
                Active = user.Active.ToBool()
            };
        }

        public static List<UserWorkerListViewModel> ToListUserWorkerViewModel(this IEnumerable<Core.User> workers)
        {
            var list = new List<UserWorkerListViewModel>();

            foreach (var worker in workers)
            {
                var model = new UserWorkerListViewModel()
                {
                    Id = worker.UserId,
                    FirstName = worker.FirstName,
                    LastName = worker.LastName,
                    RoleName = worker.Role.Name,
                    OfficeName = worker.Office.Name,
                    Active = worker.Active.ToBool()
                };

                list.Add(model);
            }

            return list;
        }

        public static UserWorkerViewModel ToUserWorkerViewModel(this Core.User worker)
        {
            var _worker = new UserWorkerViewModel()
            {
                Id = worker.UserId,
                Username = worker.Username,
                FirstName = worker.FirstName,
                LastName = worker.LastName,
                Email = worker.Email,
                Address = worker.Address,
                City = worker.City,
                DateOfBirth = worker.DateOfBirth,
                JMBG = worker.JMBG,
                OIB = worker.OIB,
                PhoneNumber = worker.PhoneNumber,
                PostalCode = worker.PostalCode,
                SanitaryIdCreatedDate = worker.SanitaryIdCreatedDate,
                SanitaryIdExpirationDate = worker.SanitaryIdExpirationDate,
                SanitaryIdVerified = worker.SanitaryIdVerified,
                RoleCode = worker.RoleCode,
                OfficeId = worker.OfficeId,
                Active = worker.Active.ToBool()
            };

            return _worker;
        }

        public static void MapUpdate(this Core.User user, UserWorkerViewModel worker)
        {
            if (worker.Password != null)
            {
                user.Password = worker.Password;
            }

            if (worker.Active != false)
            {
                user.Username = worker.Username;
                user.FirstName = worker.FirstName;
                user.LastName = worker.LastName;
                user.Email = worker.Email;
                user.Address = worker.Address;
                user.City = worker.City;
                user.DateOfBirth = worker.DateOfBirth;
                user.JMBG = worker.JMBG;
                user.OIB = worker.OIB;
                user.PhoneNumber = worker.PhoneNumber;
                user.PostalCode = worker.PostalCode;
                user.SanitaryIdCreatedDate = worker.SanitaryIdCreatedDate;
                user.SanitaryIdExpirationDate = worker.SanitaryIdExpirationDate;
                user.SanitaryIdVerified = worker.SanitaryIdVerified;
                user.RoleCode = worker.RoleCode;
                user.OfficeId = worker.OfficeId;
            }

            user.Active = worker.Active.ToInt();
        }

        public static void MapCreate(this Core.User user, UserWorkerViewModel worker)
        {
            user.Password = worker.Password;
            user.Username = worker.Username;
            user.FirstName = worker.FirstName;
            user.LastName = worker.LastName;
            user.Email = worker.Email;
            user.Address = worker.Address;
            user.City = worker.City;
            user.DateOfBirth = worker.DateOfBirth;
            user.JMBG = worker.JMBG;
            user.OIB = worker.OIB;
            user.PhoneNumber = worker.PhoneNumber;
            user.PostalCode = worker.PostalCode;
            user.SanitaryIdCreatedDate = worker.SanitaryIdCreatedDate;
            user.SanitaryIdExpirationDate = worker.SanitaryIdExpirationDate;
            user.SanitaryIdVerified = worker.SanitaryIdVerified;
            user.RoleCode = worker.RoleCode;
            user.OfficeId = worker.OfficeId;
        }
    }
}
