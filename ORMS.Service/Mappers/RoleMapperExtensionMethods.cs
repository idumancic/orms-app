﻿using ORMS.Service.Role.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMS.Service.Mappers
{
    public static class RoleMapperExtensionMethods
    {
        public static IList<RoleViewModel> ToListRoleViewModel(this IEnumerable<Core.Role> roles)
        {
            var list = new List<RoleViewModel>();

            foreach (var role in roles)
            {
                var model = new RoleViewModel()
                {
                    RoleCode = role.RoleCode,
                    Name = role.Name,
                    Active = role.Active.ToBool()
                };

                list.Add(model);
            }

            return list;
        }
    }
}
