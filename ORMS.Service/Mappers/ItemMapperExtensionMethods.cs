﻿using ORMS.Infrastructure.Models;
using ORMS.Service.Category.ViewModel;
using ORMS.Service.Item.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMS.Service.Mappers
{
    public static class ItemMapperExtensionMethods
    {
        public static IList<ItemTableViewModel> ToListItemViewModel(this IEnumerable<IndexedEntity<Core.Item>> items)
        {
            var list = new List<ItemTableViewModel>();

            foreach (var dto in items)
            {
                var item = dto.Entity;
                var model = new ItemTableViewModel
                {
                    RowIndex = dto.Index,
                    Id = item.ItemId,
                    Name = item.Name,
                    Price = item.Price,
                    Category = new CategoryViewModel()
                    {
                        Id = item.CategoryId,
                        Name = item.Category.Name,
                        Active = item.Category.Active.ToBool()
                    },
                    Active = item.Active.ToBool()
                };

                list.Add(model);
            }

            return list;
        }

        public static void Map(this Core.Item item, ItemViewModel model)
        {
            item.ItemId = model.Id;
            item.Price = model.Price;
            item.Name = model.Name;
            item.Active = model.Active.ToInt();
        }
    }
}
