﻿using ORMS.Service.Statistic.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMS.Service.Mappers
{
    public static class StatisticMapperExtensionMethods
    {
        public static StatisticsViewModel ToStatisticsViewModel(this Dictionary<int, int> earnings)
        {
            StatisticsViewModel model = new StatisticsViewModel();
            model.Series.Add("Ukupna zarada");

            foreach (var data in earnings)
            {
                model.Labels.Add(data.Key.ToString());
                model.Earnings.Add(data.Value);
            }

            return model;
        }

        public static StatisticsViewModel ToStatisticsViewModel(this Dictionary<string, int> earnings)
        {
            StatisticsViewModel model = new StatisticsViewModel();
            model.Series.Add("Ukupna zarada");

            foreach (var data in earnings)
            {
                model.Labels.Add(data.Key);
                model.Earnings.Add(data.Value);
            }

            return model;
        }
    }
}
