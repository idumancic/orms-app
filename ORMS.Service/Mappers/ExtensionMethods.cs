﻿using ORMS.Core.Base;
using ORMS.Infrastructure.Helpers;
using ORMS.Service.Authentication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMS.Service.Mappers
{
    public static class ExtensionMethods
    {
        public static bool ToBool(this int active)
        {
            return active == 1 ? true : false;
        }

        public static int ToInt(this bool active)
        {
            return active ? 1 : 0;
        }

        public static void PrepareForInsert(this BaseEntity entity)
        {
            var authUser = AuthenticationService.GetUser();
            entity.Active = (int)Status.ACTIVE;
            entity.DateCreated = DateTime.Now;
            entity.DateUpdated = DateTime.Now;
            entity.UserCreatedId = authUser.UserCreatedId;
            entity.UserUpdatedId = authUser.UserUpdatedId;
        }

        public static void PrepareForUpdate(this BaseEntity entity)
        {
            var authUser = AuthenticationService.GetUser();
            entity.UserUpdatedId = authUser.UserUpdatedId;
            entity.DateUpdated = DateTime.Now;
        }

        public static string ToFileDateString(this DateTime dateTime)
        {
            var date = dateTime.Date;
            string day = date.Day.ToString();
            string month = date.Month.ToString();
            string year = date.Year.ToString();

            if(date.Month < 10)
            {
                month = "0" + date.Month;
            }

            if (date.Day < 10)
            {
                day = "0" + date.Day;
            }

            return string.Format($"{day}{month}{year}");
        }
    }
}
