﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMS.Service.Mappers
{
    public static class IEnumerableExtensionMethods
    {
        public static int Pages<T>(this IEnumerable<T> list, int? totalCount, int? pageSize)
        {
            if (!pageSize.HasValue || !totalCount.HasValue)
                return 0;

            var totalPages = Math.Ceiling((double)totalCount.Value / pageSize.Value);
            return (int)totalPages;
        }
    }
}
