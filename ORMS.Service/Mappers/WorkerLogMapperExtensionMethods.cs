﻿using ORMS.Service.WorkerLog.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMS.Service.Mappers
{
    public static class WorkerLogMapperExtensionMethods
    {
        public static void Map(this Core.WorkerLog workerLog, WorkerLogViewModel model)
        {
            workerLog.RecordId = model.RecordId;
            workerLog.ShiftId = model.ShiftId;
            workerLog.ShiftEarnings = model.ShiftEarnings;
            workerLog.UserId = model.WorkerId;
            workerLog.DateCreated = model.Id > 0 ? workerLog.DateCreated : DateTime.Now; 
            workerLog.DateUpdated = DateTime.Now;
        }


        public static void Map(this WorkerLogViewModel model, Core.WorkerLog workerLog)
        {
            model.Id = workerLog.WorkerLogId;
            model.ShiftId = workerLog.ShiftId;
            model.ShiftEarnings = workerLog.ShiftEarnings;
            model.WorkerId = workerLog.UserId;
            model.RecordId = workerLog.RecordId;
        }
    }
}
