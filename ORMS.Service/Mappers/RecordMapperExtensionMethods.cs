﻿using ORMS.Core;
using ORMS.Infrastructure.Interfaces.Specific;
using ORMS.Infrastructure.Models;
using ORMS.Service.Item.ViewModel;
using ORMS.Service.Record.ViewModel;
using ORMS.Service.RecordItem.ViewModel;
using ORMS.Service.WorkerLog.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMS.Service.Mappers
{
    public static class RecordMapperExtensionMethods
    {
        public static IList<RecordTableViewModel> ToListRecordViewModel(this IEnumerable<IndexedEntity<Core.Record>> records, IWorkerLogRepository workerLogsRepo)
        {
            var list = new List<RecordTableViewModel>();

            foreach (var dto in records)
            {
                var record = dto.Entity;
                var model = new RecordTableViewModel
                {
                    RowIndex = dto.Index,
                    Id = record.RecordId,
                    OfficeName = record.Office.Name,
                    TotalEarnings = record.TotalEarnings,
                    DateCreated = record.DateCreated,
                    Active = record.Active.ToBool()
                };

                var workerLogs = workerLogsRepo.GetByRecord(record.RecordId);
                model.Workers = workerLogs.Select(x => x.User.ToUserViewModel()).ToList();

                list.Add(model);
            }

            return list;
        }

        public static RecordViewModel ToRecordViewModel(this Core.Record record, IEnumerable<Core.WorkerLog> workerLogs)
        {
            RecordViewModel model = new RecordViewModel()
            {
                Id = record.RecordId,
                OfficeId = record.OfficeId,
                Comment = record.Comment,
                TotalEarnings = record.TotalEarnings,
                UserCreated = record.UserCreated.GetFullName(),
                UserUpdated = record.UserUpdated.GetFullName(),
                DateCreated = record.DateCreated,
                Active = record.Active.ToBool()
            };

            foreach (var workerLog in workerLogs)
            {
                if (workerLog.Shift.Code == Shift.FIRST)
                {
                    model.FirstShift = new WorkerLogViewModel();
                    model.FirstShift.Map(workerLog);
                }
                else if (workerLog.Shift.Code == Shift.SECOND)
                {
                    model.SecondShift = new WorkerLogViewModel();
                    model.SecondShift.Map(workerLog);
                }
            }

            return model;
        }
    }
}
