﻿using ORMS.Service.Office.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMS.Service.Mappers
{
    public static class OfficeMapperExtensionMethods
    {
        public static IList<OfficeListViewModel> ToListOfficeViewModel(this IEnumerable<Core.Office> offices)
        {
            var list = new List<OfficeListViewModel>();

            foreach (var office in offices)
            {
                var model = new OfficeListViewModel()
                {
                    Id = office.OfficeId,
                    Name = office.Name,
                    Active = office.Active.ToBool()
                };

                list.Add(model);
            }

            return list;
        }

        public static OfficeViewModel ToOfficeViewModel(this Core.Office office)
        {
            var model = new OfficeViewModel()
            {
                Id = office.OfficeId,
                Name = office.Name,
                OIB = office.OIB,
                Address = office.Address,
                City = office.City,
                Place = office.Place,
                PostalCode = office.PostalCode,
                Active = office.Active.ToBool()
            };

            return model;
        }

        public static void MapCreate(this Core.Office office, OfficeViewModel model)
        {
            office.Name = model.Name;
            office.OIB = model.OIB;
            office.Address = model.Address;
            office.PostalCode = model.PostalCode;
            office.City = model.City;
            office.Place = model.Place;
            office.Active = model.Active.ToInt();
        }

        public static void MapUpdate(this Core.Office office, OfficeViewModel model)
        {
            if(model.Active != false)
            {
                office.Name = model.Name;
                office.OIB = model.OIB;
                office.Address = model.Address;
                office.PostalCode = model.PostalCode;
                office.City = model.City;
                office.Place = model.Place;
            }

            office.Active = model.Active.ToInt();
        }


    }
}
