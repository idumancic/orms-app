﻿using ORMS.Service.Category.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMS.Service.Mappers
{
    public static class CategoryMapperExtensionMethods
    {
        public static IList<CategoryViewModel> ToListCategoryViewModel(this IEnumerable<Core.Category> categories)
        {
            var list = new List<CategoryViewModel>();

            foreach (var category in categories)
            {
                var model = new CategoryViewModel()
                {
                    Id = category.CategoryId,
                    Name = category.Name,
                    Active = category.Active.ToBool()
                };

                list.Add(model);
            }

            return list;
        }
    }
}
