﻿using ORMS.Service.Base;
using ORMS.Service.Category.Request;
using ORMS.Service.Category.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMS.Service.Category.Interface
{
    public interface ICategoryService : IBaseService
    {
        CategoryListResponse GetAll();
        GeneralResponse Add(CategoryUpdateRequest request);
        GeneralResponse Update(CategoryUpdateRequest request);
    }
}
