﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMS.Service.Category.Request
{
    public class CategoryUpdateRequest
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public bool Active { get; set; }
    }
}
