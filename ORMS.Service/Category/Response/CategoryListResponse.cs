﻿using ORMS.Service.Base;
using ORMS.Service.Category.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMS.Service.Category.Response
{
    public class CategoryListResponse : BaseResponse
    {
        public IList<CategoryViewModel> Categories { get; set; }
    }
}
