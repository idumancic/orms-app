﻿using ORMS.Service.Category.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ORMS.Infrastructure.Interfaces;
using ORMS.Service.Category.Response;
using ORMS.Service.Mappers;
using ORMS.Service.Base;
using ORMS.Service.Category.Request;
using System.Diagnostics;

namespace ORMS.Service.Category
{
    public class CategoryService : ICategoryService
    {
        private IUnitOfWork unitOfWork;

        public IUnitOfWork UnitOfWork { get { return unitOfWork; } }

        public CategoryService(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public CategoryListResponse GetAll()
        {
            CategoryListResponse response = new CategoryListResponse();

            try
            {
                var categories = unitOfWork.Categories.GetAll();
                response.Success = true;
                response.Categories = categories.ToListCategoryViewModel();
            }
            catch (Exception ex)
            {
                Debug.Print(string.Format($"CATEGORY GET ALL FAILED: {ex.ToString()}"));
                response.Success = false;
            }

            return response;
        }

        public GeneralResponse Add(CategoryUpdateRequest request)
        {
            GeneralResponse response = new GeneralResponse();
            Core.Category category = new Core.Category()
            {
                Name = request.Name,
                DateCreated = DateTime.Now,
                DateUpdated = DateTime.Now
            };

            try
            {
                category.PrepareForInsert();
                unitOfWork.Categories.Add(category);
                unitOfWork.Commit();
                response.Success = true;
            }
            catch (Exception ex)
            {
                Debug.Print(string.Format($"ITEM ADD FAILED: {ex.ToString()}"));
                response.Success = false;
            }

            return response;
        }

        public GeneralResponse Update(CategoryUpdateRequest request)
        {
            GeneralResponse response = new GeneralResponse();

            try
            {
                var category = unitOfWork.Categories.Get(request.Id);
                category.Name = request.Name;
                category.Active = request.Active.ToInt();

                category.PrepareForUpdate();
                unitOfWork.Categories.Update(category);
                unitOfWork.Commit();
                response.Success = true;
            }
            catch (Exception ex)
            {
                Debug.Print(string.Format($"CATEGORY UPDATE FAILED: {ex.ToString()}"));
                response.Success = false;
            }

            return response;
        }
    }
}
