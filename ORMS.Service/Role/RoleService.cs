﻿using ORMS.Service.Role.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ORMS.Infrastructure.Interfaces;
using ORMS.Service.Role.Response;
using System.Diagnostics;
using ORMS.Service.Mappers;

namespace ORMS.Service.Role
{
    public class RoleService : IRoleService
    {
        private IUnitOfWork unitOfWork;
        public IUnitOfWork UnitOfWork { get { return unitOfWork; } }
        public RoleService(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public RoleResponse GetRoles()
        {
            RoleResponse response = new RoleResponse();

            try
            {
                var roles = unitOfWork.Roles.GetAll()
                    .Where(x => x.RoleCode != Core.Role.ADMIN && x.RoleCode != Core.Role.OWNER);
                response.Roles = roles.ToListRoleViewModel();
                response.Success = true;
            }
            catch (Exception ex)
            {
                Debug.Print($"GET ROLES FAILED: {ex.ToString()}");
                response.Success = false;
            }

            return response;
        }
    }
}
