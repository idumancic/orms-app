﻿using ORMS.Service.Base;
using ORMS.Service.Role.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMS.Service.Role.Interface
{
    public interface IRoleService : IBaseService
    {
        RoleResponse GetRoles();
    }
}
