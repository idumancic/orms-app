﻿using ORMS.Service.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMS.Service.Role.ViewModel
{
    public class RoleViewModel : BaseModel
    {
        public string RoleCode { get; set; }
        public string Name { get; set; }
    }
}
