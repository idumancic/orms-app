﻿using ORMS.Service.Base;
using ORMS.Service.Role.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMS.Service.Role.Response
{
    public class RoleResponse : BaseResponse
    {
        public IList<RoleViewModel> Roles { get; set; }
    }
}
