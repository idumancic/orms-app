﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMS.Service.WorkerLog.ViewModel
{
    public class WorkerLogViewModel
    {
        public int Id { get; set; }
        public int WorkerId { get; set; }
        public int ShiftId { get; set; }
        public decimal ShiftEarnings { get; set; }
        public int RecordId { get; set; }
    }
}
