﻿using ORMS.Infrastructure.Interfaces;
using ORMS.Service.Item.Interface;
using ORMS.Service.Item.Response;
using ORMS.Service.Mappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ORMS.Service.Item.Request;
using ORMS.Service.Base;
using ORMS.Infrastructure.Models;
using ORMS.Service.Authentication.Interface;
using ORMS.Service.Authentication;
using System.Diagnostics;

namespace ORMS.Service.Item
{
    public class ItemService : IItemService
    {
        private IUnitOfWork unitOfWork;

        public IUnitOfWork UnitOfWork { get { return unitOfWork; } }

        public ItemService(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public ItemPagingResponse GetPage(ItemPagingRequest request)
        {
            ItemPagingResponse response = new ItemPagingResponse();
            try
            {
                var pageProp = new PageProperties(request.PageNumber, request.PageSize, request.OrderBy);
                var page = unitOfWork.Items.GetPage(pageProp, request.Name, request.CategoryId);

                response.Success = true;
                response.Items = page.Data.ToListItemViewModel();
                response.TotalCount = page.TotalCount;
                response.TotalPages = page.Data.Pages(page.TotalCount, request.PageSize);
            }
            catch (Exception ex)
            {
                Debug.Print(string.Format($"ITEM PAGE FAILED: {ex.ToString()}"));
                response.Success = false;
            }

            return response;
        }

        public ItemPagingResponse GetPageBySearch(ItemPagingRequest request)
        {
            return GetPage(request);
        }

        public GeneralResponse Add(ItemUpdateRequest request)
        {
            GeneralResponse response = new GeneralResponse();
            var authUser = AuthenticationService.GetUser();
            Core.Item item = new Core.Item()
            {
                Name = request.Name,
                Price = request.Price,
                CategoryId = request.CategoryId
            };

            try
            {
                item.PrepareForInsert();
                unitOfWork.Items.Add(item);
                unitOfWork.Commit();
                response.Success = true;
            }
            catch (Exception ex)
            {
                Debug.Print(string.Format($"ITEM ADD FAILED: {ex.ToString()}"));
                response.Success = false;
            }

            return response;   
        }

        public GeneralResponse Update(ItemUpdateRequest request)
        {
            GeneralResponse response = new GeneralResponse();
            try
            {
                var item = unitOfWork.Items.Get(request.Id);

                item.ItemId = request.Id.Value;
                item.Name = request.Name;
                item.Price = request.Price;
                item.CategoryId = request.CategoryId;
                item.Active = request.Active.ToInt();

                item.PrepareForUpdate();
                unitOfWork.Items.Update(item);
                unitOfWork.Commit();

                response.Success = true;
            }
            catch (Exception ex)
            {
                Debug.Print(string.Format($"ITEM UPDATE FAILED: {ex.ToString()}"));
                response.Success = false;
            }

            return response;
        }

        public GeneralResponse GetTotalItemCount()
        {
            GeneralResponse response = new GeneralResponse();

            try
            {
                response.Data = unitOfWork.Items.Count();
                response.Success = true;
            }
            catch (Exception ex)
            {
                Debug.Print($"GET TOTAL ITEM COUNT FAILED: {ex.ToString()}");
                response.Success = false;
            }

            return response;
        }
    }
}
