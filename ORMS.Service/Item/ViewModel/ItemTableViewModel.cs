﻿using ORMS.Service.Base;
using ORMS.Service.Category.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMS.Service.Item.ViewModel
{
    public class ItemTableViewModel : BaseModelWithRowIndex
    {
        public string Name { get; set; }
        public decimal Price { get; set; }
        public CategoryViewModel Category { get; set; }
    }
}
