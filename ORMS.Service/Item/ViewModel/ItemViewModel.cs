﻿using ORMS.Service.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMS.Service.Item.ViewModel
{
    public class ItemViewModel : BaseModel
    {
        public string Name { get; set; }
        public decimal Price { get; set; }
    }
}
