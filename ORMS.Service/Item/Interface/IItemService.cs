﻿using ORMS.Service.Base;
using ORMS.Service.Item.Request;
using ORMS.Service.Item.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMS.Service.Item.Interface
{
    public interface IItemService : IBaseService
    {
        ItemPagingResponse GetPage(ItemPagingRequest request);
        ItemPagingResponse GetPageBySearch(ItemPagingRequest request);
        GeneralResponse Add(ItemUpdateRequest request);
        GeneralResponse Update(ItemUpdateRequest request);
        GeneralResponse GetTotalItemCount();
    }
}
