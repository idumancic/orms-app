﻿using ORMS.Service.Base;

namespace ORMS.Service.Item.Request
{
    public class ItemPagingRequest : BasePagingRequest
    {
        private string name;
        public string Name
        {
            get { return name != null ? name.ToLower() : name; }
            set { name = value; }
        }
        public int? CategoryId { get; set; }
    }
}