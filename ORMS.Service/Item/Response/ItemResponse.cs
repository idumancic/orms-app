﻿using ORMS.Service.Item.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMS.Service.Item.Response
{
    public class ItemResponse
    {
        public ItemTableViewModel Item { get; set; }
    }
}
