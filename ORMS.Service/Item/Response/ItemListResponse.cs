﻿using ORMS.Service.Base;
using ORMS.Service.Item.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMS.Service.Item.Response
{
    public class ItemListResponse : BaseResponse
    {
        public IList<ItemTableViewModel> Items { get; set; }
    }
}
