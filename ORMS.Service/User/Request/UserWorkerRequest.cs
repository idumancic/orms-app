﻿using ORMS.Service.User.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMS.Service.User.Request
{
    public class UserWorkerRequest
    {
        public int WorkerId { get; set; }
    }
}
