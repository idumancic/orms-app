﻿using ORMS.Service.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMS.Service.User.ViewModel
{
    public class UserWorkerViewModel : BaseModel
    {
        public string Username { get; set; }

        public string Password { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public DateTime? DateOfBirth { get; set; }

        public string JMBG { get; set; }

        public string OIB { get; set; }

        public string Address { get; set; }

        public string PostalCode { get; set; }

        public string City { get; set; }

        public string PhoneNumber { get; set; }

        public string Email { get; set; }

        public int? OfficeId { get; set; }

        public int? SanitaryIdVerified { get; set; }

        public DateTime? SanitaryIdCreatedDate { get; set; }

        public DateTime? SanitaryIdExpirationDate { get; set; }

        public string RoleCode { get; set; }
    }
}
