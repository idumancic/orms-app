﻿using ORMS.Service.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMS.Service.User.ViewModel
{
    public class UserWorkerListViewModel : BaseModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName { get { return string.Format($"{FirstName} {LastName}"); } }
        public string RoleName { get; set; }
        public string OfficeName { get; set; }
    }
}
