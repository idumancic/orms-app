﻿using ORMS.Service.Base;
using ORMS.Service.User.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMS.Service.User.Response
{
    public class UserWorkerListResponse : BaseResponse
    {
        public IList<UserWorkerListViewModel> Workers { get; set; }
    }
}
