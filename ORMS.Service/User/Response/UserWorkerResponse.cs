﻿using ORMS.Service.Base;
using ORMS.Service.User.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMS.Service.User.Response
{
    public class UserWorkerResponse : BaseResponse
    {
        public UserWorkerViewModel Worker { get; set; }
    }
}
