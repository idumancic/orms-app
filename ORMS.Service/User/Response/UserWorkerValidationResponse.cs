﻿using ORMS.Service.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMS.Service.User.Response
{
    public class UserWorkerValidationResponse : BaseResponse
    {
        public bool UsernameTaken { get; set; }
        public bool OIBExists { get; set; }
        public bool JMBGExists { get; set; }
    }
}
