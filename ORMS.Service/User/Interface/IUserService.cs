﻿using ORMS.Service.Base;
using ORMS.Service.User.Request;
using ORMS.Service.User.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMS.Service.User.Interface
{
    public interface IUserService : IBaseService
    {
        UserWorkerListResponse GetWorkers();
        UserWorkerResponse GetWorker(UserWorkerRequest request);
        GeneralResponse AddWorker(UserWorkerUpdateRequest request);
        GeneralResponse UpdateWorker(UserWorkerUpdateRequest request);
        UserWorkerValidationResponse Validate(UserWorkerUpdateRequest request);
    }
}
