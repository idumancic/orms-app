﻿using ORMS.Service.User.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ORMS.Infrastructure.Interfaces;
using ORMS.Service.User.Response;
using ORMS.Service.Mappers;
using System.Diagnostics;
using ORMS.Service.User.Request;
using ORMS.Service.Base;

namespace ORMS.Service.User
{
    public class UserService : IUserService
    {
        private IUnitOfWork unitOfWork;

        public IUnitOfWork UnitOfWork { get { return unitOfWork; } }

        public UserService(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public UserWorkerListResponse GetWorkers()
        {
            UserWorkerListResponse response = new UserWorkerListResponse();

            try
            {
                var workers = unitOfWork.Users.GetWorkers();
                response.Success = true;
                response.Workers = workers.ToListUserWorkerViewModel();
            }
            catch (Exception ex)
            {
                Debug.Print($"WORKERS GET FAILED: {ex.ToString()}");
                response.Success = false;
            }
           
            return response;
        }

        public UserWorkerResponse GetWorker(UserWorkerRequest request)
        {
            UserWorkerResponse response = new UserWorkerResponse();

            try
            {
                var worker = unitOfWork.Users.GetWorker(request.WorkerId);
                response.Success = true;
                response.Worker = worker.ToUserWorkerViewModel();
            }
            catch (Exception ex)
            {
                Debug.Print($"WORKER GET FAILED: {ex.ToString()}");
                response.Success = false;
            }

            return response;
        }

        public GeneralResponse AddWorker(UserWorkerUpdateRequest request)
        {
            GeneralResponse response = new GeneralResponse();

            try
            {
                Core.User worker = new Core.User();
                worker.MapCreate(request.Worker);
                worker.PrepareForInsert();

                unitOfWork.Users.Add(worker);
                unitOfWork.Commit();

                response.Data = worker.UserId;
                response.Success = true;
            }
            catch (Exception ex)
            {
                Debug.Print($"ADD WORKER FAILED: {ex.ToString()}");
                response.Success = false;
            }


            return response;
        }

        public GeneralResponse UpdateWorker(UserWorkerUpdateRequest request)
        {
            GeneralResponse response = new GeneralResponse();

            try
            {
                Core.User worker = unitOfWork.Users.GetWorker(request.Worker.Id);

                worker.MapUpdate(request.Worker);
                worker.PrepareForUpdate();

                unitOfWork.Users.Update(worker);
                unitOfWork.Commit();

                response.Success = true;
            }
            catch (Exception ex)
            {
                Debug.Print($"UPDATE WORKER FAILED: {ex.ToString()}");
                response.Success = false;
            }


            return response;
        }

        public UserWorkerValidationResponse Validate(UserWorkerUpdateRequest request)
        {
            UserWorkerValidationResponse response = new UserWorkerValidationResponse();

            try
            {
                response.UsernameTaken = unitOfWork.Users.CheckUsername(request.Worker.Username);
                response.OIBExists = unitOfWork.Users.CheckOIB(request.Worker.OIB);
                response.JMBGExists = unitOfWork.Users.CheckJMBG(request.Worker.JMBG);
                response.Success = true;
            }
            catch (Exception ex)
            {
                Debug.Print($"UPDATE WORKER FAILED: {ex.ToString()}");
                response.Success = false;
            }


            return response;
        }
    }
}
