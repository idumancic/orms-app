﻿using ORMS.Service.Authentication.Request;
using ORMS.Service.Authentication.Response;
using ORMS.Service.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMS.Service.Authentication.Interface
{
    public interface IAuthenticationService : IBaseService
    {
        AuthenticationResponse Authenticate(AuthenticationRequest authRequest);
    }
}
