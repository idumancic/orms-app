﻿using ORMS.Core;
using ORMS.Infrastructure;
using ORMS.Infrastructure.Interfaces;
using ORMS.Service.Authentication.Interface;
using ORMS.Service.Authentication.Request;
using ORMS.Service.Authentication.Response;
using ORMS.Service.Base;
using ORMS.Service.Mappers;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMS.Service.Authentication
{
    public class AuthenticationService : IAuthenticationService
    {
        private static Core.User authenticatedUser;

        private IUnitOfWork unitOfWork;

        public IUnitOfWork UnitOfWork { get { return unitOfWork; } }

        public AuthenticationService(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public AuthenticationResponse Authenticate(AuthenticationRequest authRequest)
        {
            var response = new AuthenticationResponse();

            try
            {
                var user = unitOfWork.Users.Get(authRequest.Username, authRequest.Password);

                if (user != null)
                {
                    authenticatedUser = user;
                    response.Success = true;
                    response.User = user.ToUserViewModel();
                }

            }
            catch (Exception ex)
            {
                Debug.Print($"AUTHORIZATION FAILED: ${ex.ToString()}");
                response.Success = false;
            }

            return response;
        }

        public static Core.User GetUser()
        {
            return authenticatedUser;
        }

        public static string GetRole()
        {
            return authenticatedUser.RoleCode;
        }

        public static bool IsAdmin()
        {
            return GetRole().Equals(Core.Role.ADMIN);
        }

        public static bool IsOwner()
        {
            return GetRole().Equals(Core.Role.OWNER);
        }

        public static bool IsManager()
        {
            return GetRole().Equals(Core.Role.MANAGER);
        }

        public static bool IsWorker()
        {
            return GetRole().Equals(Core.Role.WORKER);
        }
    }
}
