﻿using ORMS.Service.Statistic.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ORMS.Infrastructure.Interfaces;
using ORMS.Service.Base;
using System.Diagnostics;
using ORMS.Service.Mappers;

namespace ORMS.Service.Statistic
{
    public class StatisticsService : IStatisticsService
    {
        private IUnitOfWork unitOfWork;
        public IUnitOfWork UnitOfWork { get { return unitOfWork; } }
        public StatisticsService(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public GeneralResponse GetYearlyEarnings()
        {
            GeneralResponse response = new GeneralResponse();

            try
            {
                var earnings = unitOfWork.Records.GetYearlyEarnings();
                response.Data = earnings.ToStatisticsViewModel();
                response.Success = true;
            }
            catch (Exception ex)
            {
                Debug.Print($"GET EARNINGS BY YEAR FAILED: {ex.ToString()}");
                response.Success = false;
            }

            return response;
        }

        public GeneralResponse GetMonthlyEarnings()
        {
            GeneralResponse response = new GeneralResponse();

            try
            {
                var earnings = unitOfWork.Records.GetMonthlyEarnings();
                response.Data = earnings.ToStatisticsViewModel();
                response.Success = true;
            }
            catch (Exception ex)
            {
                Debug.Print($"GET EARNINGS BY MONTH FAILED: {ex.ToString()}");
                response.Success = false;
            }

            return response;
        }

        public GeneralResponse GetTop10Items()
        {
            GeneralResponse response = new GeneralResponse();

            try
            {
                var earnings = unitOfWork.RecordItems.GetTop10Items();
                response.Data = earnings.ToStatisticsViewModel();
                response.Success = true;
            }
            catch (Exception ex)
            {
                Debug.Print($"GET EARNINGS BY TOP 10 ITEMS FAILED: {ex.ToString()}");
                response.Success = false;
            }

            return response;
        }

        public GeneralResponse GetShiftEarnings()
        {
            GeneralResponse response = new GeneralResponse();

            try
            {
                var earnings = unitOfWork.WorkerLogs.GetShiftEarnings();
                var model = earnings.ToStatisticsViewModel();
                model.Labels[0] = "1. smjena";
                model.Labels[1] = "2. smjena";

                response.Data = model;
                response.Success = true;
            }
            catch (Exception ex)
            {
                Debug.Print($"GET EARNINGS BY SHIFT FAILED: {ex.ToString()}");
                response.Success = false;
            }

            return response;
        }
    }
}
