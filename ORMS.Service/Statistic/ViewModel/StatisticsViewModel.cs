﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMS.Service.Statistic.ViewModel
{
    public class StatisticsViewModel
    {
        public List<string> Series { get; set; }
        public List<string> Labels { get; set; }
        public List<int> Earnings { get; set; }

        public StatisticsViewModel()
        {
            Series = new List<string>();
            Labels = new List<string>();
            Earnings = new List<int>();
        }
    }
}
