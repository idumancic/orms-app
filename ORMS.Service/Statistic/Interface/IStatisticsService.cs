﻿using ORMS.Service.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMS.Service.Statistic.Interface
{
    public interface IStatisticsService : IBaseService
    {
        GeneralResponse GetYearlyEarnings();
        GeneralResponse GetMonthlyEarnings();
        GeneralResponse GetTop10Items();
        GeneralResponse GetShiftEarnings();
    }
}
