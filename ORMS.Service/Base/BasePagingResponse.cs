﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMS.Service.Base
{
    public abstract class BasePagingResponse : BaseResponse
    {
        public int TotalCount { get; set; }
        public int TotalPages { get; set; }
    }
}
