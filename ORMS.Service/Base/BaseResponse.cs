﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMS.Service.Base
{
    public abstract class BaseResponse
    {
        private bool success = false;

        public bool Success
        {
            get { return success; }
            set { success = value; }
        }

        public string Message { get; set; }
    }
}
