﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMS.Service.Base
{
    public class GeneralResponse : BaseResponse
    {
        public object Data { get; set; }
    }
}
